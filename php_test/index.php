<?php
    session_start(); 
    include 'database.php';
    
    if (isset($_POST['sign_in'])) 
    {
        $v_username = $_POST['v_username'];
        $password = $_POST['v_password'];

        $select = "select * from admin_login where v_username='$v_username' and v_password='$password'";

        $result = $conn->query($select);
        $row = $result->fetch_assoc();

        if(!empty($row))
        {   
            $pass=$row['v_password'];
            if($password=$pass)
            {
                $_SESSION['v_username']=1;
                echo "<script>alert('Login success')</script>";
                echo "<script type='text/javascript'> document.location ='dashboard.php'; </script>";        
            }
        } else {
            echo '<script>alert("Invalid Password") </script>' ;
        }
    }
?>
<html>
    <title>Login</title>
    <head>
    <meta name="viewport" content="width=device-width"/>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" />
    </head>
    <body style="background: #0103;">  
        <form method="POST" autocomplete="off" enctype="multipart/form-data" id="form_submit">
            <div class="container mt-5">
                <div class="card">
                    <center><div class="card-header">
                        User Login
                    </div></center>
                    <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label>UserName </label>
                            <input type="email" class="form-control" id="v_username" name="v_username" placeholder="Enter Your UserName" required/>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label>Password</label>
                            <input type="password" class="form-control" id="v_password" name="v_password" placeholder="Enter Your Password" required/>
                        </div>
                        <button type="submit" class="btn btn-success" id="sign_in" name="sign_in">Log In</button>
                    </div>
                </div>    
                <div class="card-footer text-center">
                    <div class="small"><a href="signup_user.php">New User Sign up</a></div>
                </div>  
            </div>
        </form>
    </body>
</html>