<?php
    session_start();
    include 'database.php';
    if(empty($_SESSION)){
        header('location:index.php');
    }
    $i_id=$_GET['i_id'];
    
    $sql="SELECT * FROM `user` WHERE i_id='$i_id'";   

    $row = $conn->query($sql);
    $data=$row->fetch_assoc();

    if(isset($_POST['save'])) {

        $v_name = $_POST['v_name'];
        $v_email_id = $_POST['v_email_id'];
        $v_status = $_POST['v_status'];
        $b_phone = $_POST['b_phone'];

        $sql_update = "UPDATE `user` SET v_name='$v_name',v_email_id='$v_email_id',v_status='$v_status',b_phone='$b_phone' WHERE i_id='$i_id'";

        if($conn->query($sql_update)){
            echo "<script>alert('Record Update success')</script>";
            echo "<script type='text/javascript'> document.location ='dashboard.php'; </script>";        
        } else {
            echo "Record Not Updated";
        }
    }
?>
<html>
    <title>Edit User Data</title>
    <head>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" />
    </head>  
    <body>
        <form method="POST" autocomplete="off" enctype="multipart/form-data" id="form_submit">
            <div class="container mt-5">
                <div class="card">
                    <center><div class="card-header">
                        User Edit Data
                    </div></center>
                    <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label>Name </label>
                            <input type="text" class="form-control" id="v_name" name="v_name" placeholder="Enter Your Name" value="<?php echo $data['v_name']; ?>" required/>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label>Email Id</label>
                            <input type="email" class="form-control" id="v_email_id" name="v_email_id" placeholder="Enter Your Email Id" value="<?php echo $data['v_email_id']; ?>" required/>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label>Status</label>
                            <select class="form-control" id="v_status" name="v_status" onchange="changeMessage(this)" required>
                                <option value="Active" <?php echo $data['v_status'] == "Active" ? "selected": "";?>>Active</option>
                                <option value="Inactive" <?php echo $data['v_status'] == "Inactive" ? "selected": "";?>>Inactive</option>
                            </select>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label>Phone No</label>
                            <input type="number" class="form-control" id="b_phone" name="b_phone" placeholder="Enter Your Phone No" value="<?php echo $data['b_phone']; ?>" required/>
                        </div>
                        <button type="submit" class="btn btn-success" id="save" name="save">submit</button>
                    </div>
                </div>
            </div>
        </form>
    </body>
    <script>
        function changeMessage(v_status) {
            var selectedText = v_status.options[v_status.selectedIndex].innerHTML;
            var selectedValue = v_status.value;
            alert("Select Value is : " + selectedValue);
        }
    </script>
</html>