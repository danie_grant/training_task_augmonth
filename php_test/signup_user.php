<?php
    session_start();
    include 'database.php';
    
    if (isset($_POST['submit'])) 
    {
        $v_username = $_POST['v_username'];
        $v_password = $_POST['v_password'];
        
        $sql="INSERT INTO `admin_login`(`v_username`, `v_password`) VALUES ('$v_username','$v_password')";
        //echo $sql;
        if ($conn->query($sql) === true) {
            header("location:index.php");
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
?>
<html>
    <title>User registration</title>
    <head>
        <meta name="viewport" content="width=device-width"/>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" />
    </head>
    <body>
        <form method="POST" autocomplete="off" enctype="multipart/form-data">
            <div class="container mt-5">
                <div class="card">
                    <center><div class="card-header">
                        User Registration
                    </div></center>
                    <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label>UserName </label>
                            <input type="email" class="form-control" id="v_username" name="v_username" placeholder="Enter Your Email" required/>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label>Password</label>
                            <input type="password" class="form-control" id="v_password" name="v_password" placeholder="Enter Your Password" required/>
                        </div>
                        <div class="mt-4 ml-2">
                            <button class="btn btn-success" type="submit" name="submit" id=>Save</button>
                            <button class="btn btn-secondary">
                              <a href="dashboard.php" style="color:inherit"> Back </a>
                            </button>
                          </div>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>