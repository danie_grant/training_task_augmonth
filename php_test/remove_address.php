<?php
    session_start();
    include 'database.php';
    if(empty($_SESSION)){
        header('location:index.php');
    }
    $i_id= $_GET['i_id'];

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "DELETE FROM user_address WHERE i_id=$i_id";
    
    if ($conn->query($sql) === TRUE) {
        echo "<script>alert('Remove success')</script>";
        echo "<script type='text/javascript'> document.location ='dashboard.php'; </script>";
    } else {
        echo "Error deleting record: " . $conn->error;
    }
    $conn->close();
?>