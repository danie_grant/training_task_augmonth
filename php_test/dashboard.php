
<?php
    session_start();
    include 'database.php';
    if(empty($_SESSION)){
        header('location:index.php');
    }
    
    if (isset($_POST['logout'])) 
    {
        session_destroy();
        echo "<script>alert('Logout success')</script>";
        echo "<script type='text/javascript'> document.location ='index.php'; </script>";
    }
?>
<!DOCTYPE html>
<html>
    <title>Dashboard</title>
    <head>
        <meta name="viewport" content="width=device-width"/>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
    </head>
    <body>
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Demo Test</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="dashboard.php" style="color:white;">Dashboard</a>
                    <a class="nav-item nav-link" href="user_registration.php" style="color:white;">Add User</a>
                    <a class="nav-item nav-link" href="add_new_user_address.php" style="color:white;">Add Address</a>

                    <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                <li>
                                    <form method="POST" action="" enctype="multipart/form-data">
                                        <button class="dropdown-item" type="submit" name="logout" id="logout">Logout</button>
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav><br><br><br>
        <h1 align="center">Welcome to Dashboard</h1>
        <form enctype="multipart/form-data">
            <table class="table" border="1px" align="center" id="example">
                <thead class="thead-dark" align="center">
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email Id</th>
                    <th>Phone No.</th>
                    <th>Status</th>
                    <th>Action</th>
                </thead>
                <tbody>
                <?php
                    $select = "SELECT * FROM user";
                    $result = $conn->query($select);
                    if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                ?>
                    <tr align="center">
                        <td><?php echo $row['i_id']; ?></td>
                        <td><?php echo $row['v_name']; ?></td>
                        <td><?php echo $row['v_email_id']; ?></td>
                        <td><?php echo $row['b_phone']; ?></td>
                        <td><?php echo $row['v_status']; ?></td>
                        <td><a class="btn btn-warning" href="edit.php?i_id=<?php echo $row['i_id']; ?>"><i class="fa fa-list"></i>Edit</a>
                        <a class="btn btn-danger" href="delete.php?i_id=<?php echo $row['i_id']; ?>"><i class="fa fa-trash"></i> Delete</a>
                        
                        </td>
                        </form>
                    </tr>
                    <?php }} else {
                        echo "<No Record Found";
                    }?>
                </tbody>
            </table>
        </form>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
</html>
