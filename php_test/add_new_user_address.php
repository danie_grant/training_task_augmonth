<?php
    session_start();
    include 'database.php';
        if(empty($_SESSION)){
        header('location:index.php');
    }

    if (isset($_POST['submit'])) 
    {
        $user_id=$_POST['user_id'];
        $v_city = $_POST['v_city'];
        $v_country = $_POST['v_country'];
        $v_address = $_POST['v_address'];
        
        $sql="INSERT INTO `user_address`(`v_city`, `v_country`, `v_address`,`user_id`) VALUES ('$v_city','$v_country','$v_address','$user_id')";
        //echo $sql;
        if ($conn->query($sql) === true) {
            echo "<script>alert('successfully Inserted');</script>";
            // header("location:add_new_user_address.php");
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
?>
<html>
    <title>Add Address</title>
    <head>
        <meta name="viewport" content="width=device-width"/>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" />
    </head>
    <body>
        <form method="POST" autocomplete="off" enctype="multipart/form-data">
            <div class="container mt-5">
                <div class="card">
                    <center><div class="card-header">Add New User Address</div></center>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label>Select User Id</label>
                                <select class="form-control" name="user_id" id="user_id" onchange="changeMessage(this)" required>
                                <option value="">Select User Id</option>
                                    <?php 
                                    include 'database.php';
                                    $sql="select i_id,v_name from user";
                                    
                                    $row=$conn->query($sql);
                                    while($data=$row->fetch_assoc())
                                    {
                                    ?>
                                    <option value="<?php echo $data['i_id']?>"><?php echo $data['v_name']?></option>
                                    <?php }?>
                                </select>
                            </div>    
                        <div class="col-md-12 mb-3">
                            <label>City Name </label>
                            <input type="text" class="form-control" id="v_city" name="v_city" placeholder="Enter Your City Name" required/>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label>Country Name</label>
                            <input type="text" class="form-control" id="v_country" name="v_country" placeholder="Enter Your Country Name" required/>
                        </div>
                        <div class="col-md-12 mb-3">
                            <label>Address</label>
                            <input type="text" class="form-control" id="v_address" name="v_address" placeholder="Enter Your Address" required/>
                        </div>
                        <div class="mt-4 ml-2">
                            <button class="btn btn-success" type="submit" name="submit">Save</button>
                            <button class="btn btn-secondary">
                              <a href="dashboard.php" style="color:inherit"> Back </a>
                            </button>
                        </div>
                    </div>
                </div>
            </div><br>
            <table class="table" border="1px" align="center" id="example">
                <thead class="thead-dark" align="center">
                    <th>Id</th>
                    <th>City Name</th>
                    <th>Country Name</th>
                    <th>Address</th>
                    <th>Action</th>
                </thead>
                <tbody>
                <?php
                    $select = "SELECT * FROM user_address";
                    $result = $conn->query($select);
                    if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                ?>
                    <tr align="center">
                        <td><?php echo $row['i_id']; ?></td>
                        <td><?php echo $row['v_city']; ?></td>
                        <td><?php echo $row['v_country']; ?></td>
                        <td><?php echo $row['v_address']; ?></td>
                        <td><a class="btn btn-danger" href="remove_address.php?i_id=<?php echo $row['i_id']; ?>">Remove </a>
                        </td>
                        </form>
                    </tr>
                    <?php }} else {
                        echo "No Record Found";
                    }?>
                </tbody>
            </table>
        </form>
    </body>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
        function changeMessage(v_status) {
            var selectedText = v_status.options[v_status.selectedIndex].innerHTML;
            var selectedValue = v_status.value;
           // alert("Select Value is : " + selectedValue);
        }
    </script>
</html>