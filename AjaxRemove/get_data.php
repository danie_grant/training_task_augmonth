<div class="col-md-4">
    <table class="table" id="user_table">
        <thead style="background-color: #708090;">
            <tr>
                <th style="color:white" class="text-center">Id</th>
                <th style="color:white" class="text-center">Name</th>
                <th style="color:white" class="text-center">Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
                include 'database.php';
                $sql=mysqli_query($conn,"select * from `user`");
                while($row=mysqli_fetch_array($sql)){
                    ?>
                        <tr align="center">
                            <td><?php echo $row['id']; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td>
                                <button class="btn btn-danger" id="delete" value="<?php echo $row['id']; ?>">Delete</button>
                                <button class="btn btn-warning" data-toggle="modal" id="btn_edit" data-id="<?php echo $row['id']; ?>" data-target="#editmodel">Edit</button>
                            </td>
                        </tr>
                    <?php
                }
            ?>
        </tbody>
    </table>
</div>