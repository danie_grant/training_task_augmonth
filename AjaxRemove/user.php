<?php
    session_start();    
    include 'database.php';
?>
<html>
    <title>Ajax Example</title>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    </head>
    <body>
        <div class="col-md-4 alert alert-success mt-5" id="alert">
        </div>
        <div class="col-md-12">
            <h3>Add New Record </h3>
            <div>
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal">Add Record</button>
            </div><br>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add New Record</h4>
                        </div>
                        <form method="POST" autocomplete="off" enctype="multipart/form-data">
                            <div class="modal-body">
                                <label for="name"> Name <span> </span> </label>
                                <input class="form-control" id="name" name="name" type="text" required>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success" name="save" value="save" id="save">Save</button>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
        </div>
       

        <div class="col-md-12">
            <div class="modal fade" id="editmodel" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Record</h4>
                        </div>
                        <form method="POST" autocomplete="off" enctype="multipart/form-data">
                            <div class="modal-body">
                            <input class="form-control" id="edit_id" name="edit_id" type="hidden" hidden>
                                <label for="name"> Name <span> </span> </label>
                                <input class="form-control" id="edit_name" name="edit_name" type="text" value="">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success" name="edit_btn" id="edit_btn">Save</button>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
        </div>
        
        <div id="data_tables"><?php include_once('get_data.php'); ?></div>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#alert").hide();
                $('#save').on('click', function(e) {
                    e.preventDefault();
                    var name=$('#name').val();
                    $.ajax({
                        type: "POST",
                        url: "insert_data.php",
                        data: { name:name,},
                        success: function(data) {
                            $('#myModal').modal('hide');
                            $('#alert').html(data).fadeIn('slow');
                            $('#alert').delay(1500).fadeOut('slow');
                            $('.modal-backdrop').remove();
                            test();
                            
                        }
                    });
                });
            });
        
            function test()
            {
                $('#data_tables').load('get_data.php');
            }

            $(document).on('click', '#delete', function(){
                if (confirm('Are You Sure Delete Record?')) {
                    $id=$(this).val();
                        $.ajax({
                        type: "POST",
                        url: "delete.php",
                        data: {
                            id: $id,
                        },
                        success: function(data){
                            $('#alert').html(data).fadeIn('slow');
                            $('#alert').delay(1500).fadeOut('slow');
                            test();
                        }
                    });
                }
            });

            $(document).on('click', '#btn_edit', function(event){
                event.preventDefault();
                id=$(this).data('id');
                console.log(id);
                    $.ajax({
                    type: "POST",
                    url: "edit_data.php",
                    data: {
                        id: id,
                    },
                    dataTyepe:"json",
                    success: function(data){
                        // alert(data);
                        $("input#edit_name").val(data);
                        $("input#edit_id").val(id);
                    }
                });
            });
            
            $(document).on('click','#edit_btn',function(event){
                event.preventDefault();
                var id=$('#edit_id').val();
                var name=$('#edit_name').val();
                $.ajax({
                    type: "POST",
                    url: "update_data.php",
                    data: {
                        id:id,
                        name:name
                    },
                    success: function(data){
                        $('#editmodel').hide();
                        $('.modal-backdrop').remove();
                        $("input#edit_name").val(data);
                        $("input#edit_id").val(id);
                        $('#alert').html(data).fadeIn('slow');
                        $('#alert').delay(1500).fadeOut('slow');
                        test();
                    },
                });
            });
        </script>
    </body>
</html>