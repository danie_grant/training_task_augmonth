<?php
    $word = 'camelCaseVar';
    $came = ucwords($word, "|");
    echo $came;

    echo "<br><br>";
    $var = "snake case var";
    $snake = str_replace(" ", "_", $var);
    echo $snake; 

    echo "<br><br>";
    $str = "snake case var";
    $new = str_replace(" ","-", $str);
    echo $new;

    echo "<br><br>";
    $str = "pascal_case_var";
    $str = str_replace("_", "", ucwords($str, " _"));
    echo $str;

    echo "<br><br>";
    $str = 'upper case snake var';
    echo str_replace(' ', '_', strtoupper($str));

?>

