<html>
    <title>User Input Palindrome Number Example</title>
    <body>
        <form method="post" align="center"> 
            <h1>Palindrome Number Or Not. Example</h1> 
            Enter a Number: <input type="text" name="num"><br><br>  
            <input type="submit" name="submit" value="Submit">  
        </form>  
    </body>
    <?php   
        if($_POST)  
        {  
            //get the value from form  
            $num = $_POST['num'];  
            //reversing the number  
            $reverse = strrev($num);  
            
            //checking if the number and reverse is equal  
            if($num == $reverse){  
                echo "The number $num is Palindrome";     
            }else{  
                echo "The number $num is not a Palindrome";   
            }  
        }     
    ?>  
</html>