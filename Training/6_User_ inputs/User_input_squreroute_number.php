<html>
    <title>User Input Square Root of a Number Example</title>
    <body>
        <form method="post" align="center"> 
            <h1>Square Root of a Number Example</h1> 
            Enter a Number: <input type="text" name="input"><br><br>  
            <input type="submit" name="submit" value="Submit">  
        </form>  
    
    <?php
        if(isset($_POST['submit'])) {
        //storing the number in a variable $input
        $input = $_POST['input'];
        //storing the square root of the number in a variable $ans
        $ans = sqrt($input);
        //printing the result
        echo 'The square root of '.$input.'=='.$ans;
        }
    ?>
    </body>
</html>