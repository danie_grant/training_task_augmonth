<!DOCTYPE html>
<?php
include 'database.php';
?>
<html>
    <head>
      <TITLE>
        
      </TITLE>
      <title>bootstrap example</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    </head>
  <body>
	<br>
    <h1 align="center">Registration Data Show</h1>
    <div class="container-fluid">
      <table class="table table-striped table-hover">
  <thead>
    <tr>
     <th>id</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Email</th>
      <th>Phone</th>
      <th>Address</th>
      <th>Gender</th>
      <th>Experience</th>
      <th>Birthday Date</th>
      <th>Photo</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php
$select = "select * from registration_process";

$result = $conn->query($select);
if ($result->num_rows > 0) {
  while ($row = $result->fetch_assoc()) {
    ?>
      <tr>
        <td><?php echo $row['id']; ?></td>
        <td><?php echo $row['first_name']; ?></td>
        <td><?php echo $row['last_name']; ?></td>
        <td><?php echo $row['email']; ?></td>
        <td><?php echo $row['phone_no']; ?></td>
        <td><?php echo $row['address']; ?></td>
        <td><?php echo $row['gender']; ?></td>
        <td><?php echo $row['experience']; ?></td>
        <td><?php echo $row['birthday']; ?></td>

        <td><img src="assets/images/<?php echo $row['photo']; ?>" height="70px;">
        <td><a class="btn btn-primary" href="registration.html">Home</a>
        <a href="delete.php?id=<?php echo $row['id']; ?>" class="btn btn-danger">Delete</a>
        <a href="edit.php?id=<?php echo $row["id"]; ?>" class="btn btn-warning">Edit</a></td>
      </tr>
      <?php }} else {
  echo "No Record Found";
}?>
  </tbody>
</table>
    </div>
	</body>
</html>