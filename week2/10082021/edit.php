<?php
    include 'database.php';
    $id=$_GET['id'];
    $sql="select * from registration_process where id='$id'";
    $row=$conn->query($sql);
    $data=$row->fetch_assoc();
    if (isset($_POST['save'])) {
        $edit_id=$_POST['edit_id'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $phone_no = $_POST['phone_no'];
        $address = $_POST['address'];
        $gender = $_POST['gender'];
        
        $exp = $_POST['experience'];
        
        $birthday = $_POST['birthday'];

        $photo = $_FILES['photo']['name'];
        $pic_tmp = $_FILES['photo']['tmp_name'];
    
        $sql_update="UPDATE registration_process SET first_name='$first_name',last_name='$last_name',email='$email',phone_no='$phone_no',address='$address',gender='$gender',experience='$exp',birthday='$birthday',photo='$photo' WHERE id='$id'"; 

        
        //echo $sql_update;
        if($conn->query($sql_update)){
            move_uploaded_file($pic_tmp, "assets/images/".$photo);
            header("location:registration.html");
        }
        else{
            echo "Record Not Updated";
        }
    }
?>

<html>
<link rel="stylesheet" type="text/css" href="styles.css">
<title>Edit Registration Form</title>
<body>
    <h1 align="center">Edit Registration Data</h1>
<form method="POST" action="" align="center" enctype="multipart/form-data">
First name:
        <input type="text" name="first_name" value="<?php echo $data['first_name']; ?>">
        <br><br>
        Last name:
        <input type="text" name="last_name" value="<?php echo $data['last_name']; ?>">
        <br><br>
        Email Id:
        <input type="email" name="email" value="<?php echo $data['email']; ?>">
        <br><br>
        Phone No:
        <input type="text" name="phone_no" value="<?php echo $data['phone_no']; ?>">
        <br><br>
        Address:
        <input type="textarea" name="address" value="<?php echo $data['address']; ?>">
        <br><br>  
        Gender:
        <input type="radio" name="gender" <?php if ($data['gender']=="male") echo "checked";?> value="male">Male 
        <input type="radio" name="gender" <?php if ($data['gender']=="female") echo "checked";?> value="female">Female
        <br><br>
        Experience :
        <select name="experience" id="experience">
            <option value="0"<?php if($data['experience'] == '0') { ?> selected="selected"<?php } ?>>0</option>
            <option value="1"<?php if($data['experience'] == '1') { ?> selected="selected"<?php } ?>>1</option>
            <option value="2"<?php if($data['experience'] == '2') { ?> selected="selected"<?php } ?>>2</option>
            <option value="3"<?php if($data['experience'] == '3') { ?> selected="selected"<?php } ?>>3</option>
            <option value="4"<?php if($data['experience'] == '4') { ?> selected="selected"<?php } ?>>4</option>
            </select>
            <br><br>
          Birthday Date : 
            <input class="w3-input w3-border" type="date" name="birthday" value="<?php echo $data['birthday']; ?>" id="birthday" placeholder="select birthday">
            <br><br> 
        Photo :
        <input class="w3-input w3-border" name="photo" type="file" id="photo">
        <img src="assets/images/<?php echo $data['photo'];?>" width="100px;" height="100px;">
        <br><br>
        
        <input type="submit" name="save" value="submit" id="save">

    </form>
</body>
</html>