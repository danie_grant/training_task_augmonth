<?php
// PHP Program to swap two
// numbers without using
// temporary variable
$x = 10; $y = 15;
 
// Code to swap 'x' and 'y'
$x = $x + $y; // x now becomes 15
$y = $x - $y; // y becomes 10
$x = $x - $y; // x becomes 5
 
echo "After Swapping: x = ",
       $x, ", ", "y = ", $y;
 
// This code is contributed by m_kit
?>