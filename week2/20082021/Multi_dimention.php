<?php

$employee = array(
        array(
            "id" => 15,
            "name" => "Anil",
        ),
        array(
            "id" => 24,
            "name" => "Raj",
        ),
        array(
            "id" => 3,
            "name" => "Pooja",
        ),
        array(
            "id" => 40,
            "name" => "Jay",
        )
    );
$empdata =  array(
            40=> array(
                "salary" => 30000,
                "bonus" => 10000,
            ),
            3=> array(
                "salary" => 20000,
                "bonus" => 5000,
            ),
            24=> array(
                "salary" => 25000,
                "bonus" => 4000,
            ),
            15=> array(
                "salary" => 15000,
                "bonus" => 2000,
            )
        );
    $output = array(
    array(
            "id" =>40,
            "name" =>"Anil",
            "salary" =>30000,
            "bonus" =>10000,
            "Total" => 40000,
        ),
        array(
            "id" =>2,
            "name" =>"Raj",
            "salary" =>20000,
            "bonus" =>5000,
            "Total" => 25000,
        ),
        array(
            "id" =>3,
            "name" =>"Pooja",
            "salary" =>25000,
            "bonus" =>4000,
            "Total" => 29000,
        ),
        array(
            "id" =>4,
            "name" =>"Jay",
            "salary" =>15000,
            "bonus" =>2000,
            "Total" => 17000,
        )
    );

    $temp_array=array();
    echo"<pre>";
    //print_r ($employee);
    foreach($employee as $work => $val)
    {
        
        $temp_array[$work]['id'] = $val['id'];
        $temp_array[$work]['name'] = $val['name'];
        $temp_array[$work]['salary'] = $empdata[$val['id']]['salary'];
        $temp_array[$work]['bonus'] =  $empdata[$val['id']]['bonus'];
        $temp_array[$work]['Total'] = array_sum($empdata[$val['id']]);
    }
    echo "<pre>";
    print_r($temp_array);
?>