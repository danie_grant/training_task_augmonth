<!DOCTYPE html>
<?php
include 'database.php';
?>
<html>
	<h1 align="center">Registration Form</h1>
  <body>
  	<link rel="stylesheet" type="text/css" href="assets/styles.css">
	<form method="post" action="process.php" align="center" enctype="multipart/form-data">
		First name:
		<input type="text" name="first_name">
		<br><br>
		Last name:
		<input type="text" name="last_name">
		<br><br>
		City name:
		<input type="text" name="city_name">
		<br><br>
		Email Id:
		<input type="email" name="email">
		<br><br>
		Photo :
		<input type="file" name="photo" id="photo" value="<?php echo $row['photo']; ?>">
		<br><br>
		Gender :
		Male <input type="radio" name="gender" value="male">
		Female<input type="radio" name="gender" value="female">
		<br><br>
		<input type="submit" name="save" value="submit">
	</form>
	<br>
	<table border="1px">
		<thead>
			<th>id</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>City Name</th>
			<th>Email</th>
			<th>Photo</th>
			<th>Gender</th>
			<th>Action</th>
		</thead>
		<tbody>
		<?php
$select = "select * from employee";
$result = $conn->query($select);
if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		?>
		<tr>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['first_name']; ?></td>
			<td><?php echo $row['last_name']; ?></td>
			<td><?php echo $row['city_name']; ?></td>
			<td><?php echo $row['email']; ?></td>
			<td><img src="images/<?php echo $row['photo']; ?>" height="70px;"></td>
			<td><?php echo $row['gender']; ?></td>
			<td>
	     		<a class="button button1" href="edit.php?action=edit&id=<?php echo $row["id"]; ?>" class="btn btn-warning">Edit</a>
     		</td>
		</tr>
		<?php }} else {
	echo "No Record Found!";
}?>
		</tbody>							
	</table>		
  </body>
</html>
