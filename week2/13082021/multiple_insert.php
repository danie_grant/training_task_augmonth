<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "phpdemo";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO category (firstname, lastname, email)
VALUES ('John', 'Doe', 'john@example.com');";
$sql .= "INSERT INTO category (firstname, lastname, email)
VALUES ('Mary', 'Moe', 'mary@example.com');";
$sql .= "INSERT INTO category (firstname, lastname, email)
VALUES ('1', 'Dooley', 'julie@example.com');";
$sql .= "INSERT INTO category (firstname, lastname, email)
VALUES ('2', 'Dooley', 'julie@example.com');";
$sql .= "INSERT INTO category (firstname, lastname, email)
VALUES ('2', 'Dooley', 'julie@example.com');";

if ($conn->multi_query($sql) === TRUE) {
  echo "New records created successfully";
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?>