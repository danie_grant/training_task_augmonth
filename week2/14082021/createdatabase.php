<?php
$servername = "localhost";
$username = "root";
$password = "";

// Create connection
$connection  = new mysqli($servername, $username, $password);
// Check connection
if ($connection->connect_error) {
  die("Connection failed: " . $connection->connect_error);
}

// Create database
$sql = "CREATE DATABASE phpdemo";
if ($connection->query($sql) === TRUE) {
  echo "Database created successfully";
} else {
  echo "Error creating database: " . $connection->error;
}

$connection->close();
?>