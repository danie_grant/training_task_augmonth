<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "phpdemo";

  // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }
  $sql = "INSERT INTO category(firstname, lastname, email, mo)
  VALUES ('John', 'Doe', 'john@example.com','9979013141');";
  if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
  } else {
    echo "Error: " . $sql . "<br>" . $conn->error;
  }

  $conn->close();
?>