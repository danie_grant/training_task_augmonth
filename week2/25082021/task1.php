<?php
$data1 = [0, 1, 2, 3, 4];

$data2 = ["zero" => 0, "one" => 1, "two" => 2, "three" => 3, "four" => 4];

$data3 = [
  [
    0,
    1
  ],
  [
    2,
    [
      3
    ]
  ]
];

$data4 =  [
  "a" => [
    "b" => 0,
    "c" => 1
  ],
  "b" => [
    "e" => 2,
    "o" => [
      "b" => 3
    ]
  ]
];

echo "<pre>";
$temp_array = array_search(3,$data1);
echo "array value is : &nbsp;&nbsp";
print_r($temp_array);

echo "<pre>";
$temp_array=array("three");
print_r($temp_array);


echo "<pre>";
$temp_array=array(3);
print_r($temp_array);

echo "<pre>";
$temp_array=array('b'=>3);
print_r($temp_array);
?>
