<?php
// INPUT 1
$keys = [
  "field1" => "first",
  "field2" => "second",
  "field3" => "third"
];

// INPUT 2
$values = [
  "field1value" => "dinosaur",
  "field2value" => "pig",
  "field3value" => "platypus"
];

// OUTPUT
$output = [
  "first" => "dinosaur",
  "second" => "pig",
  "third" => "platypus"
];

echo "<pre>";
print_r(array_combine($keys, $values));
?>