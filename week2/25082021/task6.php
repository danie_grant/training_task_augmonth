<?php
$data1 = [0, 1, 2, 3, 4, 5, 6];
echo "Data No.1 sum is = : " . array_sum($data1) . "<br>";

$data2 = ['0', '1', '2', '3', '4', '5', '6'];
echo "Data No.2 sum is = : " . array_sum($data2) . "<br>";

$data3 = [0, 1, 2, 3, 4, 5, 6, '0', '1', '2', '3', '4', '5', '6'];
echo "Data No.3 sum is = : " . array_sum($data3) . "<br>";

$data4 = [0, 1, 2, 3, 4, 5, 6, '0', '1', '2', '3', '4', '5', '6', 'a', 'b', 'c'];
echo "Data No.4 sum is = : " . array_sum($data4) . "<br>";
/* 
$sum = array_sum( $data1 );
print_r($sum);

echo "<pre>";
$sum = array_sum( $data2 );
print_r($sum);

echo "<pre>";
$sum = array_sum( $data3 );
print_r($sum);


echo "<pre>";
$sum = array_sum( $data4 );
print_r($sum); */
?>