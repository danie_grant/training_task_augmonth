<!DOCTYPE html>
<html>
<body>

<?php
echo "<br>";

//pi function
echo (pi());

//Minimum and maximam function
echo "<br>";
echo "<br>";

echo (min(0, 150, 30, 20, -8, -200)); // returns -200
echo "<br>";
echo "<br>";

echo (max(0, 150, 30, 20, -8, -200)); // returns 150

//absolute function
echo "<br>";
echo "<br>";
echo "<br>";
echo (abs(-6.7)); // returns 6.7

//square function
echo "<br>";

echo "<br>";
echo "<br>";
echo (sqrt(64)); // returns 8

//rounds function
echo "<br>";
echo "<br>";
echo (round(0.60)); // returns 1
echo (round(0.49)); // returns 0

//random function
echo "<br>";
echo "<br>";
echo (rand(10, 100));

?>

</body>
</html>
