<?php
    session_start(); 
    include 'database.php';
    if (isset($_POST['submit'])) 
    {
        $email_id = $_POST['email_id'];
        $password = $_POST['password'];
        $select = "select * from ragistration where email_id='$email_id' and password='$password'";
        $result = $conn->query($select);
        $row = $result->fetch_assoc();
        $pass=$row['password'];
        if($password=$pass){
            header('location:dashboard.php');
        } else{
            echo "Invalid Password";
        }
    }
?>
<html>
    <title>Login</title>
    <head>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <form method="POST" action="" enctype="multipart/form-data" align="center">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card mt-5">
                            <div class="card-body">
                                <h5 class="card-title font-weight-bold" style="text-align: center;">Login Form</h5>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="inputEmail">Email address</label>
                                        <input class="form-control" id="inputEmail" name="email_id" type="email" placeholder="name@example.com" />
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="inputPassword">Password</label>
                                        <input class="form-control" id="inputPassword" name="password" type="password" placeholder="Password" />
                                    </div>
                                        <button class="btn btn-primary form-control" type="submit" name="submit">Login</button>
                                </div>
                                <div class="card-footer text-center py-3">
                                        <div class="small"><a href="register.php">Need an account? Sign up!</a></div>
                                    </div>   
                            </div>
                        </div>      
                    </div> 
                </div> 
            </div>     
        </form>
    </body>
</html>