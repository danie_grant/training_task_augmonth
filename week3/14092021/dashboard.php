<?php
    session_start(); 
    include 'database.php';
     if (isset($_POST['logout'])) 
    {
        session_destroy();
        header("location:index.php");  
    }
?>
<!DOCTYPE html>
<html>
<title>Home Page</title>
<head>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.1/css/dataTables.bootstrap4.min.css">
    <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="assets/css/jquery.dataTables.min.css"> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
</head>
    <body class="sb-nav-fixed">
    <nav class="sb-top nav navbar navbar-expand navbar-dark bg-dark">
        <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <li>
                    <form method="POST" action="" enctype="multipart/form-data">
                        <button class="dropdown-item" type="submit" name="logout">Logout</button>
                    </form>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
    <form enctype="multipart/form-data">
        <h1 align="center">Welcome To Dashboard</h1>
        <table class="table" border="1px" align="center" id="example">
            <thead class="thead-dark" align="center">
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email Id</th>
                <th>Action</th>
            </thead>
            <tbody>
               <?php
                    $select = "select * from ragistration";
                    $result = $conn->query($select);
                    if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
		        ?>
                <tr align="center">
                    <td><?php echo $row['first_name']; ?></td>
                    <td><?php echo $row['last_name']; ?></td>
                    <td><?php echo $row['email_id']; ?></td>
                    <td><a class="btn btn-warning" href="edit.php?id=<?php echo $row['id']; ?>"><i class="fa fa-list"></i>Edit</a>
                    <a class="btn btn-danger" href="delete.php?id=<?php echo $row['id']; ?>"><i class="fa fa-trash"></i> Delete</a>
                        <!--<a href="edit.php?id=<?php echo $row['id']; ?>"><i class="fa fa-list" aria-hidden="true"></i> 
                            <a href="delete.php?id=<?php echo $row['id']; ?>"><i class="fa fa-trash" aria-hidden="true"></i> -->
                    </td>
                    </form>
                </tr>
                <?php }} else {
                    echo "<No Record Found";
                }?>
            </tbody>
        </table>
    </form>
    <script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <!-- <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
    <script src="js/datatables-simple-demo.js"></script> -->
</body>
</html>
