    <?php
    session_start();
    include 'database.php';
?>
<html>
    <title>Registration form</title>
    <head>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" />
    </head>
    <body>
    <div class="container mt-5">
        <?php if(isset($_SESSION['success'])){ ?>
            <div class="alert alert-success alert-dismissible" id="success">
            <?php echo $_SESSION['success']; ?>
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            </div>
            <?php }
                unset($_SESSION['success']);
            ?>
            <div class="card-header" style="background-color:rgb(101, 141, 56); color: honeydew; ">
                <h1 align="center"> Registration </h1>
            </div>
            <div class="card-body" style="border: 3px solid rgb(101, 141, 56); ">
                <form method="POST" autocomplete="off" enctype="multipart/form-data" action="registration_process.php">
                    <div class="form-group row ml-4">
                        <label for="first_name">First Name</label>
                        <input class="form-control" id="first_name" name="first_name" type="first_name" placeholder="please enter your first name" required/>
                    </div>
                    <div class="form-group row ml-4">
                        <label for="last_name">Last Name</label>
                        <input class="form-control" id="last_name" name="last_name" type="last_name" placeholder="please enter your last name" required/>
                    </div>
                    <div class="form-group row ml-4">
                        <label for="email_id">Email id</label>
                        <input class="form-control" id="email_id" name="email_id" type="email_id" placeholder="please enter your Email." required/>
                    </div>
                    <div class="form-group row ml-4">
                        <label for="password">Password</label>
                        <input class="form-control" id="password" name="password" type="password" placeholder="please enter your password" required/>
                    </div>
                    <div class="form-group row ml-4">
                        <label for="password">Confirm Password</label>
                        <input class="form-control" id="confirm_password" name="confirm_password" type="confirm_password" placeholder="please enter your Confirm Password" required/>
                    </div>
                    <div class="form-group row ml-4">
                        <label for="images">Images</label>
                        <input type="file" id="files" name="files[]" multiple id="images" />
                    </div>
                    <!-- button class="btn btn-primary btn-block" type="submit" name="save">Save</button> -->
                    <div class="form-group row ml-4">
                        <input class="btn btn-primary btn-block" type="submit" name="save" value="Save" id="save">
                    </div>
                </div>      
            </form>
        </div> 
    </body>
    <script>
        setTimeout(function () {
            // Closing the alert
            $('.alert').alert('close');
            }, 4000);
    </script>
</html>