<?php
session_start();
include 'database.php';
$id=$_GET['id'];

    $sql = "SELECT * FROM `ragistration` WHERE id='$id'";   

    $row = $conn->query($sql);
    $data=$row->fetch_assoc();
    if (isset($_POST['save'])) {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email_id = $_POST['email_id'];
        
        $pic = $_FILES['files']['name'];
	    $pic_tmp = $_FILES['files']['tmp_name'];

        $path = "assets/image/";
        $allowed_types = array('jpg', 'png', 'jpeg', 'gif');
        $maxsize = 2 * 1024 * 1024;
    
        foreach($_FILES['files']['name'] as $key=>$val){ 
            $file_tmpname = $_FILES['files']['tmp_name'][$key];
            $file_name = $_FILES['files']['name'][$key];
            $file_size = $_FILES['files']['size'][$key];
            $file_ext = pathinfo($file_name, PATHINFO_EXTENSION);
            $filename = uniqid().'.'.$file_ext;
    
            $sql_update = "UPDATE `ragistration` SET first_name='$first_name',last_name='$last_name',email_id='$email_id',images='$filename' WHERE id='$id'";
            //echo $sql_update;
            if($conn->query($sql_update)){
                move_uploaded_file($file_tmpname, $path.$filename);
                header('location:dashboard.php');
            } else {
                echo "Record Not Updated";
            }
        }
    }
?>
<html>
    <title>Edit Registration Form</title>
    <head>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" />
    </head>  
    <body>
        <div class="container mt-5">
            <div class="card-header" style="background-color:rgb(101, 141, 56); color: honeydew; ">
                <h1 align="center"> Edit Registration Data </h1>
            </div>
            <div class="card-body" style="border: 3px solid rgb(101, 141, 56); ">
                <form method="POST" autocomplete="off" enctype="multipart/form-data" action="">
                    <div class="form-group row ml-4">
                        <label for="first_name"> First Name <span> &nbsp;</span> </label>
                        <input class="col-sm-8" id="first_name" name="first_name" type="text" value="<?php echo $data['first_name']; ?>">
                    </div>
                    <div class="form-group row ml-4">
                        <label for="last_name"> Last Name <span> &nbsp;</span> </label>
                        <input class="col-sm-8" type="text" name="last_name" id="last_name" value="<?php echo $data['last_name']; ?>">
                    </div>
                    <div class="form-group row ml-4">
                        <label for="email_id"> Email Id <span> &nbsp;</span> </label>
                        <input class="col-sm-8" type="text" name="email_id" id="email_id" value="<?php echo $data['email_id']; ?>">
                    </div>
                    <div class="form-group row ml-4">
                        <label for="images"> Images <span> &nbsp;</span> </label>
                        <input type="file" id="files" name="files[]" id="images" value="<?php echo $data['images']; ?>" multiple/>
                    </div>
                    <div class="form-group row ml-5">
                        <input type="submit" name="save" class="btn btn-primary" value="Save" id="save">
                    </div>
                </form>
            </div>
        </div>
    </body>
    <script>
        setTimeout(function () {
            $('.alert').alert('close');
        }, 4000);
    </script>
</html>