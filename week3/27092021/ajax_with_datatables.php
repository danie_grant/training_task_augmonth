<?php
    session_start();
    include 'database.php';
?>

<html>
<title>Ajax Example</title>

    <head>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    </head>


    <body>
        <div class="container mt-5">
            <?php if(isset($_SESSION['success'])){ ?>
            <div class="alert alert-success alert-dismissible" id="success">
                <?php echo $_SESSION['success']; ?>
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            </div>
            <?php }
                unset($_SESSION['success']);
            ?>
            <div class="card-header" style="background-color:rgb(101, 141, 56); color: honeydew; ">
                <h1 align="center"> Ajax Demo </h1>
            </div>
            <div class="card-body" style="border: 3px solid rgb(101, 141, 56); ">
                <form id="f_Form" method="POST" autocomplete="off" enctype="multipart/form-data" action="process.php">
                    <h3 align="center">Ajax</h3>
                    <div class="form-group row ml-4">
                        <label for="first_name"> First Name <span> &nbsp;</span> </label>
                        <input class="col-sm-8" id="first_name" name="first_name" type="text" required>
                    </div>
                    <div class="form-group row ml-4">
                        <label for="last_name"> Last Name <span> &nbsp;</span> </label>
                        <input class="col-sm-8" type="text" name="last_name" id="last_name" required>
                    </div>
                    <div class="form-group row ml-5">
                        <input type="submit" name="save" class="btn btn-primary" value="Save" id="save">
                    </div>
                    <table class="table" id="example">
                        <thead align="center">
                            <tr>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                    $select = "select * from crud";
                                    $result = $conn->query($select);
                                    if ($result->num_rows > 0) {
                                    while($row = $result->fetch_assoc()) {
                                ?>
                                <tr align="center">
                                    <td><?php echo $row['first_name']; ?></td>
                                    <td><?php echo $row['last_name']; ?></td>
                                    <td><a href="edit.php?id=<?php echo $row['id']; ?>"><i class="fas fa-edit"></i>
                                        <a href="delete.php?id=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure to delete record?');"><i class="fas fa-trash"></i>
                                    </td>
                                    </form>
                                </tr>
                                <?php }} else {
                                    echo "<No Record Found";
                                }?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </body>
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
        setTimeout(function () {
            // Closing the alert
            $('.alert').alert('close');
            }, 4000);
    </script>
</html>