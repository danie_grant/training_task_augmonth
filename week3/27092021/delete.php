<?php
    session_start();
    include 'database.php';
    $id= $_GET['id'];
    
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "DELETE FROM crud WHERE id=$id";
    
    if ($conn->query($sql) === TRUE) {
        $_SESSION['success']="Data Deleted Successfully";
        header('location:ajax_with_datatables.php');
    } else {
        echo "Error deleting record: " . $conn->error;
    }
    
    $conn->close();
    

?>