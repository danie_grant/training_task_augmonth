<?php
    session_start(); 
    include 'database.php';
?>
<!DOCTYPE html>
<html>
    <title>Ajax Example</title>
    <head>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    </head>
    <body>  
        <div class="container mt-5">
            <div class="alert alert-success alert-dismissible" id="success" style="display:none;">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            </div>
            <div class="card-header " style="background-color:rgb(101, 141, 56); color: honeydew; ">
                <h1 align="center"> Ajax Demo </h1>
            </div>
            <div class="card-body" style="border: 3px solid rgb(101, 141, 56); ">
                <form id="f_Form" method="POST" action="process.php" autocomplete="off" enctype="multipart/form-data" >
                    <h3 align="center">Ajax</h3>
                    <div class="form-group">
                        <label for="first_name"> First Name : <span> </span> </label>
                        <input class="col-sm-8" id="first_name" name="first_name" type="text" required>
                    </div>
                    <div class="form-group">
                        <label for="last_name"> Last Name : <span> </span> </label>
                        <input class="col-sm-8" type="text" name="last_name" id="last_name" required>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="save" class="btn btn-primary" value="save" id="save">
                    </div>
                    <table class="table" id="example">
                        <thead>
                            <tr>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $select = "select * from crud";
                                $result = $conn->query($select);
                                if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                            ?>
                            <tr align="center">
                                <td><?php echo $row['first_name']; ?></td>
                                <td><?php echo $row['last_name']; ?></td>
                                <td><a href="edit.php?id=<?php echo $row['id']; ?>"><i class="fa fa-list" aria-hidden="true"></i>
                                    <a href="delete.php?id=<?php echo $row['id']; ?>"><i class="fa fa-trash" aria-hidden="true"></i>
                                </td>
                                </form>
                            </tr>
                            <?php }} else {
                                echo "<No Record Found";
                            }?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
        $('#example').DataTable();
                $('#save').on('click', function() {
                    //$('#save').attr("disabled", "disabled");
                    var first_name = $('#first_name').val();
                    var last_name = $('#last_name').val();
                    if (first_name != "" && last_name != "") {
                        $.ajax({
                            url: "index.php",
                            type: "POST",
                            data: {
                                first_name: first_name,
                                last_name: last_name,
                            },
                            cache: false,
                            success: function(dataResult) {
                                var dataResult = JSON.parse(dataResult);
                                if (dataResult.statusCode == 200) {
                                    $("#save").removeAttr("disabled");
                                    $('#f_Form').find('input:text').val('');
                                    $("#success").show();
                                    $('#success').html('Data added successfully !');
						            //$('#success').html('Data added successfully !'); 						
                                    //$('#success').html('Data added successfully !');
                                    setTimeout(function() {
                                        $('#success').alert('close');
                                        }, 4000);
                                    } else if (dataResult.statusCode == 201) {
                                    alert("Error occured !");
                                }
                            }
                        });
                    } else {
                        alert('Please fill all the field !');
                    }
                });
            });
        </script>
    </body>
</html>
  