<!DOCTYPE html>
<html>
	<title>Insert data in MySQL database using Ajax</title>
<head>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<body>
<div style="margin: auto;width: 60%;">
	<div class="alert alert-success alert-dismissible" id="success" style="display:none;">
	  <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	</div>
	<form class="mt-5" id="first_Form" name="form" method="POST">
		<div class="form-group">
			<label for="first_name">First Name:</label>
			<input type="text" class="form-control" id="first_name" placeholder="first name" name="first_name">
		</div>
		<div class="form-group">
			<label for="last_name">Last Name:</label>
			<input type="text" class="form-control" id="last_name" placeholder="last Name" name="last_name">
		</div>
		<input type="submit" name="save" class="btn btn-primary" value="Save to database" id="save">
	</form>
</div>

<script>
$(document).ready(function() {
	$('#save').on('click', function() {
		$("#save").attr("disabled", "disabled");
		var first_name = $('#first_name').val();
		var last_name = $('#last_name').val();
		
		if(first_name!="" && last_name!="" ){
			$.ajax({
				url: "index1.php",
				type: "POST",
				data: {
					first_name: first_name,
					last_name: last_name,
				},
				cache: false,
				success: function(dataResult){
					var dataResult = JSON.parse(dataResult);
					if(dataResult.statusCode==200){
						$("#butsave").removeAttr("disabled");
						$('#first_Form').find('input:text').val('');
						$("#success").show();
						$('#success').html('Data added successfully !'); 						
					}
					else if(dataResult.statusCode==201){
					   alert("Error occured !");
					}
					
				}
			});
		}
		else{
			alert('Please fill all the field !');
		}
	});
});
</script>
</body>
</html>
  