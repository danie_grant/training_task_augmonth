<?php
    session_start();
    include 'database.php';

    $id=$_GET['id'];

    $sql = "select * from crud where id='$id'"; 

    $row = $conn->query($sql);
    $data=$row->fetch_assoc();
    if (isset($_POST['save'])) {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
    
        $sql_update="UPDATE `crud` SET `first_name`='$first_name',`last_name`='$last_name' WHERE id='$id'";
        
        if($conn->query($sql_update)){
            $_SESSION['success']="Data Updated Successfully";
            header("location:index.php");
        }
        else{
            echo "Record Not Updated";
        }
    }
?>

<html>
    <title>Edit data</title>  
    <head>   
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    </head>
    <body>
    <div class="container mt-5">
        <div class="card-header" style="background-color:rgb(101, 141, 56); color: honeydew; ">
            <h1 align="center"> Edit Ajax Demo </h1>
        </div>
        <div class="card-body" style="border: 3px solid rgb(101, 141, 56); ">
        <form id="f_Form" method="POST" autocomplete="off" enctype="multipart/form-data" action="">
                <div class="form-group row ml-4">
                    <label for="first_name"> First Name <span> &nbsp;</span> </label>
                    <input class="col-sm-8" id="first_name" name="first_name" type="text" value="<?php echo $data['first_name']; ?>">
                </div>
                <div class="form-group row ml-4">
                    <label for="last_name"> Last Name <span> &nbsp;</span> </label>
                    <input class="col-sm-8" type="text" name="last_name" id="last_name" value="<?php echo $data['last_name']; ?>">
                </div>
                <div class="form-group row ml-5">
                    <input type="submit" name="save" class="btn btn-primary" value="Save" id="save">
                </div>
            </form>
        </div>
    </div>
    </body>
    <script>
        setTimeout(function () {
            $('.alert').alert('close');
        }, 4000);
    </script>
</html>

