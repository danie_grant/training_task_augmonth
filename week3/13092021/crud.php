<?php
    include 'database.php';
?>
    <html>
    <title>Crud</title>

    <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/jquery.dataTables.min.css"> 
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>   
    </head>

    <body class="bg-success">
        <form method="POST" action="process_crud.php" align="center" enctype="multipart/form-data">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card mt-5">
                        <div class="card-body">
                            <h5 class="card-title font-weight-bold" style="text-align: center;">Registration Form</h5>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label>Name</label>
                                        <input class="form-control" type="text" id="name"  name="name"><br>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label>Village</label>
                                        <input class="form-control" type="text" id="village"  name="village">
                                </div>
                                <input class="btn btn-primary form-control" type="submit" name="save" value="submit" id="save">
                            </div>
                        </div>
                    </div>  
                </div>
            </div> 
        </div> <br><br>
        <table class="table" border="1px" align="center" id="example">
            <thead class="thead-dark" align="center">
                <th>Name</th>
                <th>village</th>
                <th>Action</th>
            </thead>
            <tbody>
               <?php
                    $select = "select * from javascript_registration";
                    $result = $conn->query($select);
                    if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
		        ?>
                <tr align="center">
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo $row['village']; ?></td>
                    <td><a href="edit.php?id=<?php echo $row['id']; ?>"><i class="fa fa-list" aria-hidden="true"></i>
                        <a href="delete.php?id=<?php echo $row['id']; ?>"><i class="fa fa-trash" aria-hidden="true"></i>
                    </td>
                    </form>
                </tr>
                <?php }} else {
                    echo "<No Record Found";
                }?>
            </tbody>
        </table>
    </body>
    <script>
        $(document).ready(function() {
        $('#example').DataTable();
    } );
    </script>
</html>