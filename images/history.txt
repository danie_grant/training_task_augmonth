The Diamonds Majestic Collection is a unique collection of one-of-a-kind designer jewelery created by one of the worlds most talented goldsmiths.

This rare collection showcases beautiful handmade designer necklaces, pendant, rings and earrings featuring rare precious gems set with brilliant diamonds in 18ct gold.

Diamonds Majestic jewelery features only the finest natural gems.

Free consultations may be arranged with your award winning jewelery designer to answer all your jewelery questions, guiding you with your jewelery design ideas and transforming them into a stunning jewelery masterpiece to enjoy and admire for the rest of your life.

This site also offers clients a FREE ebook entitled "Symbols of Love and Power" linking mans desire to give the worlds finest jewelery as proof of enduring love to the magnitude of his wealth and power.

Imagine yourself wearing brilliant diamonds and lustrous pearls, rare precious gems, rubies, sapphires, emeralds or opal accessories set in gold and studded with diamonds.

Your unique designer rings, elegant necklace, beautiful earrings and one-of-a-kind jewelery accessories will arouse admiration and envy from both men and women alike.

