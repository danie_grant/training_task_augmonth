<?php
  session_start(); 
  include 'database.php';

  if (isset($_POST['sign_in'])) 
  {
    $email_id = $_POST['email_id'];
    $password = $_POST['password'];

    $select = "SELECT * from user where v_email='$email_id' And v_password='$password'";
    
    $result = $conn->query($select) or die($conn->error);
    $row = $result->fetch_assoc();
    
    if(!empty($row))
    {
      $pass=$row['v_password'];

      if($password=$pass)
      {
        $_SESSION['user']=$row['v_email'];
        echo "<script>alert('Login Successfully')</script>";
		    echo "<script type='text/javascript'>document.location='dashboard.php';</script>";
      }
    } else {
        echo '<script>alert("Your Login Email Address or Password is invalid") </script>' ;
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <title> Log in</title>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  </head>
  <style>
    body {
      font-family: Arial, Sans-serif;
    }
    .error {
        color:red;
        font-family:verdana, Helvetica;
    }
  </style>
  <body class="hold-transition login-page">
    <div class="login-box">
      <!-- /.login-logo -->
      <div class="card card-outline card-primary">
        <div class="card-header text-center">
          <a href="index.php" class="h1"><b>Admin</b></a>
        </div>
        <div class="card-body">
          <form method="POST" enctype="multipart/form-data" action="" id="login_form" name="login_form">
            <div class="input-group mb-3">
              <input type="email" class="form-control" placeholder="Please Enter Your Email" id="email_id" name="email_id">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" class="form-control" placeholder="Please Enter Your Password" id="password" name="password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-block" id="sign_in" name="sign_in">Sign In</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
          <!-- /.social-auth-links -->
          <p class="mb-0 mt-2">
            <center><a href="registration.php" class="text-center">Register a new membership</a></center>
          </p>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- validation jquery -->
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
    <script>
      $(function()
        {          
          $("form#login_form").validate(
          {
            rules: 
            {
              email_id: 
              {
                required: true,
                email:true,
              },
              password: 
              {
                required: true,
                minlength: 5,

              },
              message: 
              {
                maxlength: 1024
              }
            },
            messages: 
            {
              email_id: 
              {
                required: "Please Enter Your Email Id."
              },
              password: 
              {
                required: "Please Your Password."
              },
              message: 
              {
                maxlength: jQuery.format("Please limit the message to {0} letters!")
              }
            }
          });	
      });
    </script>
  </body>
</html>
