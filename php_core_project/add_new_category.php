<?php 
  session_start(); 
  include 'database.php';
  if(empty($_SESSION))
  {
    header('location:index.php');
  }
   
    if(isset($_POST['category_success']))
    {
        $category_name = $_POST['v_category_name'];
        $qty = $_POST['i_qty'];
        $status = $_POST['v_status'];

        $photo = explode(".", $_FILES['v_category_image']['name']);
        $pic = round(microtime(true)) . '.' . end($photo);

        $pic_tmp = $_FILES['v_category_image']['tmp_name'];

        $path = "dist/images/";
        $path = $path.$pic;
        
        $type = strtolower(pathinfo($pic,PATHINFO_EXTENSION));
        $arr = array("jpeg","png","jpg");

        $select="SELECT * from category where v_category_name='$category_name'";
        //echo $select;
        $result = $conn->query($select) or die($conn->error);
        $row = $result->fetch_assoc();
    
        if($row>0)
        {
            echo '<script>alert("Category Already Exist!") </script>';
            echo "<script type='text/javascript'> document.location='add_new_category.php'; </script>";
        } else if(in_array($type,$arr)) {
            $sql = mysqli_query($conn,"INSERT INTO category(v_category_image,v_category_name,i_qty,v_status) VALUES ('$pic','$category_name','$qty','$status')");
            move_uploaded_file($pic_tmp, $path);
            echo "<script>alert('Category has been added Successfully')</script>";
            echo "<script type='text/javascript'> document.location='category.php'; </script>";
        } else {
            echo "<script>alert('Allowed file formats .jpg, .jpeg and .png.')</script>";
            echo "<script type='text/javascript'>document.location='add_new_category.php';</script>";
        } 
    }
?>
<!DOCTYPE html>
<html lang="en">
    <title> Admin Panel Create New Category </title>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- bootstrap -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-2.2.3.js" type="text/javascript"></script>
        <!-- DataTables -->
        <link href="https://cdn.datatables.net/1.10.1/css/jquery.dataTables.css" rel="stylesheet">
        <script src="https://cdn.datatables.net/1.10.1/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <link href="https://cdn.datatables.net/responsive/1.0.0/css/dataTables.responsive.css" rel="stylesheet">
        <script src="https://cdn.datatables.net/responsive/1.0.0/js/dataTables.responsive.js" type="text/javascript"></script>
    </head>
    <style>
        .user-panel img {
            height: auto;
            width: 1.8rem;
        }
        body {
            font-family: Arial, Sans-serif;
        }
        .error {
            color:red;
            font-family:verdana, Helvetica;
        }  
    </style>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>
                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <!-- Navbar Search -->
                    <div class="user-panel mt-1 mb-1 d-flex">
                        <div class="image">
                            <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block" style="color:blue;">
                                <?php echo $_SESSION['user'];?>
                            </a>
                        </div>
                    </div>
                </ul>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="index.php" class="brand-link">
                    <img src="dist/img/store.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                    <span class="brand-text font-weight-light">Store Management</span>
                </a>
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item menu-open">
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="dashboard.php" class="nav-link">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p>Dashboard</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="category.php" class="nav-link">
                                        <i class="nav-icon fas fa-list-alt"></i>
                                        <p>Category</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="product.php" class="nav-link">
                                        <i class="nav-icon fas fa-th"></i>
                                        <p>Products</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="logout.php" class="nav-link" id="logout">
                                        <i class="nav-icon fas fa-user-alt"></i>
                                        <p>Logout</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </aside>
        </div>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h2 class="m-0" style="text-align:center mr-5">Category</h2>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header"><h3 class="text-center font-weight-light my-2">Create New Category</h3></div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <form method="POST" action="" enctype="multipart/form-data" id="category_form" name="category_form">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-floating mb-3">
                                                    <label for="v_category_photo">Category Image</label>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="v_category_image" name="v_category_image" onchange="preview()">
                                                        <center><img id="image_view" src="" width="150px" /></center>
                                                        <label class="custom-file-label" for="v_category_photo" id="v_category_photo">Please Select Category Image</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-floating">
                                                    <label for="v_category_name">Category Name</label>
                                                    <input class="form-control" id="v_category_name" type="text" name="v_category_name" placeholder="Enter your Category Name"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-floating">
                                                    <label for="i_qty"> Qty</label>
                                                    <input class="form-control" id="i_qty" type="number" name="i_qty" placeholder="Enter your Qty"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-floating">
                                                    <label for="v_status">Status</label>
                                                    <select class="form-control" name="v_status" id="v_status" onchange="changeMessage(this)">
                                                        <option value="">Select Status</option>
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>
                                                    </select>
                                               </div>
                                            </div><br><br><br><br>
                                        </div>
                                        <div class="mt-4 ml-2">
                                            <button class="btn btn-success" type="submit" name="category_success" id="category_success">Save</button>
                                            <button class="btn btn-secondary"><a href="category.php" style="color:inherit"> Back </a></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <!-- /.container-fluid -->
            </section>
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        <!-- jQuery -->
        <script src="plugins/jquery/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- overlayScrollbars -->
        <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/adminlte.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="dist/js/demo.js"></script>
        <!-- DataTables  & Plugins -->
        <script src="plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
        <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
        <script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
        <script src="plugins/jszip/jszip.min.js"></script>
        <script src="plugins/pdfmake/pdfmake.min.js"></script>
        <script src="plugins/pdfmake/vfs_fonts.js"></script>
        <script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
        <script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
        <script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
        <!-- validation jquery -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
        <script type="text/javascript">
            $("#v_category_image").change(function() {
                $("#v_category_photo").text(this.files[0].name);
            });
            function preview() {
                image_view.src = URL.createObjectURL(event.target.files[0]);
            }
            function changeMessage(v_status) {
                var selectedText = v_status.options[v_status.selectedIndex].innerHTML;
                var selectedValue = v_status.value;
            }
        </script>
        <!-- validation jquery -->
        <script type="text/javascript">
            $(function()
            {          
                $("form#category_form").validate(
                {
                    rules: 
                    {
                        v_category_image: 
                        {
                            required: true,
                        },
                        v_category_name: 
                        {
                            required: true,
                        },
                        i_qty: 
                        {
                            required: true,
                        },
                        v_status: 
                        {
                            required: true,
                        },
                        message: 
                        {
                            maxlength: 1024
                        }
                    },
                    messages: 
                    {
                        v_category_name: 
                        {
                            required: "Please Enter Your Category Name."
                        },
                        v_category_image: 
                        {
                            required: "Please Select Your Category Image."
                        },
                        i_qty: 
                        {
                            required: "Please Enter Your Category Qty."
                        },
                        v_status: 
                        {
                            required: "Please Select Your Category Status."
                        },
                        message: 
                        {
                            maxlength: jQuery.format("Please limit the message to {0} letters!")
                        }
                    }
                });	
            }); 
        </script>
    </body>
</html>