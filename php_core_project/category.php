<?php 
  session_start(); 
  include 'database.php';
  if(empty($_SESSION))
  {
    header('location:index.php');
  }
?> 

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Admin Panel Category </title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-2.2.3.js" type="text/javascript"></script>
    <!-- DataTables -->
    <link href="https://cdn.datatables.net/1.10.1/css/jquery.dataTables.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.1/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <link href="https://cdn.datatables.net/responsive/1.0.0/css/dataTables.responsive.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/responsive/1.0.0/js/dataTables.responsive.js" type="text/javascript"></script>
  </head>
  <style>
    .user-panel img {
    height: auto;
    width: 1.8rem;
    }
    thead input {
        width: 100%;
    }
    select.form-control{
    display: inline;
    width: 200px;
    margin-left: 25px;
  }
  .bold{
    font-family:"Arial Black";
    background-color:"#708090";
  }
  .table td, .table th {
        font-size: 20px;
    }
  
  </style>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      <!-- Preloader -->
      <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
      </div>
      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
          </li>
        </ul>
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Navbar Search -->
          <div class="user-panel mt-1 mb-1 d-flex">
            <div class="image">
              <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
            <a href="#" class="d-block" style="color:blue;"><?php echo $_SESSION['user'];?></a>
            </div>
          </div>
        </ul>
      </nav>
      <!-- /.navbar -->
      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="index.php" class="brand-link">
          <img src="dist/img/store.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
          <span class="brand-text font-weight-light">Store Management</span>
        </a>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item menu-open">
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="dashboard.php" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>Dashboard</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="category.php" class="nav-link">
                    <i class="nav-icon fas fa-list-alt"></i>
                    <p>Category</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="product.php" class="nav-link">
                  <i class="nav-icon fas fa-th"></i>
                    <p>Products</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="logout.php" class="nav-link" id="logout">
                    <i class="nav-icon fas fa-user-alt"></i>
                    <p>Logout</p>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </aside>
    </div>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Category</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
      <!-- Main content -->
      <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <form method="POST" autocomplete="off" enctype="multipart/form-data" action="add_new_category.php">
                <i class="fas fa-table">&nbsp;</i>Category Table
                <button type="submit" class="btn btn-primary float-right">Add Category</button>
                </form>
              </div><br>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table stripe my-table table-hover" style="width:100%">
                  <thead style="text-align: center">
                  <tr style="background-color: #708090;">
                    <th>Id</th>
                    <th>Image</th>
                    <th>Category Name</th>
                    <th> Qty</th>
                    <th>No of Product</th>
                     <th>Status</th>
                    <th>Created Date</th>
                    <th>Modified Date</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php
                      //$select ="select * from category";
                      $select ="SELECT c.*, COUNT(i_cat_id) As i_no_of_product FROM category c LEFT JOIN tbl_relation_category_product ON c.i_id=tbl_relation_category_product.i_cat_id GROUP BY c.i_id;
                      ";
                      
                      $result=$conn->query($select);
                      if ($result->num_rows > 0) {
                      while ($row = $result->fetch_assoc()) {
                    ?>
                      <tr align="center">
                        <td><?php echo $row['i_id'];?></td>
                        <td><img src="dist/images/<?php echo $row['v_category_image']; ?>" height="70px;"></td>
                        <td><?php echo $row['v_category_name']; ?></td>
                        <td><?php echo $row['i_qty']; ?></td>
                        <td><?php echo $row['i_no_of_product'];?></td>
                        <td><?php echo $row['v_status'] == 1 ? "Active": "Inactive";?></td>
                        <td><?php echo $row['created_date'];?></td>
                        <td><?php echo $row['modified_date'];?></td>
                        <td><a href="category_edit.php?i_id=<?php echo $row["i_id"]; ?>" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                            <a href="category_delete.php?i_id=<?php echo $row['i_id']; ?>" class="btn btn-danger" onclick="return confirm('Are You Sure Want to delete Category?')"><i class="fas fa-trash"></i></a>
                        </td>
                      </tr>
                    <?php }} else {
                      echo "No Record Found";
                    } ?>
                  </tbody>
                  <tfoot style="text-align: center">
                    <tr>
                      <th>Id</th>
                    <th>Image</th>
                    <th>Category Name</th>
                    <th> Qty</th>
                    <th>No of Product</th>
                     <th>Status</th>
                    <th>Created Date</th>
                    <th>Modified Date</th>
                    <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
          <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
      </div>
    </div>
    <!-- ./wrapper -->
    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="plugins/jszip/jszip.min.js"></script>
    <script src="plugins/pdfmake/pdfmake.min.js"></script>
    <script src="plugins/pdfmake/vfs_fonts.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
      $("#v_photo").change(function(){
        $("#v_category_photo").text(this.files[0].name);
      });
      $(document).ready(function() {
        $("table.my-table").DataTable({
          "responsive": true, "searching": true,"lengthChange": true, "autoWidth": true,
          "dom": "lBfrtip",
          "buttons": [{
            "exportOptions": {
              "format": {
                "header": function(content, index) {
                  return index === 8 ? "Status" : content;
                }
              }
            }
          }],
          "initComplete": function() {
            this.api().column(5).every(function() {
              var column = this;
              var select = $('<select class="form-control input-sm bold"><option value="">Status All</option></select>')
                .appendTo($(column.header()).empty())
                .on('change', function() {
                  var val = $.fn.dataTable.util.escapeRegex(
                    $(this).val()
                  );
                  column
                    .search(val ? '^' + val + '$' : '', true, false)
                    .draw();
                });
              column.data().unique().sort().each(function(d, j) {
                select.append("<option value='" + d + "'>" + d + "</option>")
              });
            });
          },
          "columnDefs": [{
            targets: [5],
            orderable: false
          }]
        });
      });
    </script>
  </body>
</html>