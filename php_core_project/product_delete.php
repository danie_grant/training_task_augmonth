<?php
	session_start();
	include 'database.php';

	$id=$_GET['i_id'];
	
	if ($conn->connect_error) 
	{
		die("Connection failed:" . $conn->connect_error);
	}
	$sql = "DELETE FROM Product WHERE i_id=$id";
	if ($conn->query($sql) === TRUE) {
		echo "<script>alert('Product Delete Successfully')</script>";
		echo "<script type='text/javascript'> document.location ='product.php'; </script>";

	} else {
		echo "Error deleting record: " . $conn->error;
	}
	$conn->close();
?>