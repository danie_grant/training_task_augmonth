-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2021 at 06:22 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store_managment`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `i_id` int(10) NOT NULL,
  `v_category_image` varchar(200) NOT NULL,
  `v_category_name` varchar(200) NOT NULL,
  `i_qty` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified_date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `v_status` int(1) NOT NULL DEFAULT 0 COMMENT '1=Active, 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`i_id`, `v_category_image`, `v_category_name`, `i_qty`, `created_date`, `modified_date`, `v_status`) VALUES
(1, '61c94ad659e0.jpg', 'Electronic', 1, '2021-12-27 05:01:02', '2021-12-27 05:14:00', 1),
(2, '1640581283.jpg', 'Laptop', 1, '2021-12-27 05:01:23', '2021-12-27 05:01:23', 1),
(3, '1640582078.jpg', 'Sony TV', 1, '2021-12-27 05:01:54', '2021-12-27 05:14:38', 1),
(4, '1640581337.jpg', 'mobile', 1, '2021-12-27 05:02:16', '2021-12-27 05:02:16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `v_product_image` varchar(255) NOT NULL,
  `v_status` int(1) NOT NULL DEFAULT 0 COMMENT '0=Inactive,1=Active',
  `i_product_id` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `v_product_image`, `v_status`, `i_product_id`) VALUES
(1, '61c9490ba6be1.jpg', 1, 1),
(2, '61c9490bbc95b.jpg', 0, 1),
(6, '61c94a9727199.jpg', 1, 4),
(7, '61c94a973f58e.jpg', 0, 4),
(8, '61c94a975a50a.jpg', 0, 4),
(9, '61c94ad659e0b.jpg', 1, 5),
(10, '61c94ad66b329.jpg', 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `i_id` int(10) UNSIGNED NOT NULL,
  `v_product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `v_product_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `i_price` int(11) NOT NULL,
  `i_sale_price` int(11) NOT NULL,
  `i_qty` int(11) NOT NULL,
  `v_product_status` int(1) NOT NULL DEFAULT 0 COMMENT '0 = Inactive , 1= Active',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`i_id`, `v_product_name`, `v_product_code`, `i_price`, `i_sale_price`, `i_qty`, `v_product_status`, `created_at`, `updated_at`) VALUES
(1, 'Redmi note pro 8', 'HAOCLKI', 16000, 17000, 1, 1, '2021-12-27 05:03:07', '2021-12-27 05:03:07'),
(4, 'Dell Inspiron', 'RPJAB1Y', 40000, 46000, 1, 1, '2021-12-27 05:09:43', '2021-12-27 05:09:43'),
(5, 'Sony Ultra HD', 'BTA2EDR', 40000, 41000, 1, 1, '2021-12-27 05:10:46', '2021-12-27 05:16:26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_relation_category_product`
--

CREATE TABLE `tbl_relation_category_product` (
  `id` int(10) NOT NULL,
  `i_cat_id` int(11) NOT NULL,
  `i_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_relation_category_product`
--

INSERT INTO `tbl_relation_category_product` (`id`, `i_cat_id`, `i_product_id`) VALUES
(1, 1, 1),
(2, 4, 1),
(5, 2, 4),
(9, 1, 5),
(10, 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `i_id` int(11) NOT NULL,
  `v_fname` varchar(250) NOT NULL,
  `v_lname` varchar(250) NOT NULL,
  `v_email` varchar(250) NOT NULL,
  `v_password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`i_id`, `v_fname`, `v_lname`, `v_email`, `v_password`) VALUES
(1, 'DEV', 'GOSIYA', 'admin@gmail.com', 'admin'),
(2, 'TEST', 'DEMO', 'test@gmail.com', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `tbl_relation_category_product`
--
ALTER TABLE `tbl_relation_category_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`i_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `i_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `i_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_relation_category_product`
--
ALTER TABLE `tbl_relation_category_product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `i_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
