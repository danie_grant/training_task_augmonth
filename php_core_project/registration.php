<?php
  session_start();
  include 'database.php';
  
  if (isset($_POST['reg_success'])) 
  {
    $v_fname = $_POST['v_fname'];
    $v_lname = $_POST['v_lname'];
    $v_email = $_POST['v_email'];
    $v_password =$_POST['v_password'];
    
    if($_POST["v_password"] === $_POST['conform_password'])
    {
      $select = "SELECT * from user where v_email='$v_email'";
      $data=$conn->query($select);
      $row=mysqli_num_rows($data);

      if($row>0)
      {
        echo '<script>alert("Email Already  Exist") </script>';
      } else {
        $sql = mysqli_query($conn, "INSERT INTO user(v_fname,v_lname,v_email,v_password)
        VALUES('$v_fname','$v_lname','$v_email','$v_password')");
        header('location:index.php');        
      } 
    }
    echo '<script>alert("Your Login password or conform Password is invalid") </script>';
  }
?>
    
<!DOCTYPE html>
<html lang="en">
  <title> User Registration</title>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
  </head>
  <style>
    body {
      font-family: Arial, Sans-serif;
    }
    .error {
        color:red;
        font-family:verdana, Helvetica;
    }
  </style>
  <body class="hold-transition register-page">
    <div class="register-box">
      <div class="card card-outline card-primary">
        <div class="card-header text-center">
          <a href="#" class="h1"><b>Admin</b></a>
        </div>
        <div class="card-body">
          <p class="login-box-msg">Register a new membership</p>
          <form method="POST" action="" enctype="multipart/form-data" id="registration_form" name="registration_form">
            <div class="input-group mb-3">
              <input type="text" class="form-control" placeholder="Please Enter First name" id="v_fname" name="v_fname" onkeyup="keyup()">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="text" class="form-control" placeholder="Please Enter Last name" id="v_lname" name="v_lname" onkeyup="keyup()">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="email" class="form-control" placeholder="Please Enter Your Email" id="email_id" name="v_email">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" class="form-control" placeholder="Please Enter Your Password" id="v_password" name="v_password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" class="form-control" placeholder="Confirm password" id="conform_password" name="conform_password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-block" id="reg_success" name="reg_success">Register</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
          <center><a href="index.php" class="text-center">I already have a membership</a></center>
        </div>
        <!-- /.form-box -->
      </div><!-- /.card -->
    </div>
    <!-- /.register-box -->
    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- validation jquery -->
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
    <script>
      function keyup() {
        var x = document.getElementById("v_fname");
        var y = document.getElementById("v_lname");
        x.value = x.value.toUpperCase();
        y.value = y.value.toUpperCase();
      }
    </script>
    <!-- validation jquery -->
    <script type="text/javascript">
      $(function()
      {          
        $("form#registration_form").validate(
        {
          rules: 
          {
            v_fname: 
            {
                required: true,
            },
            v_lname: 
            {
              required: true,
            },
            v_email: 
            {
              required: true,
              email:true,
            },
            v_password: 
            {
              required: true,
            },
            conform_password: 
            {
              required: true,
            },
            message: 
            {
              maxlength: 1024
            }
          },
          messages: 
          {
            v_fname: 
            {
              required: "Please Enter Your First Name."
            },
            v_lname: 
            {
              required: "Please Enter Your Last Name."
            },
            v_email: 
            {
              required: "Please Enter Your Email Id."
            },
            v_password: 
            {
              required: "Please Enter Your Password."
            },
            conform_password: 
            {
              required: "Please Enter Your Confirm Password."
            },
            message: 
            {
              maxlength: jQuery.format("Please limit the message to {0} letters!")
            }
          }
        });	
      }); 
    </script>
  </body>
</html>