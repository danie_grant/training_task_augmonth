<?php 
    session_start(); 
    include 'database.php';
    if(empty($_SESSION))
    {
        header('location:index.php');
    }
    $i_id=$_GET['i_id'];

    $category="SELECT i_cat_id FROM `tbl_relation_category_product` WHERE i_product_id='".$_GET['i_id']."'";
    $cat_data=$conn->query($category);
    $category_data=array();
    while($d=$cat_data->fetch_assoc()){
        $category_data[]=$d;
    } 
    foreach($category_data as $keys => $category_2)
    {
        $cat_selected[$keys]=$category_2['i_cat_id'];
    }
    
    $sql="select i_id,v_category_name from category";
    $row=$conn->query($sql);
    while($val=$row->fetch_assoc())
    {
        $category_show[]=$val;
    }

    $sql="select * from product where i_id='$i_id'";
    $row=$conn->query($sql);
    $data=$row->fetch_assoc();
   
    if (isset($_POST['product_success'])) 
    {
        $product_name = $_POST['v_product_name'];
        $price = $_POST['i_price'];
        $sale_price =$_POST['i_sale_price'];
        $qty = $_POST['i_qty'];
        $status = $_POST['v_product_status'];

        $pic = $_FILES['v_product_image']['name'];
        $pic_tmp = $_FILES['v_product_image']['tmp_name'];

        $path = "dist/images/";
        $arr = array('jpg', 'png', 'jpeg');
        $maxsize = 2 * 1024 * 1024;
        
        foreach($cat_selected as $del_id=>$del_cat_id)
        {
            $cat_new_update=$conn->query("DELETE FROM `tbl_relation_category_product` WHERE i_cat_id=$del_cat_id");
        }
        
        foreach($_POST['i_cat_id'] as $key1 => $val1)
        {
            $sql1=$conn->query("INSERT INTO `tbl_relation_category_product`(`i_cat_id`,`i_product_id`) VALUES ('$val1','$i_id')");
        }

        foreach($_FILES['v_product_image']['name'] as $key=>$val)
        {
            
            $file_tmpname = $_FILES['v_product_image']['tmp_name'][$key];
            $file_name = $_FILES['v_product_image']['name'][$key];
            $file_size = $_FILES['v_product_image']['size'][$key];
            $file_ext = pathinfo($file_name, PATHINFO_EXTENSION);
            $filename = rand().'.'.$file_ext;
            

            if(in_array($file_ext, $arr))
            {
                $insert = $conn->query("INSERT INTO images(v_product_image,i_product_id,v_status) VALUES('$filename','$i_id',0)");
                move_uploaded_file($file_tmpname, $path.$filename);
                echo "<script>alert('Product has been Update Successfully')</script>";
                echo "<script type='text/javascript'> document.location='product.php'; </script>";
            } else {
                echo "<script>alert('Allowed file formats .jpg, .jpeg and .png.')</script>";
                echo "<script type='text/javascript'>document.location='product.php';</script>";
            }
        }
        
        $sql_update=$conn->query("UPDATE `product` SET `v_product_name`='$product_name',`i_price`='$price',`i_sale_price`='$sale_price',`i_qty`='$qty',`v_product_status`='$status' WHERE i_id='$i_id'");
        
    } 
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Admin Panel Product </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- bootstrap -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-2.2.3.js" type="text/javascript"></script>
        <!-- DataTables -->
        <link href="https://cdn.datatables.net/1.10.1/css/jquery.dataTables.css" rel="stylesheet">
        <script src="https://cdn.datatables.net/1.10.1/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <link href="https://cdn.datatables.net/responsive/1.0.0/css/dataTables.responsive.css" rel="stylesheet">
        <script src="https://cdn.datatables.net/responsive/1.0.0/js/dataTables.responsive.js" type="text/javascript"></script>
         <!-- Select2 CSS -->
         <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    </head>
    <style>
        .user-panel img {
            height: auto;
            width: 1.8rem;
        }
        body {
            font-family: Arial, Sans-serif;
        }
        .error {
            color:red;
            font-family:verdana, Helvetica;
        }
        .select2 {
            width: 100%!important;
        }
        .select2.select2-container .select2-selection--multiple .select2-selection__choice {
            background-color: #C0C0C0;
            font-size :16px;
        }
        .container {
            max-width: 450px;
        }
        .imgGallery img {
            padding: 8px;
            max-width: 100px;
        }    
    </style>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
            </div>
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>
                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                    <!-- Navbar Search -->
                    <div class="user-panel mt-1 mb-1 d-flex">
                        <div class="image">
                            <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block" style="color:blue;">
                                <?php echo $_SESSION['user'];?>
                            </a>
                        </div>
                    </div>
                </ul>
            </nav>
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="index.php" class="brand-link">
                    <img src="dist/img/store.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                    <span class="brand-text font-weight-light">Store Management</span>
                </a>
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item menu-open">
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="dashboard.php" class="nav-link">
                                        <i class="nav-icon fas fa-tachometer-alt"></i>
                                        <p>Dashboard</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="category.php" class="nav-link">
                                        <i class="nav-icon fas fa-list-alt"></i>
                                        <p>Category</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="product.php" class="nav-link">
                                        <i class="nav-icon fas fa-th"></i>
                                        <p>Products</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="logout.php" class="nav-link" id="logout">
                                        <i class="nav-icon fas fa-user-alt"></i>
                                        <p>Logout</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </aside>
        </div>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header"><h3 class="text-center font-weight-light my-2">Edit Product</h3></div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <form method="POST" action="" enctype="multipart/form-data" id="product_edit_form" name="product_edit_form">
                                        <div class="row">
                                        <div class="col-md-6">
                                                <div class="form-floating">
                                                    <label>Category Name select</label>
                                                    <select class="category form-control" name="i_cat_id[]" id="i_cat_id" multiple>
                                                        <option label="Please Select Category"></option>
                                                        <?php 
                                                        foreach($category_show as $k=>$cat){
                                                        ?> 
                                                        <option value="<?php echo $cat['i_id'];?>"<?php echo $cat['i_id']=in_array($cat['i_id'],$cat_selected) ? "selected" : ""; ?>><?php echo $cat['v_category_name']?></option>
                                                        <?php } ?>
                                                    </select>
                                                    </div>
                                                </div>                     
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3">
                                                        <label for="v_product_name">Product Name</label>
                                                        <input class="form-control" id="v_product_name" type="text" name="v_product_name" placeholder="Enter your Product Name" value="<?php echo $data['v_product_name'];?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating">
                                                        <label for="i_price">Price</label>
                                                        <input class="form-control" id="i_price" type="number" name="i_price" placeholder="Enter your Price" value="<?php echo $data['i_price'];?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3">
                                                        <label for="i_sale_price">Sale Price</label>
                                                        <input class="form-control" id="i_sale_price" type="number" name="i_sale_price" placeholder="Enter your Sale Price" value="<?php echo $data['i_sale_price'];?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3">
                                                        <label for="i_qty">Qty</label>
                                                        <input class="form-control" id="i_qty" type="number" name="i_qty" placeholder="Enter your Qty" value="<?php echo $data['i_qty'];?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating">
                                                        <label for="v_product_status">Status</label>
                                                        <select class="form-control" name="v_product_status" id="v_product_status" onchange="changeMessage(this)">
                                                            <option value="1" <?php echo $data['v_product_status'] == "Active" ? "selected": "";?>>Active</option>
                                                            <option value="0" <?php echo $data['v_product_status'] == "Inactive" ? "selected": "";?>>Inactive</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-floating mb-3">
                                                        <label for="v_product_photo">Product Image</label>
                                                        <div class="custom-file">
                                                            <?php 
                                                            include 'database.php';
                                                            //echo $i_id;exit;
                                                            $sql="SELECT * FROM images WHERE i_product_id='$i_id'";

                                                            $row2=$conn->query($sql);
                                                            while($data2=$row2->fetch_assoc())
                                                            {
                                                            ?> 
                                                            <input type="file" name="v_product_image[]" class="custom-file-input" id="v_product_image" multiple>
                                                            <label class="custom-file-label" for="v_product_photo" id="v_product_photo">Please Select Product Image</label>
                                                            <center>
                                                            <?php 
                                                            $image_id = $data2['id'];
                                                            $product_id = $data2['i_product_id'];
                                                            $stat = $data2['v_status'];
                                                             if((int)$data2['v_status'] != 1)
                                                             { 
                                                            ?>
                                                                <!--<a href="product_edit.php?i_id=$i_id='.$data2['i_id'].'" class="btn btn-success">Active</a>-->
                                                                 <a href="change_image_status.php?i_id=<?php echo $image_id?>&p_id=<?php echo $product_id?>" class="btn btn-success">Active</a>
                                                                <?php
                                                             } 
                                                            ?>
                                                            <img class='mt-2' id="image_view" src="dist/images/<?php echo $data2['v_product_image'];?>" width="100px;" height="100px;">&nbsp;&nbsp;<a href="image_delete.php?id=<?php echo $data2['id']; ?>" class="btn btn-danger" onclick="return confirm('Are You Sure Want to delete Images?')">Delete</a></center>
                                                            <?php }
                                                            ?>
                                                            <!-- Image preview -->
                                                            <div class="user-image text-center">
                                                                <div class="imgGallery"> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-4 ml-2">
                                                <button class="btn btn-success" type="submit" name="product_success" id="product_success">Save</button>
                                                <button class="btn btn-secondary"><a href="product.php" style="color:inherit"> Back </a></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <!-- /.container-fluid -->
            </section>
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        <!-- jQuery -->
        <script src="plugins/jquery/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- overlayScrollbars -->
        <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/adminlte.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="dist/js/demo.js"></script>
        <!-- DataTables  & Plugins -->
        <script src="plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
        <script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
        <script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
        <script src="plugins/jszip/jszip.min.js"></script>
        <script src="plugins/pdfmake/pdfmake.min.js"></script>
        <script src="plugins/pdfmake/vfs_fonts.js"></script>
        <script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
        <script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
        <script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
        <!-- Select2 CSS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
        <!-- validation jquery -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
             $("#i_cat_id").select2({
                placeholder: "Select Category Name",
                allowClear: true
            });
            $("#v_product_image").change(function() {
                $("#v_product_photo").text(this.files[0].name);
            });
            function preview() {
                image_view.src = URL.createObjectURL(event.target.files[0]);
            }
            function changeMessage(v_status) 
            {
                var selectedText = v_status.options[v_status.selectedIndex].innerHTML;
                var selectedValue = v_status.value;
            }
        });
        </script>
        <!-- Validation -->
        <script type="text/javascript">
            $(function()
            {          
                $("form#product_edit_form").validate(
                {
                    rules: 
                    {
                        "i_cat_id[]" : 
                        {
                            required: true,
                        },
                        "v_product_name": 
                        {
                            required: true,
                        },
                        "i_price": 
                        {
                            required: true,
                        },
                        "i_sale_price": 
                        {
                            required: true,
                        },
                        "i_qty": 
                        {
                            required: true,
                        },
                        "v_product_status": 
                        {
                            required: true,
                        },
                        message: 
                        {
                            maxlength: 1024
                        }
                    },
                    messages: 
                    {
                        "i_cat_id[]" : 
                        {
                            required: "Please Select Category."
                        },
                        "v_product_name": 
                        {
                            required: "Please Enter Your Product Name."
                        },
                        "i_price": 
                        {
                            required: "Please Enter Your Price."
                        },
                        "i_sale_price": 
                        {
                            required: "Please Enter Your Sale Price."
                        },
                        "i_qty": 
                        {
                            required: "Please Enter Your Qty."
                        },
                        "v_product_status": 
                        {
                            required: "Please Select Your Product Status."
                        },
                        message: 
                        {
                            maxlength: jQuery.format("Please limit the message to {0} letters!")
                        }
                    }
                });	
            }); 
        </script>
            <!-- Multiple images preview with JavaScript -->
        <script>
            $(function() 
            {
                var multiImgPreview = function (input, imgPreviewPlaceholder) 
                {
                    if (input.files) {
                        var filesAmount = input.files.length;
                        for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
                        reader.onload = function (event) {
                            $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                        }
                        reader.readAsDataURL(input.files[i]);
                        }
                    }
                };
                    $('#v_product_image').on('change', function () {
                    multiImgPreview(this, 'div.imgGallery');
                });
            });
        </script>
    </body>
</html>