<?php
    session_start();
    include 'database.php';

    if (isset($_SESSION['user'])) 
    {
        unset($_SESSION['user']);
        echo "<script>alert('Logout Successfully')</script>";
        echo "<script type='text/javascript'>document.location='index.php'; </script>";        
    }
?>
