<?php
    class test
    {
        private $name;

        function demo(){
            echo "Hello ";
        }
    }
    class testing extends test
    {
        function demo(){
            echo "Welcome ";
        }
    }
    class test_demo extends test
    {
        function demo()
        {
            echo "User ";
        }
    }

    $data = new test();
    $data1 = new testing();
    $data2 = new test_demo();
    
    $data->demo();
    $data1->demo();
    $data2->demo();
    
    echo "<br>";
    $data=$data1;
    echo $data->demo();

?>