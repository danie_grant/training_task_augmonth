<?php
class database
{
    private $db_host = "localhost";
    private $db_user = "root";
    private $db_password = "";
    private $db_name = "crud";
    private $mysqli="";
    private $result = array();
    private $conn = false;
    
    public function __construct()
    {
        if(!$this->conn)
        {
            $this->mysqli = new mysqli($this->db_host,$this->db_user,$this->db_password,$this->db_name);
            if($this->mysqli->connect_error)
            {
                array_push($this->result,$this->mysqli->connect_error);
                return false;

            }
        } else {
            return true;

        }
    }

    public function insert($table,$params=array())
    {
        if($this->tbl_exist($table))
        {
            $tbl_column = implode(', ',array_keys($params));
            $tbl_value = implode("', '",$params);
            $sql="INSERT INTO $table($tbl_column) Values('$tbl_value')";
           
            if($this->mysqli->query($sql))
            {
                array_push($this->result,$this->mysqli->insert_id);
                return true;
            } else {
                array_push($this->result,$this->mysqli->error);
                return false;
            }
        }else {
            return false;
        }
    }


    public function sql($sql)
    {
        $query = $this->mysqli->query($sql);
        if($query)
        {
            $this->result = $query->fetch_all(MYSQLI_ASSOC);
            return true;
        }else{
            array_push($this->result,$this->mysqli->error);
            return false;
        }
    }
    public function get_data()
    {
        $val = $this->result;
        $this->result = array();
        return $val;
    }

    private function tbl_exist($table)
    {
        $sql="SHOW tables from $this->db_name LIKE '$table'";
        $tbl_db=$this->mysqli->query($sql);
        if($tbl_db)
        {
            if($tbl_db->num_rows == 1)
            {
                return true;
            } else{
                array_push($this->result,$table." Not Exist In database");
                return false;
            }
        }
    }
    public function __destruct()
    {
        if($this->conn)
        {
            if($this->mysqli->close())
            {
                $this->conn = false;
                return true;
            }
        } else {
            return false;
        }
    }
}
?>