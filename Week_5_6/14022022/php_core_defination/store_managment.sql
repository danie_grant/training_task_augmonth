-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2022 at 02:48 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store_managment`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `i_id` int(10) NOT NULL,
  `v_category_image` varchar(200) DEFAULT NULL,
  `v_category_name` varchar(200) NOT NULL,
  `i_cat_qty` int(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified_date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `v_status` int(1) NOT NULL COMMENT '1=Active,0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`i_id`, `v_category_image`, `v_category_name`, `i_cat_qty`, `created_date`, `modified_date`, `v_status`) VALUES
(1, '1644928387.jpeg', 'Electronic', 3, '2022-02-15 12:33:08', '2022-02-15 12:33:08', 1),
(2, '1644928402.jpeg', 'Tablet', 1, '2022-02-15 12:33:22', '2022-02-15 12:33:22', 0),
(3, '1644928434.jpeg', 'Laptop', 1, '2022-02-15 12:33:54', '2022-02-15 12:33:54', 1),
(4, '1644928451.jpeg', 'mobile', 1, '2022-02-15 12:34:11', '2022-02-15 13:19:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `v_product_image` varchar(255) DEFAULT NULL,
  `v_status` int(1) NOT NULL DEFAULT 0 COMMENT '0=Inactive,1=Active',
  `i_product_id` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `v_product_image`, `v_status`, `i_product_id`) VALUES
(1, '620b9e4f82395.jpeg', 0, 1),
(2, '620b9ede951e4.jpeg', 0, 2),
(3, '620b9f8c18470.jpeg', 0, 3),
(4, '620b9f8c1d3ba.jpeg', 0, 3),
(6, '620ba00acd30b.jpeg', 0, 4),
(8, '620ba074e8147.jpeg', 0, 5),
(9, '620ba5b0a52b2.jpeg', 0, 6),
(10, '620ba6616c0ce.jpeg', 0, 7),
(11, '620ba8b25521f.jpeg', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `i_id` int(10) UNSIGNED NOT NULL,
  `v_product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `v_product_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `i_price` int(11) NOT NULL,
  `i_sale_price` int(11) NOT NULL,
  `i_qty` int(11) NOT NULL,
  `v_product_status` int(1) NOT NULL DEFAULT 0 COMMENT '0 = Inactive , 1= Active',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`i_id`, `v_product_name`, `product_image`, `v_product_code`, `i_price`, `i_sale_price`, `i_qty`, `v_product_status`, `created_at`, `updated_at`) VALUES
(1, 'Tablet', '1644928591.jpeg', 'NVCBDY0', 11000, 13000, 1, 1, '2022-02-15 12:36:31', '2022-02-15 12:36:31'),
(2, 'Lenovo G480', '1644928734.jpeg', 'CD3H5NM', 30000, 33000, 1, 0, '2022-02-15 12:38:54', '2022-02-15 12:38:54'),
(3, 'Dell Inspiron', '1644928907.jpeg', 'KPYR6OS', 12000, 14000, 3, 1, '2022-02-15 12:41:48', '2022-02-15 13:28:38');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_relation_category_product`
--

CREATE TABLE `tbl_relation_category_product` (
  `id` int(10) NOT NULL,
  `i_cat_id` int(11) NOT NULL,
  `i_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_relation_category_product`
--

INSERT INTO `tbl_relation_category_product` (`id`, `i_cat_id`, `i_product_id`) VALUES
(1, 1, 1),
(2, 3, 2),
(4, 1, 4),
(5, 3, 5),
(6, 3, 6),
(7, 4, 7),
(12, 1, 3),
(13, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `i_id` int(11) NOT NULL,
  `v_fname` varchar(250) NOT NULL,
  `v_lname` varchar(250) NOT NULL,
  `v_email` varchar(250) NOT NULL,
  `v_password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`i_id`, `v_fname`, `v_lname`, `v_email`, `v_password`) VALUES
(1, 'dev', 'gosiya', 'admin@gmail.com', 'admin'),
(2, 'test', 'demo', 'test@gmail.com', 'admin'),
(5, 'rajesh', 'shah', 'rajesh@gmail.com', '12345'),
(6, 'VICTORIA', 'HALE', 'victoriahale.cis@gmail.com', '111111'),
(12, 'mellissa', 'petra', 'mellissapetra.cis@gmail.com', '123456'),
(13, 'v1', 'v', 'v1@gmail.com', '111'),
(14, 'v1', 'v', 'v2@gmail.com', '111111'),
(15, 'c4', 'c', 'v3@gg.com', '111'),
(17, 'Gosiya', 'Devashi', 'testing@gmail.com', 'Admin@123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `tbl_relation_category_product`
--
ALTER TABLE `tbl_relation_category_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`i_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `i_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `i_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_relation_category_product`
--
ALTER TABLE `tbl_relation_category_product`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `i_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
