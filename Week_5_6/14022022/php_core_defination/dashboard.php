<?php
  session_start(); 
  include 'database.php';
  if(empty($_SESSION))
  {
    header('location:index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include 'inc/head.php'; ?>
  </head>
  <body>
    <?php include 'inc/navbar.php'; ?>
    <div class="container mt-5">
      <?php if(isset($_SESSION['success'])){ ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong><?php echo $_SESSION['success']; ?></strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php } unset($_SESSION['success']); ?>
      <?php if(isset($_SESSION['danger'])){ ?>
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><?php echo $_SESSION['danger']; ?></strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php } unset($_SESSION['danger']); ?>
      <div class="row">
        <div class="col-md-4">
          <div class="card text-white bg-info mb-3" >
            <div class="card-body">
              <h3 class="card-title">Category <span class="badge badge-secondary"><?php $result = $conn->query("select count('i_id') as category_count FROM category");
                $category_count =$result->fetch_assoc();
                echo $category_count['category_count'];  ?></span></h3>
            </div>
            <a href="" class="d-flex float-left btn btn-info card-footer">More Details</a>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-white bg-info mb-3" >
            <div class="card-body">
              <h3 class="card-title">Products <span class="badge badge-secondary"><?php $result_data = $conn->query("select count('i_id') as product_count FROM product"); $product_count = $result_data->fetch_assoc(); echo $product_count['product_count']; ?></span></h3>
            </div>
            <a href="" class="d-flex float-left btn btn-info card-footer">More Details</a>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card text-white bg-info mb-3" >
            <div class="card-body">
              <h3 class="card-title">User<span class="badge badge-secondary">
                <?php
                  $result1 = $conn->query("select count('i_id') as user_count FROM user");
                  $user_count =$result1->fetch_assoc();
                  echo $user_count['user_count']; 
                ?></span></h3>
            </div>
            <a href="" class="d-flex float-left btn btn-info card-footer">More Details</a>
          </div>
        </div>
      </div>
      <table class="table my-table table-hover" style="width:100%">
        <center><h1>All User Information</h1></center>
        <thead style="text-align: center">
          <tr style="background-color: #708090;">
            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email Id</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $select ="Select * from user";
            $result = $conn->query($select);
            if ($result->num_rows > 0) {
            while($row = mysqli_fetch_array($result)){
          ?>
          <tr align="center">
            <td><?php echo $row['i_id'];?></td>
            <td><?php echo $row['v_fname']; ?></td>
            <td><?php echo $row['v_lname']; ?></td>
            <td><?php echo $row['v_email'];?></td>
          </tr>
          <?php } } ?>
        </tbody>
        <tfoot style="text-align: center">
          <tr style="background-color: #708090;">
            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email Id</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <footer class="bg-dark fixed-bottom" style="color:white;">
      <?php include 'inc/footer.php'; ?>
    </footer>
  </body>
</html>