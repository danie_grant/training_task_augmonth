<?php

abstract class demo {
	abstract function data();
}
class child extends demo {
	function data() {
		echo "Demo class";
	}
}

$show = new child;
$show->data();
?>
