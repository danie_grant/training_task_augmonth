<?php
interface Demo {
  public function show();
}

class Child implements Demo {
  public function show() {
    echo "Hello";
  }
}

$test = new Child();
$test->show();
?>