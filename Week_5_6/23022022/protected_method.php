<?php 
class Customer
{
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    protected function set()
    {
        return ucwords($this->name);
    }

    public function getName()
    {
        return $this->set($this->name);
    }
}
    $data = new Customer('Testing Demo');
    echo $data->getName(); 
?>