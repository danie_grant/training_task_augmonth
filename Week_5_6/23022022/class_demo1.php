<?php

class Alert 
{
    var $user;
    var $alertType;

    function __construct($username, $typeOfAlert) 
    {
        $this->user = $username;
        $this->alertType = $typeOfAlert;
    }

    function alertPopUp() {
        echo $this->user . ". Welcome";
    }
    function alertMe() {
    if($this->alertType == "PopUp")
        $this->alertPopUp();
    }
}

$user1 = new Alert("Hello", "PopUp");
$user1->alertMe();

?>