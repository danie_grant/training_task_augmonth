<!DOCTYPE html>
<html lang="en">
    <title>Dashboard</title>
    <head>
        <?php include 'inc/head.php';?>
    </head>
    <body>
        <?php include 'inc/navbar.php';?>
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-4">
                    <div class="card text-white bg-info mb-3" >
                        <div class="card-body">
                            <h3 class="card-title">User <span class="badge badge-secondary">10</span></h3>
                        </div>
                        <a href="" class="d-flex float-left btn btn-info card-footer">More Details</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card text-white bg-info mb-3" >
                        <div class="card-body">
                            <h3 class="card-title">Category <span class="badge badge-secondary">10</span></h3>
                        </div>
                        <a href="" class="d-flex float-left btn btn-info card-footer">More Details</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card text-white bg-info mb-3" >
                        <div class="card-body">
                            <h3 class="card-title">Product <span class="badge badge-secondary">10</span></h3>
                        </div>
                        <a href="" class="d-flex float-left btn btn-info card-footer">More Details</a>
                    </div>
                </div>
            </div>
        </div>
        <footer class="bg-dark fixed-bottom" style="color:white;">
            <?php include 'inc/footer.php'; ?>
        </footer>
    </body>
</html>