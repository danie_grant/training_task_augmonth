<!DOCTYPE html>
<html lang="en">
    <title>Category</title>
    <head>
        <?php include 'inc/head.php';?>
    </head>
    <body>
        <?php include 'inc/navbar.php';?>
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card text-white mb-3" >
                        <div class="card-body">
                        <a class="btn btn-success" href="manage_category.php?action=insert">Add Category</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="bg-dark fixed-bottom" style="color:white;">
            <?php include 'inc/footer.php'; ?>
        </footer>
    </body>
</html>