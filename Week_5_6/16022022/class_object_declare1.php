<?php
    class City
    {
        public function set_name($address)
        {
            $this->address=$address;
        }
        
        public function get_name()
        {
            return $this->address;
        }
    }

    $rajkot = new City();
    $morbi = new City();
    $junagadh = new City();
    
    $rajkot->set_name('Rajkot');
    $morbi->set_name('Morbi');
    $junagadh->set_name('Junagadh');

    echo $rajkot->get_name();
    echo "<br>";
    echo $morbi->get_name();
    echo "<br>";
    echo $junagadh->get_name();
?>