<?php

    $data = [
        [
        'name' => 'Paresh',
        'email' => 'paresh@gmail.com'
        ],
        [
        'name' => 'Rakesh',
        'email' => 'rakesh@gmail.com'
        ],
        [
        'name' => 'Naresh',
        'email' => 'naresh@gmail.com'
        ],
    ];

    $names = array_column($data, 'name');
    $emails = array_map(function ($arr) {return $arr['email'];}, $data);

    echo "<pre>";
    print_r($names);
    print_r($emails);
    
echo "<pre>";
$demo = [
    [
        'team_name' => 'Web Designers',
        'employee_name' => [
            [
                'name' => 'raj',
                'age'  => '28'
            ],
            [
                'name' => 'mohan',
                'age'  => '24'
            ],
            [
                'name' => 'viram',
                'age'  => '21'
            ]
        ]
        
    ],
    [
        'team_name' => 'Web Developers',
        'employee_name' => [
            [
                'name' => 'amit',
                'age'  => '15'
            ],
            [
                'name' => 'ravi',
                'age'  => '19'
            ],
            [
                'name' => 'Het',
                'age'  => '21'
            ]
        ]
    ]
];
print_r($demo);
?>