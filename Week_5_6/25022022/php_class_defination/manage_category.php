<!DOCTYPE html>
<html lang="en">
    <title>Category</title>
    <head>
        <?php include 'inc/head.php';?>
    </head>
    <body>
        <?php include 'inc/navbar.php'; ?>
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card text-white mb-3" >
                        <div class="card-body">
                        <a class="btn btn-success" href="manage_category.php?action=insert">Add Category</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <h5 class="card-header">Category</h5>
                <div class="card-body">
                    <form method="POST" action="" enctype="multipart/form-data" id="category_form" name="category_form">
                        <div class="row">
                            <div class="col-md-6">
                                <div>
                                    <label>Category Name</label>
                                    <input class="form-control" id="v_category_name" type="text" name="v_category_name" placeholder="Enter Your Category Name"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div>
                                    <label>Category Qty</label>
                                    <input class="form-control" id="v_category_name" type="text" name="v_category_name" placeholder="Enter Your Category Name"/>
                                </div>
                            </div>
                            <div class="col-md-6 mt-3">
                                <div>
                                   <label>Category Status</label>
                                    <input class="form-control" id="v_category_name" type="text" name="v_category_name" placeholder="Enter Your Category Name"/>
                                </div>
                            </div>
                            <div class="col-md-6 mt-3">
                                <div>
                                    <label>Category Image</label>
                                    <input class="form-control" type="file" placeholder="Please Select Category Image" onchange="preview()">
                                    <center><img class="mt-4" id="image_view" src="" width="100px;"/></center>
                                </div>
                            </div>
                            <div class="mt-4 ml-2">
                                <button class="btn btn-success" type="submit" name="category_success" id="category_success">Save</button>
                                <button class="btn btn-secondary"><a href="#" style="color:inherit"> Back </a></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <footer class="bg-dark fixed-bottom" style="color:white;">
            <?php include 'inc/footer.php'; ?>
        </footer>
        <script>
            function preview() 
            {
                image_view.src = URL.createObjectURL(event.target.files[0]);
            }
        </script>
    </body>
</html>