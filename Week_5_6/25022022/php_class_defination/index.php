<?php 
?>

<!DOCTYPE html>
<html lang="en">
  <title>Login</title>
  <head>
    <?php include 'inc/head.php'; ?>    
  </head>

  <body>
    <?php if(isset($_GET['action']) && $_GET['action']!=null && $_GET['action'] == "register") { ?>
    <div class="container mt-5">
      <div class="card bg-light mb-3">
        <div class="card-header">Registration</div>
        <div class="card-body">
          <form method="POST" enctype="multipart/form-data" action="" id="registration_form" name="registration_form">
            <div class="form-group">
              <label for="exampleInputEmail1">First Name</label>
              <input type="text" class="form-control" id="v_fname" name="v_fname" placeholder="Please Enter First Name">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Last Name</label>
              <input type="text" class="form-control" id="v_lname" name="v_lname" placeholder="Please Enter Last Name">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="email" class="form-control" id="v_email" name="v_email" placeholder="Please Enter Your Email Id">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" id="v_password" name="v_password" placeholder="Please Enter Your Password">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Confirm Password</label>
                  <input type="password" class="form-control" id="conform_password" name="conform_password" placeholder="Enter Your Conform Password">
            </div><br>
            <button type="submit" class="btn btn-primary" id="register" name="register">Registration</button>
            <a href="index.php?action=login" class="btn btn-primary" id="sign_in" name="sign_in">Already Have Account Login Now</a>
          </form>
        </div>
      </div>
      <?php } else { ?>
      <div class="container mt-5">
        <div class="card bg-light mb-3">
          <div class="card-header">Login</div>
          <div class="card-body">
            <form method="POST" enctype="multipart/form-data" action="" id="login_form" name="login_form">
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="email" name="email">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="password" name="password">
              </div><br>
              <button type="submit" class="btn btn-primary" id="sign_in" name="sign_in">Login</button>
              <a href="index.php?action=register" class="btn btn-primary">Not Have Account Register Now</a>
            </form>
          </div>
        </div>
      </div> <?php } ?>
    </div>
  </body>
</html>