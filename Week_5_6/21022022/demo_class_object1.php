<?php 
class Test
{
    static $storage = array();
    
    public static function set($key, $value)
    {
      self::$storage[$key] = $value;
    }
    
    public static function get($key)
    {
      return self::$storage[$key];
    }
}
  Test::set('["name"]', 'Hello');
  echo Test::get('["name"]');
  echo "<br>";
  Test::set('["array"]', 'Welcome');
  echo Test::get('["array"]'); 
?>