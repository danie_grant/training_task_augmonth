<?php
    session_start();    
    include 'database.php';
?>
<html>
    <title>Employee Example</title>
    <head>
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <script src="assets/css/jquery.min.js"></script>
        <script src="assets/css/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand mb-3" href="index.php">
                <img src="assets/images/303.jpg" alt="logo" style="width:60px;">
            </a>            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="index.php" style="color:white;">Home</a>
                    <a class="nav-item nav-link" href="series.php" style="color:white;">Series</a>
                    <a class="nav-item nav-link" href="Pattern.php" style="color:white;">Pattern</a>
                    <a class="nav-item nav-link" href="sum.php" style="color:white;">Sum</a>
                    <a class="nav-item nav-link" href="query.php" style="color:white;">My Sql Query</a>
                    <a class="nav-item nav-link" href="employee_info.php" style="color:white;">Emp Data</a>
                </div>
            </div>
        </nav><br>
        <div class="col-md-12 alert alert-success mt-5" id="alert"></div>
        <div class="col-md-12">
            <h3 style="text-align:center;">Add Employee Data </h3>
            <div class="col-md-1" style="float: right;">
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal">Add Record</button>
            </div><br><br>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Add New Record</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <form method="POST" autocomplete="off" enctype="multipart/form-data">
                            <div class="modal-body">
                                <label for="name">Emp Name <span> </span> </label>
                                <input class="form-control" id="emp_name" name="emp_name" type="text" required>
                            </div>
                            <div class="modal-body">
                                <label for="name">Emp Position <span> </span> </label>
                                <input class="form-control" id="emp_position" name="emp_position" type="text" required>
                            </div>
                            <div class="modal-body">
                                <label for="salary">Emp Salary <span> </span> </label>
                                <input class="form-control" id="emp_salary" name="emp_salary" type="number" required>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success" name="save" value="save" id="save">Save</button>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="modal fade" id="editmodel" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Edit Record</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <form method="POST" autocomplete="off" enctype="multipart/form-data">
                            <div class="modal-body">
                                <input class="form-control" id="edit_id" name="edit_id" type="hidden" hidden>
                                <label for="edit_name">Emp Name <span> </span> </label>
                                <input class="form-control" id="edit_name" name="edit_name" type="text" value="">
                            </div>
                            <div class="modal-body">
                                <label for="edit_position"> Emp Position <span> </span> </label>
                                <input class="form-control" id="edit_position" name="edit_position" type="text" value="">
                            </div>
                            <div class="modal-body">
                                <label for="edit_salary"> Emp Salary <span> </span> </label>
                                <input class="form-control" id="edit_salary" name="edit_salary" type="number" value="">
                            </div>
                            
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success" name="edit_btn" id="edit_btn">Save</button>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
        </div>
        <div id="data_tables"><?php include_once('get_data.php'); ?></div>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#alert").hide();
                $('#save').on('click', function(e) {
                    e.preventDefault();
                    var emp_name=$('#emp_name').val();
                    var emp_position=$('#emp_position').val();
                    var emp_salary=$('#emp_salary').val();
                    $.ajax({
                        type: "POST",
                        url: "insert_data.php",
                        data: { 
                            emp_name:emp_name,
                            emp_position:emp_position,
                            emp_salary:emp_salary,
                        },
                        success: function(data) {
                            $('#myModal').modal('hide');
                            $('#alert').html(data).fadeIn('slow');
                            $('#alert').delay(1500).fadeOut('slow');
                            $('.modal-backdrop').remove();
                            employee_info();
                        }
                    });
                });
            });
        
            function employee_info()
            {
                $('#data_tables').load('get_data.php');
            }

            $(document).on('click', '#delete', function(){
                if (confirm('Are You Sure Delete Record?')) {
                    $id=$(this).val();
                        $.ajax({
                        type: "POST",
                        url: "delete.php",
                        data: {
                            id: $id,
                        },
                        success: function(data){
                            $('#alert').html(data).fadeIn('slow');
                            $('#alert').delay(1500).fadeOut('slow');
                            employee_info();
                        }
                    });
                }
            });

            $(document).on('click', '#btn_edit', function(event){
                event.preventDefault();
                id=$(this).data('id');
                //console.log(id);
                    $.ajax({
                    type: "POST",
                    url: "edit_data.php",
                    data: {
                        id: id,
                    },
                    dataTyepe:"json",
                    success: function(data){
                        data = JSON.parse(data);
                        $("input#edit_name").val(data.emp_name);
                        $("input#edit_position").val(data.emp_position);
                        $("input#edit_salary").val(data.emp_salary);
                        $("input#edit_id").val(id);
                    }
                });
            });
            
            $(document).on('click','#edit_btn',function(event){
                event.preventDefault();
                var id=$('#edit_id').val();
                var edit_name=$('#edit_name').val();
                var edit_position=$('#edit_position').val();
                var edit_salary=$('#edit_salary').val();
                $.ajax({
                    type: "POST",
                    url: "update_data.php",
                    data: {
                        id:id,
                        edit_name:edit_name,
                        edit_position:edit_position,
                        edit_salary:edit_salary,
                    },
                    success: function(data){
                        $('#editmodel').hide();
                        $('.modal-backdrop').remove();
                        $("input#emp_name").val(data);
                        $("input#emp_position").val(data);
                        $("input#emp_salary").val(data);
                        $("input#edit_id").val(id);
                        $('#alert').html(data).fadeIn('slow');
                        $('#alert').delay(1500).fadeOut('slow');
                        employee_info();
                    },
                });
            });
        </script>
        <footer class="py-5 bg-dark fixed-bottom" style="color:white;">
            <div class="container">
                <div class="row">
                    <div class="container px-4" style="font-size:20px;"><br>
                        <p class="m-0 text-center text-white">Copyright 1999-2021 by Refrences Data. All Rights Reserved. My Testing Project is Powered by</p>
                    </div>
                </div>
            </div>
        </footer> 
    </body>
</html>