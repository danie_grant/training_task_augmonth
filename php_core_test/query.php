<html>
    <title>Series</title>
    <head>
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <script src="assets/css/jquery.min.js"></script>
        <script src="assets/css/bootstrap.min.js"></script>
    </head>
    <style>
        .faqHeader {
            font-size: 27px;
            margin: 20px;
        }
        
        .panel-heading [data-toggle="collapse"]:after {
            font-family: 'Glyphicons Halflings';
            content: "\e072";
            /* "play" icon */
            float: right;
            color: #F58723;
            font-size: 18px;
            line-height: 22px;
            /* rotate "play" icon from > (right arrow) to down arrow */
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);
        }
        
        .panel-heading [data-toggle="collapse"].collapsed:after {
            /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);
            color: #454444;
        }
    </style>
    <body>
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand mb-3" href="index.php">
                <img src="assets/images/303.jpg" alt="logo" style="width:60px;">
            </a>            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="index.php" style="color:white;">Home</a>
                    <a class="nav-item nav-link" href="series.php" style="color:white;">Series</a>
                    <a class="nav-item nav-link" href="Pattern.php" style="color:white;">Pattern</a>
                    <a class="nav-item nav-link" href="sum.php" style="color:white;">Sum</a>
                    <a class="nav-item nav-link" href="query.php" style="color:white;">My Sql Query</a>
                    <a class="nav-item nav-link" href="employee_info.php" style="color:white;">Emp Data</a>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="panel-group" id="accordion">
                <div class="faqHeader" style="text-align:center">PHP Query</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Q.1 Write a query to fetch the EmpFname from the EmployeeInfo table in the upper case and use the ALIAS name as EmpName.</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body"> <strong>Ans : </strong> SELECT upper(emp_first_name) AS Empname from employeeinfo;
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Q.2 Write a query to fetch the number of employees working in the department ‘HR’.</a>
                            </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse">
                            <div class="panel-body">
                            <strong>Ans : </strong>SELECT COUNT(*) FROM employeeinfo WHERE department="HR";
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">Q.3 Write a query to get the current date.</a>
                            </h4>
                        </div>
                        <div id="collapseEleven" class="panel-collapse collapse">
                            <div class="panel-body">
                            <strong>Ans : </strong>SELECT CURRENT_DATE();
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Q.4 Write a query to retrieve the first four characters of  EmpLname from the EmployeeInfo table.</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body"> <strong>Ans : </strong>SELECT substring(emp_last_name,1,4) FROM employeeinfo;
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Q.5 Write a query to fetch only the place name(string before brackets) from the Address column of the EmployeeInfo table.</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                    <strong>Ans : </strong> 
                            
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Q.6 Write a query to create a new table that consists of data and structure copied from the other table.</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <strong>Ans : </strong>CREATE TABLE emp_data SELECT * FROM employeeinfo;
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Q.7 Write q query to find all the employees whose salary is between 50000 to 100000.</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <strong>Ans : </strong>SELECT * FROM `employeeposition` WHERE salary BETWEEN 50000 AND 100000;
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">Q.8 Write a query to find the names of employees that begin with ‘S’</a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse">
                            <div class="panel-body">
                            <strong>Ans : </strong> SELECT emp_first_name FROM employeeinfo WHERE emp_first_name LIKE 's%';
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">Q.9 Write a query to fetch top N records.</a>
                            </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse">
                            <div class="panel-body">
                            <strong>Ans : </strong> SELECT * FROM `employeeposition` ORDER BY salary DESC LIMIT 10;
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Q.10 Write a query to retrieve the EmpFname and EmpLname in a single column as “FullName”. The first name and the last name must be separated with space.</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                            <strong>Ans : </strong> SELECT CONCAT(emp_first_name, ' ', emp_last_name) As FULLName FROM employeeinfo;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><br><br><br><br>
        <footer class="py-3 bg-dark fixed-bottom" style="color:white;">
            <div class="container">
                <div class="row">
                    <div class="container px-4" style="font-size:20px;"><br>
                        <p class="m-0 text-center text-white">Copyright 1999-2021 by References Data. All Rights Reserved. My Testing Project is Powered by</p>
                    </div>
                </div>
            </div>
        </footer> 
   </body>
</html>