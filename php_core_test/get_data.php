<div class="col-md-12">
    <table class="table" id="user_table">
        <thead style="background-color: #708090;">
            <tr>
                <th style="color:white" class="text-center">Id</th>
                <th style="color:white" class="text-center">Emp Name</th>
                <th style="color:white" class="text-center">Emp Position</th>
                <th style="color:white" class="text-center">Emp Salary</th>
                <th style="color:white" class="text-center">Status</th>
            </tr>
        </thead>
        <tbody>
            <?php
                include 'database.php';
                $sql=mysqli_query($conn,"select * from `employee`");
                while($row=mysqli_fetch_array($sql)){
                    ?>
                        <tr align="center">
                            <td><?php echo $row['id']; ?></td>
                            <td><?php echo $row['emp_name']; ?></td>
                            <td><?php echo $row['emp_position']; ?></td>
                            <td><?php echo $row['emp_salary']; ?></td>
                            <td>
                                <button class="btn btn-warning" data-toggle="modal" id="btn_edit" data-id="<?php echo $row['id']; ?>" data-target="#editmodel">Edit</button>
                                <button class="btn btn-danger" id="delete" value="<?php echo $row['id']; ?>">Delete</button>
                            </td>
                        </tr>
                    <?php
                }
            ?>
        </tbody>
    </table>
</div>