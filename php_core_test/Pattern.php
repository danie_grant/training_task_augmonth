<html>
    <title>Series</title>
    <head>
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <script src="assets/css/jquery.min.js"></script>
        <script src="assets/css/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand mb-3" href="index.php">
                <img src="assets/images/303.jpg" alt="logo" style="width:60px;">
            </a>            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="index.php" style="color:white;">Home</a>
                    <a class="nav-item nav-link" href="series.php" style="color:white;">Series</a>
                    <a class="nav-item nav-link" href="Pattern.php" style="color:white;">Pattern</a>
                    <a class="nav-item nav-link" href="sum.php" style="color:white;">Sum</a>
                    <a class="nav-item nav-link" href="query.php" style="color:white;">My Sql Query</a>
                    <a class="nav-item nav-link" href="employee_info.php" style="color:white;">Emp Data</a>
                </div>
            </div>
        </nav><br>
        <div class="container" style="margin-bottom:90px;">
            <div class="row">
                <div class="col-sm-4">
                    <h4 style="font-weight: bold;">While loop increase Operator</h4>
                    <?php  
                        $i=1;
                        while($i<=7)
                        {
                            
                            $j=1;
                            while($j<=$i)
                            {
                                $j++;
                                if($i % 2 !=0)
                                {
                                    echo $i;
                                }
                                $j++;
                            }
                            if($i % 2 != 0)
                            {
                                echo "<br>";
                            }
                            $i++;
                        }
                        
                        /* for($i=1;$i<=7;$i+=2)
                        {
                            for($j=1;$j<=$i;$j+=2)
                            if($i%2 !=0)
                            {
                                echo $i;
                            }
                            if($i%2 !=0)
                            {
                                echo "<br>";
                            }
                        } */
                    ?>
                    <hr class="d-sm-none">
                </div>
                <div class="col-sm-4">
                    <h4 style="font-weight: bold;">for loop decrement Operator</h4>
                    <?php  
                        for ($x=8; $x>0; $x-=2) {
                            for($y=8;$y>=$x;$y-=2)
                            {
                                echo $x;
                            }
                            echo "<br>";
                        }
                    ?> 
                </div>
                <div class="col-sm-4">
                    <h4 style="font-weight: bold;">for loop increase Operator</h4>
                    <?php
                        for($a=1;$a<=3;$a++)
                        {
                            for($b=1;$b<=$a;$b++)
                            {
                                $sum=$a*$a*$a;
                                echo $sum;
                            }
                            echo "<br>";
                        }
                    ?>
                </div>
            </div>
        </div>
        <footer class="py-5 bg-dark fixed-bottom" style="color:white;">
            <div class="container">
                <div class="row">
                    <div class="container px-4" style="font-size:20px;"><br>
                        <p class="m-0 text-center text-white">Copyright 1999-2021 by Refrences Data. All Rights Reserved. My Testing Project is Powered by</p>
                    </div>
                </div>
            </div>
        </footer> 
   </body>
</html>