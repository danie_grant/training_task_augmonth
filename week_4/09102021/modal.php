<?php 
    session_start();
    if(isset($_POST['submit']))
    {
        $price = $_POST['price'];
        echo $price;
        $calc = $_POST['calc'];
        $discount_amount = $_POST['discount_amount'];
        
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
        <h2>Modal Example</h2>
        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Product</button>
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Product Header</h4>
                    </div>
                    <div class="modal-body">
                        <label for="price"> Price <span> </span> </label>
                        <input class="form-control" id="price" name="price" type="number" required>
                    </div>
                    <div class="modal-body">
                        <label for="calc">Discount Type<span></span></label>
                        <select class="form-control" id="calc">
                            <option value="selected">Please Select</option>
                            <option value="0">value</option>
                            <option value="1">%</option>
                        </select>
                    </div>
                    <div class="modal-body">
                        <label for="discount_amount"> Discount Amount<span></span> </label>
                        <input class="form-control" id="discount_amount" name="discount_amount" type="number" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success" id="submit" value="submit">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>