<?php
$employee_data = array (
    0 => Array
        (
            'id' => '1',
            'name' => 'test',
            'address' => 'Junagadh',
        ),
    1 => Array
        (
            'id' => '2',
            'name' => 'Raj',
            'address' => 'Rajkot',
        ),
    2 => Array
        (
            'id' => '3',
            'name' => 'Meet',
            'address' => 'Pune'
        )
    );
    
    function Ifexist($search_key, $id, $employee_data) 
    {
        foreach ($employee_data as $key => $val) {
            if ($val[$search_key] == $id) {
                return $key;
            }
        }
        return false;
     }
 
    $search_data = Ifexist("id", '3', $employee_data);
	if ($search_data!==false) {
		echo "Search Result : ", $employee_data[$search_data]['address'];
	} else {
		echo "Search Result Not Found";
	} 

?>
