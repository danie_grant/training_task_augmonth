<?php
$data= array(
    0 => Array
        (
            'id' => '101',
            'name' => 'test',
            'address' => 'Junagadh',
        ),
    1 => Array
        (
            'id' => '102',
            'name' => 'Raj',
            'address' => 'Rajkot',
        ),
    2 => Array
        (
            'id' => '103',
            'name' => 'Meet',
            'address' => 'Pune',
        ),
    3 => Array(
        'id' => '104',
        'name' => 'amit',
        'address' => 'rajkot'
    )
);
?>
 
 <style>
    table, th, td {
        padding: 06px;
        border: 1px solid black;
    }
</style>
<h3 align="center">Array Tables</h3>
<table align="center" style="width:70%">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Address</th>
    </tr>

    <?php foreach($data as $key => $item) : ?>

    <tr>
        <td align="center"><?php echo $item['id'] ?></td>
        <td align="center"><?php echo $item['name'] ?></td>
        <td align="center"><?php echo $item['address'] ?></td>
    </tr>
    <?php endforeach; ?>
</table>