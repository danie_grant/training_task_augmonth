<?php 
    include 'ImageResize.php';
    trait Message
    {
        public function GetProductMessage($message)
        {
            if($message=='insert')
            {
                return 'Record Inserted successfully';
            } elseif($message=='update'){
                return 'Record Updated successfully';
            } elseif($message=='delete'){
                return 'Record Deleted successfully';
            } elseif($message=='failed'){
                return 'Something Wrong!';
            } elseif($message=='failed_image'){
                return 'Something Wrong Image!';
            } 
        }
    }

    class TableData extends ImageResize
    {
        public function imageResize($imageResourceId,$width,$height,$targetWidth,$targetHeight) 
        {
            $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
            imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);
            return $targetLayer;
        }
   
        public function get_data($sql,$mysqli)
        {
           $results = $mysqli->query($sql);
           return $results;
        }
    }

    interface FileUpload
    {
        public function File_upload($file);
    }

    class database extends TableData implements FileUpload
    {
        use Message;
        public $que;
        public $sql;
        const servername='localhost';
        const username='root';
        const password='';
        const dbname='store_managment_class';
        private $result=array();
        private $mysqli='';

        public function __construct()
        {
            $this->mysqli = new mysqli(self::servername,self::username,self::password,self::dbname);
        }

        public function insert($table,$para=array())
        {
            $table_columns = implode(',', array_keys($para));
            $table_value = implode("','", $para);

            $sql="INSERT INTO $table($table_columns) VALUES('$table_value')";

            $result = $this->mysqli->query($sql);
            
            $last_id = $this->mysqli->insert_id; 
            return $last_id;
        }

        public function update($table,$para=array(),$i_id)
        {
            $args = array();

            foreach ($para as $key => $value) {
                $args[] = "$key = '$value'"; 
            }

            $sql="UPDATE  $table SET " . implode(', ', $args);

            $sql .=" WHERE $i_id ";
            $result = $this->mysqli->query($sql);
        }

        public function delete($table,$i_id)
        {
            $sql="DELETE FROM $table";
            $sql .=" WHERE $i_id ";
            $sql;
            $result = $this->mysqli->query($sql);
        }

        public function select($table,$where = null)
        {
            $key=array_keys($where);
            $value=array_values($where);
            $where_filed=implode(",",$key);
            $where_value=implode(",",$value);
            if ($where != null) 
            {
                $sql="SELECT * FROM $table WHERE $where_filed='$where_value'";
            }else{
                $sql="SELECT * FROM $table";
            }
            $this->sql = $result = $this->mysqli->query($sql);
        }

        public function image_update($sql,$para=array(),$i_id)
        {
            $args = array();
            foreach ($para as $key => $value)
            {
                $args[] = "$key = '$value'"; 
            }
            $sql="UPDATE  `images` SET " . implode(',', $args);
            $sql .=" WHERE $i_id";
           
            $result1 = $this->mysqli->query($sql);
        }

        public function select_all($sql)
        {
           $results = $this->get_data($sql,$this->mysqli);
           return $results;
        }

        public function File_upload($file)
        {
            foreach($file as $key=>$value)
            {
                move_uploaded_file($value['img_temp_name'],$value['img_name']);
            } 
        }

        public static function GetMsg($message)
        {
            if($message=='insert')
            {
                return 'Record Inserted successfully';
            } elseif($message=='update')
            {
                return 'Record Updated successfully';
            } elseif($message=='delete'){
                return 'Record Deleted successfully';
            } elseif($message=='failed')
            {
                return 'Record Already Exist!';
            } elseif($message=='faileds')
            {
                return 'Something Wrong!';
            } 
        }

        public function __destruct()
        {
            $this->mysqli->close();
        }
    }

    function UserData($msg)
    {
        if($msg=='reg_login')
        {
            return 'User Registration successfully Please Login Now';
        } elseif($msg=='logout'){
            return 'logout successfully';
        } elseif($msg=='failed')
        {
            return 'Something Wrong!';
        } elseif($msg=='user_exist')
        {
            return 'Email Already Exist!';
        } 
    }
?>