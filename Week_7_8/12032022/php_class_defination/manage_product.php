<?php
    session_start();
    
    include 'database.php';
    
    $product = new database();

    if(empty($_SESSION))
    {
        header('location:index.php');
    }

    if(isset($_POST['product_success']))
    {
        $product_data['v_product_name'] = isset($_POST['v_product_name']) ? $_POST['v_product_name'] : '';
        $product_data['i_price'] = isset($_POST['i_price']) ? $_POST['i_price'] : '';
        $product_data['i_sale_price'] = isset($_POST['i_sale_price']) ? $_POST['i_sale_price'] : '';
        $product_data['i_qty'] = isset($_POST['i_qty']) ? $_POST['i_qty'] : '';
        $product_data['v_product_status'] = isset($_POST['v_product_status']) ? $_POST['v_product_status'] : '';
        $product_data['v_product_code'] = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 7);

        if(isset($_FILES['product_image']['name']) ? $_FILES['product_image']['name'] : '')
        {
            $arr = array('jpeg','png');
            $photo = explode(".", $_FILES['product_image']['name']);
            $filename = round(microtime(true));
            $pic2 = $filename . '.' . end($photo);
            $pic_tmp = $_FILES['product_image']['tmp_name'];
            $path = "assets/image/";
            $path = $path.$pic2;
            $sourceProperties = getimagesize($pic_tmp);
            $ext = pathinfo($_FILES['product_image']['name'], PATHINFO_EXTENSION);
            if(!in_array($ext, $arr))
            {
                header('location:manage_product.php?action=list&message=failed_image');exit;
            }
            
            $folderPath = "assets/image/thumbnail/";
            if (!file_exists('assets/image/thumbnail/')) 
            {
                mkdir('assets/image/thumbnail/', 0777, true);
            }
            $imageType = $sourceProperties[2];
            switch ($imageType) {
            case IMAGETYPE_PNG:
                $imageResourceId = imagecreatefrompng($pic_tmp); 
                $targetLayer = $product->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'100','100');
                imagepng($targetLayer,$folderPath. $filename. "_100_100.". $ext);
                break;
            case IMAGETYPE_JPEG:
                $imageResourceId = imagecreatefromjpeg($pic_tmp); 
                $targetLayer = $product->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'100','100');
                imagejpeg($targetLayer,$folderPath. $filename. "_100_100.". $ext);
                break;
            default:
                echo "Invalid Image type.";
                exit;
                break;
            }
            if($sourceProperties[0] > 1024 && $sourceProperties[1] > 720)
            {
                $imageType = $sourceProperties[2];
                switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($pic_tmp); 
                    $targetLayer = $product->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1], '1024','720');
                    imagepng($targetLayer,$path);
                    break;
                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($pic_tmp); 
                    $targetLayer = $product->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'1024','720');
                    imagejpeg($targetLayer,$path);
                    break;
                default:
                    echo "Invalid Image type.";
                    exit;
                    break;
                }
            } else {
                $product_img[0]['img_temp_name']=$_FILES['product_image']['tmp_name'];
                $product_img[0]['img_name']="assets/image/".$pic2;
                $product->File_upload($product_img);
            } 
            if(!empty($product_data) && count($product_data) > 0)
            {
                $product_data['product_image'] = $pic2;
                $last_id=$product->insert('product',$product_data);
            } else {
                header('location:manage_product.php?action=list&message=failed');
            }
        }

        if(isset($_FILES['v_product_image']['name']) ? $_FILES['v_product_image']['name'] : '')
        {   
            foreach($_FILES['v_product_image']['name'] as $key=>$val)
            {
                $type = strtolower(pathinfo($val,PATHINFO_EXTENSION));
                $photo = explode(".",$val);
                $pic = rand().'.'.$type;
                $arr = array("jpeg","png");
                if(in_array($type,$arr))
                {
                    $cat_img[$key]['img_temp_name']=$_FILES['v_product_image']['tmp_name'][$key];
                    $cat_img[$key]['img_name']="assets/image/".$pic;
                    $product->File_upload($cat_img);
                    $product->insert('images',['v_product_image'=>$pic,'v_status'=>0,'i_product_id'=>$last_id]);
                    header('location:manage_product.php?action=list&message=insert');
                } else {
                    header('location:manage_product.php?action=list&message=failed');
                }
            }
        }
        
        if(!empty($last_id))
        {
            foreach($_POST['i_cat_id'] as $key => $val)
            {
                $product->insert('tbl_relation_category_product',['i_cat_id'=>$val,'i_product_id'=>$last_id]);
            }
        }
    }

    if(isset($_POST['product_edit'])) 
    {
        $product = new database();

        $i_id=$_GET['i_id'];
        $where=array(
            "i_product_id"=>$_GET['i_id']
        );
        $product->select("tbl_relation_category_product",$where,"i_product_id='$i_id'");
        $result2 = $product->sql;
        while($row2 = mysqli_fetch_assoc($result2))
        {
            $category_data[]=$row2;
        }
        foreach($category_data as $keys => $category_2)
        {
            $cat_selected[$keys]=$category_2['i_cat_id'];
        }
        $edit = new database();
        $i_id=$_GET['i_id'];
        $where=array( 
        );
        $edit->select("category",$where);
        $result_show = $edit->sql;
        while($cat_shows = mysqli_fetch_assoc($result_show))
        {
            $category_show[]=$cat_shows;
        }
        $product->select("product",$where);
        $result = $product->sql;
        $row = mysqli_fetch_assoc($result);
        $edit_data['v_product_name'] = isset($_POST['v_product_name']) ? $_POST['v_product_name'] : '';
        $edit_data['i_price'] = isset($_POST['i_price']) ? $_POST['i_price'] : '';
        $edit_data['i_sale_price'] = isset($_POST['i_sale_price']) ? $_POST['i_sale_price'] : '';
        $edit_data['i_qty'] = isset($_POST['i_qty']) ? $_POST['i_qty'] : '';
        $edit_data['v_product_status'] = isset($_POST['v_product_status']) ? $_POST['v_product_status'] : '';
        foreach($cat_selected as $del_id=>$del_cat_id)
        {
            $product->delete('tbl_relation_category_product',"i_cat_id='$del_cat_id' AND i_product_id='".$_GET['i_id']."'");
        }
        
        if(isset($_POST['i_cat_id']) ? $_POST['i_cat_id'] : '')
        {
            foreach($_POST['i_cat_id'] as $key1 => $val1)
            {
                $product->insert('tbl_relation_category_product',['i_cat_id'=>$val1,'i_product_id'=>$i_id]);
            }
        }
        
        if(isset($_FILES['v_product_image']['name']) ? $_FILES['v_product_image']['name'] : '')
        {
            foreach($_FILES['v_product_image']['name'] as $key=>$val)
            {
                $type = strtolower(pathinfo($val,PATHINFO_EXTENSION));
                $photo = explode(".",$val);
                $pic = rand().'.'.$type;
                $arr = array("jpeg","png");    
                if(in_array($type, $arr))
                {
                    $cat_img[$key]['img_temp_name']=$_FILES['v_product_image']['tmp_name'][$key];
                    $cat_img[$key]['img_name']="assets/image/".$pic;
                    $edit->insert('images',['v_product_image'=>$pic,'v_status'=>0,'i_product_id'=>$i_id]);
                    $product->File_upload($cat_img);
                    header('location:manage_product.php?action=list&message=update');
                } 
            }
        }
        
        if(isset($_FILES['product_image']['name']) ? $_FILES['product_image']['name'] : '')
        {
            $pic2 = isset($_POST['old_product_image']) ? $_POST['old_product_image'] : '';
            $arr = array('jpeg','png');
            $photo = explode(".", $_FILES['product_image']['name']);
            $filename = round(microtime(true));
            $pic2 = $filename . '.' . end($photo);
            $pic_tmp = $_FILES['product_image']['tmp_name'];
            $path = "assets/image/";
            $path = $path.$pic2;
            $sourceProperties = getimagesize($pic_tmp);
            $ext = pathinfo($_FILES['product_image']['name'], PATHINFO_EXTENSION);
            if(!in_array($ext, $arr))
            {
                header('location:manage_product.php?action=list&message=failed_image');exit;
            }
            $folderPath = "assets/image/thumbnail/";
            if (!file_exists('assets/image/thumbnail/')) {
                mkdir('assets/image/thumbnail/', 0777, true);
            }
            $imageType = $sourceProperties[2];
            switch ($imageType) {
            case IMAGETYPE_PNG:
                $imageResourceId = imagecreatefrompng($pic_tmp); 
                $targetLayer = $product->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'100','100');
                imagepng($targetLayer,$folderPath. $filename. "_100_100.". $ext);
                break;
            case IMAGETYPE_JPEG:
                $imageResourceId = imagecreatefromjpeg($pic_tmp); 
                $targetLayer = $product->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'100','100');
                imagejpeg($targetLayer,$folderPath. $filename. "_100_100.". $ext);
                break;
            default:
                echo "Invalid Image type.";
                exit;
                break;
            }
            if($sourceProperties[0] > 1024 && $sourceProperties[1] > 720)
            {
                $imageType = $sourceProperties[2];
                switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($pic_tmp); 
                    $targetLayer = $product->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1], '1024','720');
                    imagepng($targetLayer,$path);
                    $edit_data['product_image'] = $pic2;
                    break;
                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($pic_tmp); 
                    $targetLayer = $product->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'1024','720');
                    imagejpeg($targetLayer,$path);
                    $edit_data['product_image'] = $pic2;
                    break;
                default:
                    echo "Invalid Image type.";
                    exit;
                    break;
                }
            } else {
                $edit_data['product_image'] = $pic2;
                $pro_data[0]['img_temp_name']=$_FILES['product_image']['tmp_name'];
                $pro_data[0]['img_name']="assets/image/".$pic2;
            }
            $product->File_upload($pro_data);
        }
        
        if(!empty($edit_data))
        {
            $product->update("product",$edit_data,"i_id='$i_id'");
            header('location:manage_product.php?action=list&message=update');exit;
        } else {
            header('location:manage_product.php?action=list&message=failed');
        }
    }

    if(isset($_POST['delete_id']))
    {
      $delete = new database();  

      $i_id=$_POST['delete_id'];
      $where=array("i_id" =>$_POST['delete_id']);
      $delete->select("product",$where);
      $result = $delete->sql;
      $row = mysqli_fetch_assoc($result);
      if(!empty($row['product_image']))
      {
        unlink("assets/image/".$row['product_image']);
      } 
      $delete->delete('product',"i_id='$i_id'",$where);
      if($delete){
        echo 1;
        exit;
      }else{
        echo 0;
        exit;
      }
    }

    if(isset($_POST['image_delete']))
    {
        $image = new database();

        $i_id=$_POST['image_delete'];
        $where=array(            
            'i_id' => $_POST['image_delete']
        );
        $image->select("images", $where);
        $result = $image->sql;
        $row = mysqli_fetch_assoc($result);
        if(!empty($row['v_product_image']))
        {
            if(file_exists("assets/image/".$row['v_product_image']))
            {
                unlink("assets/image/".$row['v_product_image']);
            }
        }
        $image->delete('images',"i_id='$i_id'",$where);
        if($image)
        {
        echo 1;
        exit;
        }else{
        echo 0;
        exit;
        }
    }

    if(isset($_POST['delete_product_image']))
    {
        $image = new database();
        
        $where=array(            
            "i_id"=>$_POST['delete_product_image']
        );
        $image->select("product", $where);
        $result = $image->sql;
        $row = mysqli_fetch_assoc($result);
        if(!empty($row['v_product_image']))
        {
            if(file_exists("assets/image/".$row['product_image']))
            {
            unlink("assets/image/".$row['product_image']);
            }
        }
        $delete_image['product_image']=NULL;
        $image->update('product',$delete_image,"i_id='".$_POST['delete_product_image']."'");
        if($image){
        echo 1;
        exit;
        }else{
        echo 0;
        exit;
        }
    }

    $priceOrder = "asc";
    $salepriceOrder = "asc";
    $i_qty="asc";
    $orderBy = !empty($_GET["orderby"]) ? $_GET["orderby"] : "created_at";
    $order = !empty($_GET["order"]) ? $_GET["order"] : "desc";
    $orders ="ORDER BY ". $orderBy . " " . $order;
    if($orderBy == "i_price" && $order == "asc")
    {
        $priceOrder = "desc";
    }
    if($orderBy == "i_sale_price" && $order == "asc") 
    {
        $salepriceOrder = "desc";
    }
    if($orderBy == "i_qty" && $order == "asc") 
    {
        $i_qty = "desc";
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include 'inc/head.php'; ?>
    </head>
    <body>
        <?php include 'inc/navbar.php'; ?>
        <div class="container mt-5">
            <div class="alert alert-danger alert-dismissable col-md-12 pull-right mt-5" id="delete_alert" style="display:none">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> Record Deleted successfully</b>
        </div>
        <?php if(isset($_GET['message'])){
        if($_GET['message'] == 'insert'){ ?>
        <div class="alert alert-success alert-dismissable col-md-12 pull-right mt-5">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> <?php echo $product->GetProductMessage('insert')?></b>
        </div>
        <?php } else if($_GET['message'] == 'delete'){ ?>
        <div class="alert alert-success alert-dismissable col-md-12 pull-right mt-5">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> <?php echo $product->GetProductMessage('delete')?></b>
        </div>
        <?php } else if($_GET['message'] == 'update'){ ?>
        <div class="alert alert-success alert-dismissable col-md-12 pull-right">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b><?php echo $product->GetProductMessage('update')?></b>
        </div>
        <?php } elseif($_GET['message'] == 'failed'){ ?>
        <div class="alert alert-danger alert-dismissable col-md-12 pull-right">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b><?php echo $product->GetProductMessage('failed')?></b>
        </div>
        <?php }elseif($_GET['message'] == 'failed_image'){ ?>
        <div class="alert alert-danger alert-dismissable col-md-12 pull-right">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b><?php echo $product->GetProductMessage('failed_image')?></b>
        </div>
        <?php } } ?> 
        <div class="card bg-light mb-3">
            <div class="card-header"><?php 
                if(isset($_GET['action']) && $_GET['action']=="insert")
                {
                    echo "Add Product";
                }elseif(isset($_GET['action']) && $_GET['action']=="edit")
                {
                    echo "Edit Product";
                }else{
                    echo "Product";
                } ?>
            </div>
            <div class="card-body">
                <?php if(isset($_GET['action']) && $_GET['action']=="insert"){ ?>
                <form method="POST" action="" enctype="multipart/form-data" id="product_form" name="product_form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-floating">
                                <label>Category Name</label>
                                <select class="category form-control" name="i_cat_id[]" id="i_cat_id" multiple>
                                    <option label="Please Select Category"></option>
                                    <?php 
                                        $category = new database();
                                        $where=array(
                                            "v_status"=>1,
                                        );
                                        $category->select("category",$where);
                                        $result = $category->sql;
                                        while($row = mysqli_fetch_assoc($result)){
                                        ?>                                                
                                        <option value="<?php echo $row['i_id']?>"><?php echo $row['v_category_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating mb-4">
                                <label for="v_product_photo">Product Image Multiple</label>
                                <div class="custom-file">
                                    <input type="file" name="v_product_image[]" class="custom-file-input" id="v_product_image" multiple>
                                    <label class="custom-file-label" for="v_product_photo" id="v_product_photo">Please Select Product Image</label>
                                </div>
                            </div>
                            <div class="user-image mb-3 text-center">
                                <div class="imgGallery"> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating mb-3">
                                <label for="v_product_name ">Product Name</label>
                                <input class="form-control" id="v_product_name" type="text" name="v_product_name" placeholder="Enter your Product Name"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <label for="i_price">Price</label>
                                <input class="form-control" id="i_price" type="number" name="i_price" placeholder="Enter your Price"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <label for="i_sale_price">Sale Price</label>
                                <input class="form-control" id="i_sale_price" type="number" name="i_sale_price" placeholder="Enter your Sale Price"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating mb-3">
                                <label for="i_qty">Order Qty</label>
                                <input class="form-control" id="i_qty" type="number" name="i_qty" placeholder="Enter Your Order Quantity"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <label for="v_product_status">Status</label>
                                <select class="form-control" name="v_product_status" id="v_product_status" onchange="changeMessage(this)">
                                    <option value="">Select Status</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating mb-3">
                                <label for="v_product_photo">Product Main Image</label>
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" id="product_image" name="product_image" onchange="preview()">
                                <center><img id="image_view" src="" width="100px;"/></center>
                                <label class="custom-file-label" for="v_product_photo" id="v_product_photo">Please Select Product Main Image</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4 ml-2">
                        <button class="btn btn-success" type="submit" name="product_success" id="product_success">Save</button>
                        <button class="btn btn-secondary"><a href="manage_product.php" style="color:inherit; text-decoration: none;"> Back </a></button>
                    </div>
                </form>
            </div>
            <?php } elseif(isset($_GET['action']) && $_GET['action']=="edit"){
                if(isset($_GET['i_id']) && !empty($_GET['i_id']) ){
                    $i_id=$_GET['i_id'];
                    $where=array(            
                    "i_id"=>$_GET['i_id']
                    );
                    $product->select("product",$where);
                    $result = $product->sql;
                    $data = mysqli_fetch_assoc($result);
                }
                $i_id=$_GET['i_id'];
                $where=array(
                    "i_product_id"=>$_GET['i_id']
                );
                $product->select("tbl_relation_category_product",$where,"i_product_id='$i_id'");
                $result2 = $product->sql;
                while($row2 = mysqli_fetch_assoc($result2))
                {
                    $category_data[]=$row2;
                }
                foreach($category_data as $keys => $category_2)
                {
                    $cat_selected[$keys]=$category_2['i_cat_id'];
                }
                $edit = new database();
                $where=array(
                    "v_status"=>1,
                );
                $edit->select("category",$where);
                $result_show = $edit->sql;
                while($cat_shows = mysqli_fetch_assoc($result_show))
                {
                    $category_show[]=$cat_shows;
                }
            ?>
            <form method="POST" action="" enctype="multipart/form-data" id="product_edit_form" name="product_edit_form">
                <input type="hidden" name="i_id" value="<?php echo $_GET['i_id'] ?>">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-floating">
                            <label>Category Name select</label>
                            <select class="category form-control" name="i_cat_id[]" id="i_cat_id" multiple>
                                <option label="Please Select Category"></option>
                                <?php 
                                foreach($category_show as $k=>$cat){
                                ?> 
                                <option value="<?php echo $cat['i_id'];?>"<?php echo $cat['i_id']=in_array($cat['i_id'],$cat_selected) ? "selected" : ""; ?>><?php echo $cat['v_category_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>    
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <label for="v_product_name">Product Name</label>
                            <input class="form-control" id="v_product_name" type="text" name="v_product_name" placeholder="Enter your Product Name" value="<?php echo $data['v_product_name'];?>"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating">
                            <label for="i_price">Price</label>
                            <input class="form-control" id="i_price" type="number" name="i_price" placeholder="Enter your Price" value="<?php echo $data['i_price'];?>"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <label for="i_sale_price">Sale Price</label>
                            <input class="form-control" id="i_sale_price" type="number" name="i_sale_price" placeholder="Enter your Sale Price" value="<?php echo $data['i_sale_price'];?>"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <label for="i_qty">Order Qty</label>
                            <input class="form-control" id="i_qty" type="number" name="i_qty" placeholder="Enter your Order Qty" value="<?php echo $data['i_qty'];?>"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating">
                            <label for="v_product_status">Status</label>
                            <select class="form-control" name="v_product_status" id="v_product_status" onchange="changeMessage(this)">
                                <option value="1" <?php echo $data['v_product_status'] == 1 ? "selected": "";?>>Active</option>
                                <option value="0" <?php echo $data['v_product_status'] == 0 ? "selected": "";?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <label for="product_photo">Product Main Image</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input mt-2" id="product_image" name="product_image" onchange="preview_new()">
                                <input type="hidden" name="old_product_image" id="old_product_image" value="<?php echo isset($data['product_image']) ? $data['product_image'] : ''?>">
                                <label class="custom-file-label" for="product_photo" id="product_photo">Please Select Product Image</label>
                                <?php
                                    if($data['product_image'])
                                    { 
                                ?>
                                <center><img id="image_view_display" src="assets/image/<?php echo $data['product_image'];?>" width="100px;" height="100px;">
                                <button type="button" class="btn btn-danger" data-id="<?php echo $data['i_id'] ?>" id="delete_product_image">Delete </button>
                                <?php } ?>
                                </center>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating mb-3">
                            <label for="v_product_photo">Product Multiple Image</label>
                            <div class="custom-file">
                            <input type="file" name="v_product_image[]" class="custom-file-input" id="v_product_image" multiple>
                            <label class="custom-file-label" for="v_product_photo" id="v_product_photo">Please Select Product Image</label>
                            <?php 
                                $images = new database();
                                $results=$images->select_all("Select * from images where i_product_id='$i_id'");
                                while($rows = mysqli_fetch_assoc($results))
                                {
                                    if($rows['v_product_image'])
                                    {
                                ?>
                                <center><img class='product_thumb_image mt-2' src="assets/image/<?php echo $rows['v_product_image'];?>" width="100px;" height="100px;">
                                <button type="button" class="btn btn-danger" data-id="<?php echo $rows['i_id'] ?>" id="image_delete">Delete</button>
                                </center>
                                <?php } } ?>
                                <div class="user-image text-center">
                                    <div class="imgGallery"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4 ml-2 mb-5">
                        <button class="btn btn-success" type="submit" name="product_edit" id="product_edit">Save</button>
                        <button class="btn btn-secondary"><a href="manage_product.php" style="color:inherit; text-decoration: none;"> Back </a></button>
                    </div>
                </div>
            </form>
        </div>
        <?php } else { ?>
        <a class="btn btn-success" href="manage_product.php?action=insert">Add Product</a><br><br>
        <form method="GET" autocomplete="off" enctype="multipart/form-data" action="" id="reset_form">
            <div class="row">
                <div class="col-sm">
                    <input class="form-control" id="search_product" type="text" name="search_product" placeholder="Search Product Name" value="<?php echo isset($_GET['search_product']) ? $_GET['search_product'] : ''?>"/>
                </div>
                <div class="col-sm">
                    <input class="form-control" id="search_price" type="number" name="search_price" placeholder="Search Product Price" value="<?php echo isset($_GET['search_price']) ? $_GET['search_price'] : ''?>"/>
                </div>
                <div class="col-sm">
                    <input class="form-control" id="search_qty" type="number" name="search_qty" placeholder="Search Product Qty" value="<?php echo isset($_GET['search_qty']) ? $_GET['search_qty'] : ''?>"/>
                </div>
                <div class="col-sm">
                    <select class="form-control" name="search" id="search">
                        <option value="">All Record Status</option>
                        <option value='active' <?php echo (isset($_GET['search']) && $_GET['search']=='active') ? 'selected' : ''?>>Active</option>
                        <option value='inactive' <?php echo (isset($_GET['search']) && $_GET['search']=='inactive') ? 'selected' : ''?>>Inactive</option>
                    </select>
                </div>
                <div class="col-sm">
                    <button class="btn btn-success" type="submit" name="product_search" id="product_search">Search</button>
                    <button class="btn btn-info" type="reset" name="product_reset" id="product_reset">Reset</button>
                </div>
            </div>
        </form><br>
        <table class="table table stripe my-table table-hover table-responsive" id="product_table_data">
            <thead style="text-align: center" >
                <tr style="background-color: #708090;">
                <th>Id</th>
                <th>Image</th>
                <th>Category Name</th>
                <th>Product Name</th>
                <th>Product Code</th>
                <th><a href="?orderby=i_price&order=<?php echo $priceOrder; ?>">Price</th>
                <th><a href="?orderby=i_sale_price&order=<?php echo $salepriceOrder; ?>">Sale Price</th>
                <th><a href="?orderby=i_qty&order=<?php echo $i_qty; ?>">Qty</th>
                <th>Order</th>
                <th>Status</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $per_page_record = 2;       
                if (isset($_GET["page"])) {    
                    $page = $_GET["page"];    
                } else {    
                    $page=1;    
                }    
                $where= array();
                if(isset($_GET['product_search'])) 
                {
                   if(!empty($_GET['search_product']))
                   {
                        $where[]= " v_product_name like '%".$_GET['search_product']."%'";
                   }
                   if(!empty($_GET['search']))
                   {
                        $status = '';
                        if(isset($_GET['search']) && $_GET['search'] == 'active'){
                          $status = 1;
                        }
                        else if(isset($_GET['search']) && $_GET['search'] == 'inactive'){
                          $status = 0;
                        }
                        $where[]= " v_product_status='".$status."'";
                   }
                   if(!empty($_GET['search_price']))
                   {
                       $where[]= " i_price='".$_GET['search_price']."'";
                   }
                   if(!empty($_GET['search_qty']))
                   {
                       $where[]= " i_qty='".$_GET['search_qty']."'";
                   }
                }
                if(!empty($where))
                {
                    $where = " WHERE " . implode(' AND ',$where);
                }else{
                    $where = '';
                }
                $page = isset($_GET['page']) ? $_GET['page'] : 1;
                $offset = ($page - 1)  * $per_page_record;
                $select="SELECT distinct product.*,GROUP_CONCAT(category.v_category_name) as category_name from product LEFT JOIN tbl_relation_category_product ON product.i_id=tbl_relation_category_product.i_product_id LEFT JOIN category ON tbl_relation_category_product.i_cat_id=category.i_id";
                $select=$select.$where;
                $select=$select." GROUP BY product.i_id ";
                $select=$select.$orders." LIMIT ".$offset.",". $per_page_record;
                $results=$product->select_all($select);
                $count_record = $results->num_rows;
                $counter = 0;
                if($results->num_rows < 1)
                {
                    $select="SELECT distinct product.*,GROUP_CONCAT(category.v_category_name) as category_name from product LEFT JOIN tbl_relation_category_product ON product.i_id=tbl_relation_category_product.i_product_id LEFT JOIN category ON tbl_relation_category_product.i_cat_id=category.i_id";
                    $select=$select.$where;
                    $select=$select." GROUP BY product.i_id ";
                    $select=$select.$orders." LIMIT ". $per_page_record;
                    $results=$product->select_all($select);
                }
    
                if (!empty($results) && $results->num_rows > 0) {
                while ($row = $results->fetch_assoc()) {
                ?>
                <tr align="center" id="<?php echo $row['i_id'];?>">
                    <td><?php echo $row['i_id']; ?></td>
                    <?php
                        $explodedImageName = array_filter(explode('.',$row['product_image']));
                        $imageName = '';
                        if(!empty($explodedImageName))
                        {
                            $imageName = $explodedImageName[0].'_100_100.'.$explodedImageName[1];
                        }
                    ?>
                    <td><img src="assets/image/thumbnail/<?php echo $imageName;?>"></td>
                    <td><?php echo $row['category_name'];?></td>
                    <td><?php echo $row['v_product_name']; ?></td>
                    <td><?php echo $row['v_product_code'];?></td>
                    <td><?php echo $row['i_price']; ?></td>
                    <td><?php echo $row['i_sale_price']; ?></td>
                    <td><?php echo $row['i_qty']; ?></td>
                    <td><?php echo ++$counter;?></td>
                    <td><?php echo $row['v_product_status'] == 1 ? '<span class="badge badge-success">Active</span>': '<span class="badge badge-danger">Inactive</span>';?></td>
                    <td><?php echo $row['created_at']; ?></td>
                    <td><?php echo $row['updated_at']; ?></td>
                    <td><a href="manage_product.php?action=edit&i_id=<?php echo $row["i_id"]; ?>" class="btn btn-warning">Edit</a>
                    <button class="btn btn-danger" data-id="<?php echo $row['i_id']  ?>" id="delete_id">Delete </button>
                    </td>
                </tr>
                <?php } } else {
                    echo "No Record Found";
                } ?>
            </tbody>
            <tfoot style="text-align: center">
                <tr style="background-color: #708090;">
                    <th>Id</th>
                    <th>Image</th>
                    <th>Category Name</th>
                    <th>Product Name</th>
                    <th>Product Code</th>
                    <th>Price</th>
                    <th>Sale Price</th>
                    <th>Qty</th>
                    <th>Order</th>
                    <th>Status</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
        <div class="pagination">    
            <?php  
                $select="SELECT distinct product.*,GROUP_CONCAT(category.v_category_name) as category_name from product LEFT JOIN tbl_relation_category_product ON product.i_id=tbl_relation_category_product.i_product_id LEFT JOIN category ON tbl_relation_category_product.i_cat_id=category.i_id";
                $select=$select.$where;
                $select=$select." GROUP BY product.i_id ";
                $select=$select.$orders;
                $query=$product->select_all($select);     
                $count=$query->num_rows;
                $total_records = $count;  
                echo "</br>";     
                $total_pages = ceil($total_records / $per_page_record);     
                $pagLink = "";    
                $queryParams = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
                parse_str($queryParams, $vars);
                unset($vars['page']);
                $queryParams = http_build_query($vars);
                if($page>=2){   
                    echo "<a href='manage_product.php?page=".($page-1).'&'.$queryParams."'>  Prev </a>"; 
                }       
                for($i=1;$i<=$total_pages;$i++)
                {   
                    if($i==$page)
                    {   
                        $pagLink.="<a class='active' href='manage_product.php?page=".$i.'&'.$queryParams."'>".$i." </a>";   
                    } else {   
                        $pagLink.="<a href='manage_product.php?page=".$i.'&'.$queryParams."'>".$i."</a>";     
                    }   
                };     
                    echo $pagLink;   
                if($page<$total_pages){   
                    echo "<a href='manage_product.php?page=".($page+1).'&'.$queryParams."'> Next </a>";   
                }   
            ?>    
        </div>  
        <div class="inline">   
        <input id="page" type="number" min="1" max="<?php echo $total_pages?>"placeholder="<?php echo $page."/".$total_pages; ?>"  value="<?php echo isset($_GET['page']) ? $_GET['page'] : '1'?>" required>   
            <button onClick="go2Page();">Go</button>   
        </div>
        <?php } ?>
        <footer class="bg-dark fixed-bottom" style="color:white;">
            <?php include 'inc/footer.php'; ?>
        </footer>
    </body>
    <script type="text/javascript">
        $("#v_product_image").change(function() {
            $("#v_product_photo").text(this.files[0].name);
        });
        
        function preview() 
        {
            image_view.src = URL.createObjectURL(event.target.files[0]);
        }
        
        $("#product_image").change(function() 
        {
            $("#product_photo").text(this.files[0].name);
        });
        
        $(document).on('click','#product_reset',function()
        {
            $("#search_product,#search_price,#search_qty,#search").val('');
            $("#reset_form").submit();
        });
        
        function preview_new() 
        {
            image_view_display.src = URL.createObjectURL(event.target.files[0]);
            $("#image_view_display").show();
            $("#delete_product_image").show();
            $("#old_product_image").show();
        }

        function ProductTableData()
        {
            $("#product_table_data").load(location.href + " #product_table_data"); 
        }

        $(function()
        {
            var multiImgPreview = function (input, imgPreviewPlaceholder) 
            {
                if (input.files) {
                    var filesAmount = input.files.length;
                    for (i = 0; i < filesAmount; i++) 
                    {
                        var reader = new FileReader();
                        reader.onload = function (event) 
                        {
                            $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                        }
                        reader.readAsDataURL(input.files[i]);
                    }
                }
            };
            $('#v_product_image').on('change',function() 
            {
                multiImgPreview(this, 'div.imgGallery');
            });
        });

        $("#i_cat_id").select2({
            placeholder: "Select Category Name",
            allowClear: true
        });
        
        $("#v_product_image").change(function() {
            $("#v_product_photo").text(this.files[0].name);
        });
        
        function preview()
        {
            image_view.src = URL.createObjectURL(event.target.files[0]);
        }
        
        function changeMessage(v_product_status) {
            var selectedText = v_product_status.options[v_product_status.selectedIndex].innerHTML;
            var selectedValue = v_product_status.value;
        }
        
        function go2Page()   
        {   
            var page = document.getElementById("page").value;   
            window.location.href = 'manage_product.php?page='+page;   
        } 
        
        $(document).on('click', '#delete_id', function(){
            if (confirm('Are You Sure Delete Record?')){
                var i_id=$(this).data('id');
                $.ajax({
                    type: "POST",
                    url: "manage_product.php",
                    data: {
                        delete_id: i_id,
                    },
                    success: function(data){
                        if(data){
                            $("#delete_alert").css("display", "block");
                            $("#delete_alert").text("Product Deleted Successfully");
                            window.setTimeout(function(){
                                $("#"+i_id).fadeOut(1000, 0).slideUp(1000, ProductTableData(), function(){
                                $(this).remove(); 
                                });
                            }, 5000);
                            hideAlert();
                        }
                    }
                });
            }
        });

        $(document).on('click', '#image_delete', function(){
            if (confirm('Are You Sure Delete Record?')){
                var i_id=$(this).data('id');
                $.ajax({
                    type: "POST",
                    url: "manage_product.php",
                    data: {
                        image_delete: i_id,
                    },
                    success: function(data){
                        if(data){
                            $("#delete_alert").css("display", "block");
                            $("#delete_alert").html("Deleted Successfully");
                            window.setTimeout(function(){
                                $("#image_delete").fadeOut(1000, 0).slideUp(1000, function(){
                                $(this).hide(); 
                                setTimeout('location.reload(),100');
                                });
                            }, 500);
                            hideAlert();
                        }
                    }
                });
            }
        });

        $(document).on('click', '#delete_product_image', function(){
            if (confirm('Are You Sure Delete Record?')){
                var i_id=$(this).data('id');
                $.ajax({
                    type: "POST",
                    url: "manage_product.php",
                    data: {
                        delete_product_image: i_id,
                    },
                    success: function(data){
                        if(data){
                            $("#old_product_image").val('');
                            $("#product_image").val('');
                            $("#product_photo").text('');
                            $("#image_view_display").hide();
                            $("#delete_alert").css("display", "block");
                            $("#delete_alert").html("Deleted Successfully");
                            window.setTimeout(function(){
                                $("#delete_product_image").fadeOut(1000, 0).slideUp(1000, function(){
                                $(this).hide(); 
                                });
                            }, 500);
                            hideAlert();
                        }
                    }
                });
            }
        });

        $(function()
        {          
            $("form#product_form").validate(
            {
                rules: 
                {
                    "i_cat_id[]" : 
                    {
                        required: true,
                    },
                    "v_product_image[]": 
                    {
                        required : function(element) {
                        if($("#v_product_image").val() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    },
                        required: true,
                        extension: "jpeg|png"
                    },
                    "product_image": 
                    {
                        required : function(element) {
                        if($("#product_image").val() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    },
                        required: true,
                        extension: "jpeg|png"
                    },
                    "v_product_name": 
                    {
                        required: true,
                    },
                    "i_price": 
                    {
                        required: true,
                    },
                    "i_sale_price": 
                    {
                        required: true,
                        min : function(element) {
                            return parseFloat($("#i_price").val())+1;
                        },
                    },
                    "i_qty": 
                    {
                        required: true,
                    },
                    "v_product_status": 
                    {
                        required: true,
                    },
                    message: 
                    {
                        maxlength: 1024
                    }
                },
                messages: 
                {
                    "i_cat_id[]" : 
                    {
                        required: "Please Select Category."
                    },
                    "v_product_image[]" : 
                    {
                        required: "Please Select Your Product Multiple Image.",
                        extension: "Please Select jpeg or png image.",
                    },
                    "product_image" : 
                    {
                        required: "Please Select Your Product Main Image.",
                        extension: "Please Select jpeg or png image.",
                    },
                    "v_product_name": 
                    {
                        required: "Please Enter Your Product Name."
                    },
                    "i_price": 
                    {
                        required: "Please Enter Your Price."
                    },
                    "i_sale_price": 
                    {
                        required: "Please Enter Your Sale Price."
                    },
                    "i_qty": 
                    {
                        required: "Please Enter Your Order Qty."
                    },
                    "v_product_status": 
                    {
                        required: "Please Select Your Product Status."
                    },
                    message: 
                    {
                        maxlength: jQuery.format("Please limit the message to {0} letters!")
                    }
                }
            });	
        }); 
       
        $(function()
        {       
            $("form#product_edit_form").validate(
            {
                rules: 
                {
                    "i_cat_id[]" : 
                    {
                        required: true,
                    },
                    "v_product_image[]" : {
                        required : function(element) {
                        if($(".product_thumb_image").length < 1) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                        extension: "jpeg|png"
                    },  
                    "product_image": {
                        required : function(element) {
                        if($("#old_product_image").val() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    },
                        extension: "jpeg|png"
                    },
                    "v_product_name": 
                    {
                        required: true,
                    },
                    "i_price": 
                    {
                        required: true,
                    },
                    "i_sale_price": 
                    {
                        required: true,
                        min : function(element) {
                            return parseFloat($("#i_price").val())+1;
                        },
                    },
                    "i_qty": 
                    {
                        required: true,
                    },
                    "v_product_status": 
                    {
                        required: true,
                    },
                    message: 
                    {
                        maxlength: 1024
                    }
                },
                messages: 
                {
                    "i_cat_id[]" : 
                    {
                        required: "Please Select Category."
                    },
                    "v_product_image[]" : 
                    {
                        required: "Please Select Your Product Multiple Image.",
                        extension: "Please Select Your Image jpeg or png image.",
                    },
                    "product_image" : 
                    {
                        required: "Please Select Your Product Main Image.",
                        extension: "Please Select jpeg or png image.",
                    },
                    "v_product_name": 
                    {
                        required: "Please Enter Your Product Name."
                    },
                    "i_price": 
                    {
                        required: "Please Enter Your Price."
                    },
                    "i_sale_price": 
                    {
                        required: "Please Enter Your Sale Price."
                    },
                    "i_qty": 
                    {
                        required: "Please Enter Your Order Qty."
                    },
                    "v_product_status": 
                    {
                        required: "Please Select Your Product Status."
                    },
                    message: 
                    {
                        maxlength: jQuery.format("Please limit the message to {0} letters!")
                    }
                }
            });	
        });
    </script>
</html>