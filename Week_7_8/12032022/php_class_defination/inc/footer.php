<script src="assets/js/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="assets/js/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="assets/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
<script src="assets/js/jquery.validate.js"></script>
<script src="assets/js/select2.min.js"></script>
<script src="assets/js/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        hideAlert(); 
    });
    function hideAlert()
    {
        window.setTimeout(function() {
            $(".alert").fadeOut(1000, 0).slideUp(1000, function(){
                $(this).hide(); 
            });
        }, 1000); 
    }
</script>
<div class="container">
    <div class="row">
        <div class="container px-4 mb-2 mt-2" style="font-size:20px;">
            <p class="m-0 text-center text-white">Copyright 2007-2022 by References Data. All Rights Reserved. My Testing Project is Powered by</p>
        </div>
    </div>
</div>
