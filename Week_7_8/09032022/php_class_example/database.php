<?php 
    class database
    {
        public $que;
        private $servername='localhost';
        private $username='root';
        private $password='';
        private $dbname='class_demo';
        private $result=array();
        private $mysqli='';

        public function __construct()
        {
            $this->mysqli = new mysqli($this->servername,$this->username,$this->password,$this->dbname);
        }

        public function insert_record($table,$para=array())
        {
            $table_columns = implode(',', array_keys($para));
            $table_value = implode("','", $para);

            $sql="INSERT INTO $table($table_columns) VALUES('$table_value')";

            $result = $this->mysqli->query($sql);
            
            $last_id = $this->mysqli->insert_id; 
            return $last_id;
        }

        public function update_record($table,$para=array(),$i_id)
        {
            $args = array();

            foreach ($para as $key => $value) {
                $args[] = "$key = '$value'"; 
            }

            $sql="UPDATE  $table SET " . implode(', ', $args);

            $sql .=" WHERE $i_id ";

            $result = $this->mysqli->query($sql);
        }

        public function delete_record($table,$i_id)
        {
            $sql="DELETE FROM $table";
            $sql .=" WHERE $i_id ";
            $sql;
            $result = $this->mysqli->query($sql);
        }

        public $sql;

        public function select($table,$where = null)
        {
            $key=array_keys($where);
            $value=array_values($where);
            $where_filed=implode(",",$key);
            $where_value=implode(",",$value);
            if ($where != null) {
                $sql="SELECT * FROM $table WHERE $where_filed='$where_value'";
            }else{
                $sql="SELECT * FROM $table";
            }
            $this->sql = $result = $this->mysqli->query($sql);
        }

        public function select_all_record($sql)
        {
           $results = $this->mysqli->query($sql);
           return $results;
        }

        public function File_upload($file)
        {
            foreach($file as $key=>$value)
            {
                move_uploaded_file($value['img_temp_name'],$value['img_name']);
            } 
        }
    
        public function __destruct()
        {
            $this->mysqli->close();
        }
    }
?>