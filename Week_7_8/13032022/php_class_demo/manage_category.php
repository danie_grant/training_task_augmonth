<?php
  session_start();
  
  include 'database.php';

  $category = new database();
  
  if(isset($_POST['category_success']))
  {
    $cat_data['v_category_name'] = isset($_POST['v_category_name']) ? $_POST['v_category_name'] : '';
    $cat_data['i_cat_qty'] = isset($_POST['i_cat_qty']) ? $_POST['i_cat_qty'] : '';
    $cat_data['v_status'] = isset($_POST['v_status']) ? $_POST['v_status'] : '';
    $where=array(
      "v_category_name"=>$_POST['v_category_name']
    );
    
    $category->select("category",$where);
    $result = $category->sql;
    $row = mysqli_fetch_assoc($result);
    if($row && !empty($row) && $row > 0)
    {
      $_SESSION['danger']="Category Already Exist";
      header('location:manage_category.php?action=list&message=failed');exit;
    }
    if(isset($_FILES['v_category_image']['name']) ? $_FILES['v_category_image']['name'] : '')
    {    
      $category_image = explode(".", $_FILES['v_category_image']['name']);
      $pic = round(microtime(true)) . '.' . end($category_image);
      $type = strtolower(pathinfo($pic,PATHINFO_EXTENSION));
      $arr = array("jpeg","png");
      if(in_array($type,$arr)) 
      {
        $cat_data['v_category_image'] = $pic;
        $cat_img[0]['img_temp_name']=$_FILES['v_category_image']['tmp_name'];
        $cat_img[0]['img_name']="assets/image/".$pic;
        if(!empty($cat_data) && count($cat_data) > 0)
        {
          $category->insert_record('category',$cat_data);
          $category->File_upload($cat_img); 
          header('location:manage_category.php?message=insert');
        }
      } else {
        header('location:manage_category.php?action=list&message=image_failed');exit;
      }
    }
  }

  if (isset($_POST['category_edit'])) 
  {
    $edit = new database();
    $i_id=$_POST['i_id'];
    $where=array(            
       "i_id"=>$_POST['i_id']
    );
    $edit->select("category",$where);
    $result = $edit->sql;
    $row = mysqli_fetch_assoc($result);
    $edit_data['v_category_name'] = isset($_POST['v_category_name']) ? $_POST['v_category_name'] : '';
    $edit_data['i_cat_qty'] = isset($_POST['i_cat_qty']) ? $_POST['i_cat_qty'] : '';
    $edit_data['v_status'] = isset($_POST['v_status']) ? $_POST['v_status'] : '';
    if(isset($_FILES['v_category_image']['name']) ? $_FILES['v_category_image']['name'] : '')
    {
      $pic = isset($_POST['old_category_image']) ? $_POST['old_category_image'] : '';

      if($_FILES['v_category_image']['size'] != 0 && $_FILES['v_category_image']['error'] == 0)
      {
        $category_image = explode(".", $_FILES['v_category_image']['name']);
        
        $pic = round(microtime(true)) . '.' . end($category_image);
        $type = strtolower(pathinfo($pic,PATHINFO_EXTENSION));
        $arr = array("jpeg","png");
        if(in_array($type,$arr))
        {
          $edit_data['v_category_image'] = $pic;
          $cat_img[0]['img_temp_name']=$_FILES['v_category_image']['tmp_name'];
          $cat_img[0]['img_name']="assets/image/".$pic;
          $edit->File_upload($cat_img);
          header('location:manage_category.php?message=update');
        } else {
          header('location:manage_category.php?action=list&message=image_failed');exit;
        }
      }
    }
    $edit->update_record("category",$edit_data,"i_id='$i_id'");
    header('location:manage_category.php?message=update');
  }

  if(isset($_POST['delete']))
  {
    $delete = new database();  
    $i_id=$_POST['delete'];
    $where=array("i_id" =>$_POST['delete']);
    $delete->select("category",$where);
    $result = $delete->sql;
    $row = mysqli_fetch_assoc($result);
    unlink("assets/image/".$row['v_category_image']);
    $delete->delete_record('category',"i_id='$i_id'",$where);
    if($delete){
      echo 1;
    }else{
      echo 2;
    }
  }

  if(isset($_POST['delete_image']))
  {
    $image = new database();  
    $i_id=$_POST['delete_image'];
    $where=array("i_id" =>$_POST['delete_image']);
    $image->select("category",$where);
    $result = $image->sql;
    $row = mysqli_fetch_assoc($result);
    if(!empty($row['v_category_image']))
    {
      if(file_exists("assets/image/".$row['v_category_image']))
      {
        unlink("assets/image/".$row['v_category_image']);  
      }
    }    
    $delete_data['v_category_image']=NULL;
    $image->update('category',$delete_data,"i_id='".$_POST['delete_cat']."'");
    if($image){
      echo 1;
      exit;
    }else{
      echo 0;
    }
  }
  
?>
<!DOCTYPE html>
<html lang="en">
  <title> Class Demo </title>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" integrity="sha512-eyHL1atYNycXNXZMDndxrDhNAegH2BDWt1TmkXJPoGf1WLlNYt08CSjkqF5lnCRmdm3IrkHid8s2jOUY4NIZVQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.js" integrity="sha512-Fq/wHuMI7AraoOK+juE5oYILKvSPe6GC5ZWZnvpOO/ZPdtyA29n+a5kVLP4XaLyDy9D1IBPYzdFycO33Ijd0Pg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>       
  </head>
  <style>
    .parsley-errors-list li.parsley-required {
      color: red;
    }
  </style>
  <body class="bg-secondary">
    <nav>
      <?php include 'navbar.php'; ?>
    </nav>
    <div class="container mt-5">
      <div class="alert alert-danger alert-dismissable col-md-12 pull-right mt-5" id="delete_alert" style="display:none">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b> Record Deleted successfully</b>
      </div>
      <?php if(isset($_GET['message'])){
      if($_GET['message'] == 'insert'){ ?>
      <div class="alert alert-success alert-dismissable col-md-12 pull-right mt-5" id="success_insert">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b> Record Inserted successfully</b>
      </div>
      <?php } elseif($_GET['message'] == 'update'){ ?>
      <div class="alert alert-success alert-dismissable col-md-12 pull-right" id="success_update">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Record Updated successfully</b>
      </div>
      <?php } elseif($_GET['message'] == 'image_failed'){ ?>
      <div class="alert alert-danger alert-dismissable col-md-12 pull-right" id="image_failed">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Something Wrong Image!</b>
      </div>
      <?php } elseif($_GET['message'] == 'failed'){ ?>
      <div class="alert alert-danger alert-dismissable col-md-12 pull-right" id="failed">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Category Already Exist!</b>
      </div>
      <?php } } ?>                       
      <div class="card bg-light mb-3">
        <div class="card-header">
          <?php 
            if(isset($_GET['action']) && $_GET['action']=="insert"){
              echo "<center><h3>Add Category</h3></center>";
            }elseif(isset($_GET['action']) && $_GET['action']=="edit"){
              echo "<center><h3>Edit Category</h3></center>";
            } else {
              echo "<center><h1>Category Form</h1></center>";
            } 
          ?>
        </div>
        <div class="card-body">
          <?php if(isset($_GET['action']) && $_GET['action']=="insert"){ ?>
          <form method="POST" action="" enctype="multipart/form-data" id="form" data-parsley-validate>
            <div class="row">
              <div class="col-md-12 mb-2">
                <div class="form-floating">
                  <label for="v_category_photo">Category Image</label>
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="v_category_image" name="v_category_image" onchange="preview()" required>
                    <center><img id="image_view" src="" width="100px;"/></center>
                    <label class="custom-file-label" for="v_category_photo" id="v_category_photo">Please Select Category Image</label>
                  </div>
                </div>
              </div>
              <div class="col-md-12 mb-2">
                <div class="form-floating">
                  <label for="v_category_name">Category Name</label>
                  <input class="form-control" id="v_category_name" type="text" name="v_category_name" placeholder="Enter Your Category Name" required/>
                </div>
              </div>
              <div class="col-md-12 mb-2">
                <div class="form-floating">
                  <label for="i_cat_qty">Order</label>
                  <input class="form-control" id="i_cat_qty" type="number" name="i_cat_qty" placeholder="Enter Your Order Quantity" required/>
                </div>
              </div>
              <div class="col-md-12 mb-2">
                <div class="form-floating">
                  <label for="v_status">Status</label>
                  <select class="form-control" name="v_status" id="v_status" onchange="changeMessage(this)" required>
                    <option value="">Select Status</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                  </select>
                </div>
              </div><br><br>
            </div>
            <div class="mt-4 ml-2">
              <button class="btn btn-success" type="submit" name="category_success" id="category_success">Save</button>
              <button class="btn btn-secondary"><a href="manage_category.php" style="color:inherit; text-decoration: none;"> Back </a></button>
            </div>
          </div>
        </form>
      </div>
      <?php } elseif(isset($_GET['action']) && $_GET['action']=="edit"){
        if(isset($_GET['i_id']) && !empty($_GET['i_id'])){
          $i_id=$_GET['i_id'];
          $edit = new database();
          $where=array(            
            "i_id"=>$_GET['i_id']
          );
          $edit->select("category",$where);
          $result = $edit->sql;
          $row = mysqli_fetch_assoc($result);
        }
      ?>
      <form method="POST" action="" enctype="multipart/form-data" id="edit_form" data-parsley-validate>
        <input type="hidden" name="i_id" value="<?php echo $_GET['i_id'] ?>">
          <div class="row">
            <div class="col-md-12 mb-2">
              <div class="form-floating">
                <label for="v_category_name">Category Name</label>
                <input class="form-control" id="v_category_name" type="text" name="v_category_name" value="<?php echo $row['v_category_name'];?>" placeholder="Enter Your Category Name" required/>
              </div>
            </div>
            <div class="col-md-12 mb-2">
              <div class="form-floating">
                <label for="i_cat_qty">Order</label>
                <input class="form-control" id="i_cat_qty" type="number" name="i_cat_qty" placeholder="Enter Your Order Quantity" value="<?php echo $row['i_cat_qty'];?>" required/>
              </div>
            </div>
            <div class="col-md-12 mb-2">
              <div class="form-floating">
                <label for="v_status">Status</label>
                <select class="form-control" name="v_status" id="v_status" onchange="changeMessage(this)" required>
                  <option value="">Select Status</option>
                  <option value="1" <?php echo $row['v_status'] == 1 ? "selected": "";?>>Active</option>
                  <option value="0" <?php echo $row['v_status'] == 0 ? "selected": "";?>>Inactive</option>
                </select>
              </div>
            </div>
            <div class="col-md-12 mb-2">
              <div class="form-floating mb-3">
                <label for="category_photo">Image</label>
                <center><div class="custom-file">
                   <input type="file" class="custom-file-input mt-2" id="v_category_image" name="v_category_image" onchange="new_preview(this)">
                   <input type="hidden" name="old_category_image" id="old_category_image" value="<?php echo isset($row['v_category_image']) ? $row['v_category_image'] : ''?>">
                   <label class="custom-file-label" for="category_photo" id="category_photo" name="category_image">Please Select Category Image</label>
                   <img id="image_display_view" src="assets/image/<?php echo $row['v_category_image'];?>" width="100px;" height="100px;">
                   <button type="button" class="btn btn-danger" data-id="<?php echo $row['i_id'] ?>" id="delete_image">Delete </button>
                </div></center>
              </div>
            </div>
          </div>
          <div class="mt-4 ml-2">
            <button class="btn btn-success" type="submit" name="category_edit" id="category_edit">Save</button>
            <button class="btn btn-secondary"><a href="manage_category.php" style="color:inherit; text-decoration: none;"> Back </a></button>
          </div>
        </div>
      </form>
      <?php } else {?>
      <a class="btn btn-primary" href="manage_category.php?action=insert">Add Category</a><br><br>
      <table class="table stripe my-table table-hover" style="width:100%">
        <thead style="text-align: center">
          <tr style="background-color: #708090;">
            <th>Id</th>
            <th>Image</th>
            <th>Category Name</th>
            <th>Order</th>
            <th>Status</th>
            <th>Created Date</th>
            <th>Modified Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $select="SELECT * from category";
            $select=$select." Order BY category.i_id DESC";
            $results=$category->select_all_record($select);
            while($row = mysqli_fetch_array($results)){
          ?>
          <tr align="center" id="<?php echo $row['i_id'];?>">
            <td><?php echo $row['i_id'];?></td>
            <td><img src="assets/image/<?php echo $row['v_category_image']; ?>" height="100px;" width="100px;"></td>
            <td><?php echo $row['v_category_name']; ?></td>
            <td><?php echo $row['i_cat_qty']; ?></td>
            <td><?php echo $row['v_status'] == 1 ? 'Active': 'Inactive';?></td>
            <td><?php echo $row['created_date'];?></td>
            <td><?php echo $row['modified_date'];?></td>
            <td><a href="manage_category.php?action=edit&i_id=<?php echo $row["i_id"]; ?>" class="btn btn-info">Edit</a>
              <button class="btn btn-danger" data-id="<?php echo $row['i_id']  ?>" id="delete">Delete </button>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php } ?>
    </div>
  </body>
  <script src="assets/js/jquery.validate.js"></script>
  <script type="text/javascript">
    $(function() {
      $('#form').parsley();
    });

    $(function()
    {          
      $("form#edit_form").validate(
      {
        rules: 
        {
          "v_category_image": {
            required : function(element) 
            {
              if($("#old_category_image").val()== '') {
                return true;
              } else {
                return false;
              }
            },
            extension: "jpeg|png"
          },
          message: 
          {
            maxlength: 1024
          }
        },
        messages: 
        {
          "v_category_image": 
          {
            required: "Please Select Your Image.",
            extension: "Please Select jpeg or png image.",
          },
          message: 
          {
            maxlength: jQuery.format("Please limit the message to {0} letters!")
          }
        }
      });	
    });

    $(document).ready(function () {
        hideAlert(); 
    });
    function hideAlert()
    {
      window.setTimeout(function() {
        $(".alert").fadeOut(1000, 0).slideUp(1000, function(){
          $(this).hide(); 
        });
      }, 1000); 
    }

    $("#v_category_image").change(function() {
      $("#v_category_photo").text(this.files[0].name);
    });

    function preview() {
      image_view.src = URL.createObjectURL(event.target.files[0]);
    }
    
    $("#v_category_image").change(function() {
      $("#category_photo").text(this.files[0].name);
    });
    
    function new_preview() 
    {
      image_display_view.src = URL.createObjectURL(event.target.files[0]);
      $('#image_display_view').show();
    }

    function changeMessage(v_status)
    {
      var selectedText = v_status.options[v_status.selectedIndex].innerHTML;
      var selectedValue = v_status.value;
    }
    
    $('#success_update').fadeTo(1000,1).fadeOut(1000);
    $('#success_insert').fadeTo(1000,1).fadeOut(1000);
    $('#image_failed').fadeTo(1000,1).fadeOut(1000);
    $('#failed').fadeTo(1000,1).fadeOut(1000);
   
    $(document).on('click', '#delete', function(){
      if(confirm('Are You Sure Delete Record?')) {
        var i_id=$(this).data('id');
          $.ajax({
            type: "POST",
            url: "manage_category.php",
            data: {
              delete: i_id,
            },
            success: function(data)
            {
              if(data){
                $("#delete_alert").css("display", "block");
                $("#delete_alert").text("Deleted Successfully");
                window.setTimeout(function() {
                $("#"+i_id).fadeOut(1000, 0).slideUp(1000, function(){
                  $(this).remove(); 
                });
              }, 1000);
              hideAlert();
            }
          }
        });
      }
    });

    $(document).on('click', '#delete_image', function(){
      if(confirm('Are You Sure Delete Record?')) {
        var i_id=$(this).data('id');
          $.ajax({
            type: "POST",
            url: "manage_category.php",
            data: {
              delete_image: i_id,
            },
            success: function(data){
              if(data){
                $("#old_category_image").val('');
                $("#v_category_image").val('');
                $("#v_category_photo").text('');
                $("#delete_alert").css("display", "block");
                $("#delete_alert").text("Deleted Successfully");
                window.setTimeout(function() {
                $("#image_display_view,#delete_image").fadeOut(1000, 0).slideUp(1000, function(){
                  $(this).hide(); 
                });
              }, 1000);
              hideAlert();
            }
          }
        });
      }
    });
  </script>
</html>