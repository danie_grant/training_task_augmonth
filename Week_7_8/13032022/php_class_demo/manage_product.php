<?php
    session_start();
  
    include 'database.php';

    $product = new database();
  
    if(isset($_POST['product_success']))
    {
        $product_data['v_product_name'] = isset($_POST['v_product_name']) ? $_POST['v_product_name'] : '';
        $product_data['i_price'] = isset($_POST['i_price']) ? $_POST['i_price'] : '';
        $product_data['i_sale_price'] = isset($_POST['i_sale_price']) ? $_POST['i_sale_price'] : '';
        $product_data['i_qty'] = isset($_POST['i_qty']) ? $_POST['i_qty'] : '';
        $product_data['v_product_status'] = isset($_POST['v_product_status']) ? $_POST['v_product_status'] : '';
        $product_data['v_product_code'] = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 7);

        if(isset($_FILES['product_image']['name']) ? $_FILES['product_image']['name'] : '')
        {
            $product_image = explode(".", $_FILES['product_image']['name']);
            $pic = round(microtime(true)) . '.' . end($product_image);
            $type = strtolower(pathinfo($pic,PATHINFO_EXTENSION));
            $arr = array("jpeg","png");
            if(in_array($type,$arr)) 
            {
                $product_data['product_image'] = $pic;
                $pro_img[0]['img_temp_name']=$_FILES['product_image']['tmp_name'];
                $pro_img[0]['img_name']="assets/image/".$pic;
                $product->File_upload($pro_img); 
                header('location:manage_product.php?message=insert');
            }
        }
        if(!empty($product_data) && count($product_data) > 0)
        {
            $last_id=$product->insert_record('product',$product_data);
        }else{
            header('location:manage_product.php?action=list&message=failed');
        }

        if(isset($_FILES['v_product_image']['name']) ? $_FILES['v_product_image']['name'] : '')
        {   
            foreach($_FILES['v_product_image']['name'] as $key=>$val)
            {
                $type = strtolower(pathinfo($val,PATHINFO_EXTENSION));
                $photo = explode(".",$val);
                $pic = rand().'.'.$type;
                $arr = array("jpeg","png");
                if(in_array($type,$arr))
                {
                    $i_img[$key]['img_temp_name']=$_FILES['v_product_image']['tmp_name'][$key];
                    $i_img[$key]['img_name']="assets/image/".$pic;
                    $product->File_upload($i_img);
                    $product->insert_record('images',['v_product_image'=>$pic,'i_product_id'=>$last_id]);
                    header('location:manage_product.php?message=insert');
                } else {
                    header('location:manage_product.php?action=list&message=image_failed');
                    exit;
                }
            }
        }
    }   

    if (isset($_POST['product_edit'])) 
    {
        $edit = new database();
        $i_id=$_POST['i_id'];
        $where=array(            
            "i_id"=>$_POST['i_id']
        );
        $edit->select("product",$where);
        $result = $edit->sql;
        $row = mysqli_fetch_assoc($result);

        $edit_data['v_product_name'] = isset($_POST['v_product_name']) ? $_POST['v_product_name'] : '';
        $edit_data['i_price'] = isset($_POST['i_price']) ? $_POST['i_price'] : '';
        $edit_data['i_sale_price'] = isset($_POST['i_sale_price']) ? $_POST['i_sale_price'] : '';
        $edit_data['i_qty'] = isset($_POST['i_qty']) ? $_POST['i_qty'] : '';
        $edit_data['v_product_status'] = isset($_POST['v_product_status']) ? $_POST['v_product_status'] : '';
        $pic = isset($_POST['old_product_image']) ? $_POST['old_product_image'] : '';
        
        if(isset($_FILES['product_image']['name']) ? $_FILES['product_image']['name'] : '')
        {
            $product_image = explode(".", $_FILES['product_image']['name']);
            $pic = round(microtime(true)) . '.' . end($product_image);
            $type = strtolower(pathinfo($pic,PATHINFO_EXTENSION));
            $arr = array("jpeg","png");
            if(in_array($type,$arr))
            {
                $edit_data['product_image'] = $pic;
                $pro_img[0]['img_temp_name']=$_FILES['product_image']['tmp_name'];
                $pro_img[0]['img_name']="assets/image/".$pic;
                $edit->File_upload($pro_img);
            }
        }else{
            header('location:manage_product.php?action=list&message=image_failed');
        }
        
        if(isset($_FILES['v_product_image']['name']) ? $_FILES['v_product_image']['name'] : '')
        {
            foreach($_FILES['v_product_image']['name'] as $key=>$val)
            {
                $type = strtolower(pathinfo($val,PATHINFO_EXTENSION));
                $photo = explode(".",$val);
                $pic = rand().'.'.$type;
                $arr = array("jpeg","png");    
                if(in_array($type, $arr))
                {
                    $cat_img[$key]['img_temp_name']=$_FILES['v_product_image']['tmp_name'][$key];
                    $cat_img[$key]['img_name']="assets/image/".$pic;
                    $edit->insert('images',['v_product_image'=>$pic,'i_product_id'=>$i_id]);
                    $product->File_upload($cat_img);
                    header('location:manage_product.php?action=list&message=update');
                } 
            }
        }
        if(!empty($edit_data) && count($edit_data) > 0)
        {
            $edit->update_record("product",$edit_data,"i_id='$i_id'");
            header('location:manage_product.php?message=update');
            exit;
        }else {
            header('location:manage_product.php?message=failed');
        }
    }
  
    
    if(isset($_POST['delete']))
    {
        $delete = new database();  
        $i_id=$_POST['delete'];
        $where=array("i_id" =>$_POST['delete']);
        $delete->select("product",$where);
        $result = $delete->sql;
        $row = mysqli_fetch_assoc($result);
        if(!empty($row['product_image']))
        {
            unlink("assets/image/".$row['product_image']);
        }   
        $delete->delete_record('product',"i_id='$i_id'",$where);
        if($delete){
        echo 1;
        }else{
        echo 0;
        }
    }

    if(isset($_POST['delete_image']))
    {
        $image = new database();  
        $i_id=$_POST['delete_image'];
        $where=array("i_id" =>$_POST['delete_image']);
        $image->select("product",$where);
        $result = $image->sql;
        $row = mysqli_fetch_assoc($result);
        if(!empty($row['product_image']))
        {
            if(file_exists("assets/image/".$row['product_image']))
            {
                unlink("assets/image/".$row['product_image']);  
            }
        }    
        $delete_data['product_image']=NULL;
        $image->update_record('product',$delete_data,"i_id='".$_POST['delete_cat']."'");
        if($image){
        echo 1;
        exit;
        }else{
        echo 0;
        }
    }

    if(isset($_POST['image_delete']))
    {
        $image_delete = new database();

        $i_id=$_POST['image_delete'];
        $where=array(            
            'i_id' => $_POST['image_delete']
        );
        $image_delete->select("images", $where);
        $result = $image_delete->sql;
        $row = mysqli_fetch_assoc($result);
        if(!empty($row['v_product_image']))
        {
            if(file_exists("assets/image/".$row['v_product_image']))
            {
                unlink("assets/image/".$row['v_product_image']);
            }
        }
        $image_delete->delete_record('images',"i_id='$i_id'",$where);
        if($image_delete)
        {
        echo 1;
        exit;
        }else{
        echo 0;
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <title> Class Demo </title>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/styles.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" integrity="sha512-eyHL1atYNycXNXZMDndxrDhNAegH2BDWt1TmkXJPoGf1WLlNYt08CSjkqF5lnCRmdm3IrkHid8s2jOUY4NIZVQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.js" integrity="sha512-Fq/wHuMI7AraoOK+juE5oYILKvSPe6GC5ZWZnvpOO/ZPdtyA29n+a5kVLP4XaLyDy9D1IBPYzdFycO33Ijd0Pg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>       
    </head>
    <style>
    .parsley-errors-list li.parsley-required {
        color: red;
    }
    .imgGallery img {
       padding: 8px;
        max-width: 100px;
    }
    .user-panel img {
        height: auto;
        width: 1.8rem;
    }
    </style>
    <body class="bg-secondary">
        <nav>
            <?php include 'navbar.php'; ?>
        </nav>
        <div class="container mt-5">
            <div class="alert alert-danger alert-dismissable col-md-12 pull-right mt-5" id="delete_alert" style="display:none">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> Record Deleted successfully</b>
            </div>
            <?php if(isset($_GET['message'])){
            if($_GET['message'] == 'insert'){ ?>
            <div class="alert alert-success alert-dismissable col-md-12 pull-right mt-5" id="success_insert">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> Record Inserted successfully</b>
            </div>
            <?php } elseif($_GET['message'] == 'update'){ ?>
            <div class="alert alert-success alert-dismissable col-md-12 pull-right" id="success_update">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> Record Updated successfully</b>
            </div>
            <?php } elseif($_GET['message'] == 'image_failed'){ ?>
            <div class="alert alert-danger alert-dismissable col-md-12 pull-right" id="image_failed">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b> Something Wrong Image!</b>
            </div>
            <?php } elseif($_GET['message'] == 'failed'){ ?>
            <div class="alert alert-danger alert-dismissable col-md-12 pull-right" id="failed">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Something Wrong!</b>
            </div>
            <?php } } ?>                       
            <div class="card bg-light mb-3">
                <div class="card-header">
                    <?php 
                        if(isset($_GET['action']) && $_GET['action']=="insert"){
                        echo "<center><h3>Add Product</h3></center>";
                        }elseif(isset($_GET['action']) && $_GET['action']=="edit"){
                        echo "<center><h3>Edit Product</h3></center>";
                        } else {
                        echo "<center><h1>Product Form</h1></center>";
                        } 
                    ?>
                </div>
                <div class="card-body">
                    <?php if(isset($_GET['action']) && $_GET['action']=="insert"){ ?>
                    <form method="POST" action="" enctype="multipart/form-data" id="form" data-parsley-validate>
                        <div class="row">
                            <div class="col-md-12 mb-2">
                                <div class="form-floating">
                                    <label for="v_product_name">Product Name</label>
                                    <input class="form-control" id="v_product_name" type="text" name="v_product_name" placeholder="Enter Your Product Name" required/>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-floating">
                                    <label for="i_price">Price</label>
                                    <input class="form-control" id="i_price" type="number" name="i_price" placeholder="Enter your Price" required/>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-floating">
                                    <label for="i_sale_price">Sale Price</label>
                                    <input class="form-control" id="i_sale_price" type="number" name="i_sale_price" placeholder="Enter your Sale Price" required/>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-floating">
                                    <label for="i_qty">Order</label>
                                    <input class="form-control" id="i_qty" type="number" name="i_qty" placeholder="Enter Your Order Quantity" required/>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-floating">
                                    <label for="v_product_status">Status</label>
                                    <select class="form-control" name="v_product_status" id="v_product_status" onchange="changeMessage(this)" required>
                                        <option value="">Select Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 mb-2">
                                <div class="form-floating">
                                    <label for="product_photo">Product Image</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="product_image" name="product_image" onchange="preview()" required>
                                        <center><img id="image_view" src="" width="100px;"/></center>
                                        <label class="custom-file-label" for="product_photo" id="product_photo">Please Select Product Image</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-2">
                            <div class="form-floating mb-4">
                                <label for="v_product_photo">Product Image Multiple</label>
                                <div class="custom-file">
                                    <input type="file" name="v_product_image[]" class="custom-file-input" id="v_product_image" multiple required>
                                    <label class="custom-file-label" for="v_product_photo" id="v_product_photo">Please Select Product Image</label>
                                </div>
                            </div>
                            <div class="user-image mb-3 text-center">
                                <div class="imgGallery"> 
                                </div>
                            </div>
                        </div>
                        <div class="mt-4 ml-2">
                            <button class="btn btn-success" type="submit" name="product_success" id="product_success">Save</button>
                            <button class="btn btn-secondary"><a href="manage_product.php" style="color:inherit; text-decoration: none;"> Back </a></button>
                        </div>
                    </form>
                </div>
            </div>
            <?php } elseif(isset($_GET['action']) && $_GET['action']=="edit"){
            if(isset($_GET['i_id']) && !empty($_GET['i_id'])){
            $i_id=$_GET['i_id'];
                $edit = new database();
                $where=array(            
                "i_id"=>$_GET['i_id']
                );
            $edit->select("product",$where);
            $result = $edit->sql;
            $row = mysqli_fetch_assoc($result);
            }
            ?>
            <form method="POST" action="" enctype="multipart/form-data" id="edit_form" data-parsley-validate>
                <input type="hidden" name="i_id" value="<?php echo $_GET['i_id'] ?>">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <div class="form-floating">
                            <label for="v_product_name">Product Name</label>
                            <input class="form-control" id="v_product_name" type="text" name="v_product_name" placeholder="Enter Your Product Name" value="<?php echo $row['v_product_name'];?>" required/>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="form-floating">
                            <label for="i_price">Price</label>
                            <input class="form-control" id="i_price" type="number" name="i_price" placeholder="Enter your Price" value="<?php echo $row['i_price'];?>" required/>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="form-floating">
                            <label for="i_sale_price">Sale Price</label>
                            <input class="form-control" id="i_sale_price" type="number" name="i_sale_price" placeholder="Enter your Sale Price" value="<?php echo $row['i_sale_price'];?>" required/>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="form-floating">
                            <label for="i_qty">Order</label>
                            <input class="form-control" id="i_qty" type="number" name="i_qty" placeholder="Enter Your Order Quantity" value="<?php echo $row['i_qty'];?>" required/>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="form-floating">
                            <label for="v_product_status">Status</label>
                            <select class="form-control" name="v_product_status" id="v_product_status" onchange="changeMessage(this)" required>
                                <option value="">Select Status</option>
                                <option value="1" <?php echo $row['v_product_status'] == 1 ? "selected": "";?>>Active</option>
                                <option value="0" <?php echo $row['v_product_status'] == 0 ? "selected": "";?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 mb-2">
                        <div class="form-floating">
                            <label for="product_photo">Product Image</label>
                            <div class="custom-file">
                                <center><input type="file" class="custom-file-input" id="product_image" name="product_image" onchange="new_preview()">
                                <input type="hidden" name="old_product_image" id="old_product_image" value="<?php echo isset($row['product_image']) ? $row['product_image'] : ''?>">
                                <label class="custom-file-label" for="product_photo" id="product_photo" name="product_photo">Please Select Product Image</label>
                                <img id="image_display_view" src="assets/image/<?php echo $row['product_image'];?>" width="100px;" height="100px;">
                                <button type="button" class="btn btn-danger" data-id="<?php echo $row['i_id'] ?>" id="delete_image">Delete </button>
                            </center>
                            </div>
                        </div>
                    </div>
                <div class="col-md-6 MB-2">
                        <div class="form-floating mb-3">
                            <label for="v_product_photo">Product Multiple Image</label>
                            <div class="custom-file">
                            <input type="file" name="v_product_image[]" class="custom-file-input" id="v_product_image" multiple>
                            <label class="custom-file-label" for="v_product_photo" id="v_product_photo">Please Select Product Image</label>
                            <?php 
                                $images = new database();
                                $results=$images->select_all_record("Select * from images where i_product_id='$i_id'");
                                while($rows = mysqli_fetch_assoc($results))
                                {
                                    if($rows['v_product_image'])
                                    {
                                ?>
                                <center><img class='product_thumb_image mt-2' src="assets/image/<?php echo $rows['v_product_image'];?>" width="100px;" height="100px;">
                                <button type="button" class="btn btn-danger" data-id="<?php echo $rows['i_id'] ?>" id="image_delete">Delete</button>
                                </center>
                                <?php } } ?>
                                <div class="user-image text-center">
                                    <div class="imgGallery"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-4 ml-2">
                    <button class="btn btn-success" type="submit" name="product_edit" id="product_edit">Save</button>
                    <button class="btn btn-secondary"><a href="manage_product.php" style="color:inherit; text-decoration: none;"> Back </a></button>
                </div>
            </form>
        </div>
        <?php } else { ?>
        <a class="btn btn-primary" href="manage_product.php?action=insert">Add Product</a><br><br>
        <table class="table stripe my-table table-hover" style="width:100%">
            <thead style="text-align: center">
                <tr style="background-color: #708090;">
                    <th>Id</th>
                    <th>Image</th>
                    <th>Product Name</th>
                    <th>Product Code</th>
                    <th>Price</th>
                    <th>Sale Price</th>
                    <th>Order</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $select="SELECT * from product";
                    $select=$select." Order BY product.i_id DESC";
                    $results=$product->select_all_record($select);
                    while($row = mysqli_fetch_array($results)){
                ?>
                <tr align="center" id="<?php echo $row['i_id'];?>">
                    <td><?php echo $row['i_id'];?></td>
                    <td><img src="assets/image/<?php echo $row['product_image']; ?>" height="100px;" width="100px;"></td>
                    <td><?php echo $row['v_product_name']; ?></td>
                    <td><?php echo $row['v_product_code']; ?></td>
                    <td><?php echo $row['i_price'];?></td>
                    <td><?php echo $row['i_sale_price'];?></td>
                    <td><?php echo $row['i_qty'];?></td>
                    <td><?php echo $row['v_product_status'] == 1 ? 'Active': 'Inactive';?></td>
                    <td><a href="manage_product.php?action=edit&i_id=<?php echo $row["i_id"]; ?>" class="btn btn-info">Edit</a>
                        <button class="btn btn-danger" data-id="<?php echo $row['i_id']  ?>" id="delete">Delete </button>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <?php } ?>
    </body>
    <script src="assets/js/jquery.validate.js"></script>
    <script type="text/javascript">
        $(function() 
        {
            $('#form').parsley();
        });

        $(function()
        {          
            $("form#edit_form").validate(
            {
                rules: 
                {
                "product_image": {
                    required : function(element) 
                    {
                        if($("#old_product_image").val()=='') {
                            return true;
                        } else {
                            return false;
                        }
                    },
                        extension: "jpeg|png"
                    },
                message: 
                {
                    maxlength: 1024
                } 
                },
                messages: 
                {
                    "product_image": 
                    {
                        required: "Please Select Your Image.",
                        extension: "Please Select jpeg or png image.",
                    },
                    message: 
                    {
                        maxlength: jQuery.format("Please limit the message to {0} letters!")
                    }
                }
            });	
        });

        $(document).ready(function () 
        {
            hideAlert(); 
        });
        function hideAlert()
        {
            window.setTimeout(function() {
            $(".alert").fadeOut(1000, 0).slideUp(1000, function(){
            $(this).hide(); 
            });
            }, 1000); 
        }

        $("#product_image").change(function() {
            $("#product_photo").text(this.files[0].name);
        });

        function preview() {
            image_view.src = URL.createObjectURL(event.target.files[0]);
        }
        
        $("#product_image").change(function() {
            $("#product_photo").text(this.files[0].name);
        });
        
        function new_preview() {
            image_display_view.src = URL.createObjectURL(event.target.files[0]);
            $('#image_display_view').show();

        }

        function changeMessage(v_product_status) {
            var selectedText = v_product_status.options[v_product_status.selectedIndex].innerHTML;
            var selectedValue = v_product_status.value;
        }
       
        $(function()
        {
            var multiImgPreview = function (input, imgPreviewPlaceholder) 
            {
                if (input.files) {
                    var filesAmount = input.files.length;
                    for (i = 0; i < filesAmount; i++) 
                    {
                        var reader = new FileReader();
                        reader.onload = function (event) 
                        {
                            $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                        }
                        reader.readAsDataURL(input.files[i]);
                    }
                }
            };
            $('#v_product_image').on('change',function() 
            {
                multiImgPreview(this, 'div.imgGallery');
            });
        });

        $('#success_update').fadeTo(1000,1).fadeOut(1000);
        $('#success_insert').fadeTo(1000,1).fadeOut(1000);
        $('#image_failed').fadeTo(1000,1).fadeOut(1000);
        $('#failed').fadeTo(1000,1).fadeOut(1000);
    
        $(document).on('click', '#delete', function(){
            if(confirm('Are You Sure Delete Record?')) {
                var i_id=$(this).data('id');
                $.ajax({
                    type: "POST",
                    url: "manage_product.php",
                    data: {
                    delete: i_id,
                    },
                    success: function(data){
                        if(data)
                        {
                            $("#delete_alert").css("display", "block");
                            $("#delete_alert").text("Deleted Successfully");
                            window.setTimeout(function() {
                            $("#"+i_id).fadeOut(1000, 0).slideUp(1000, function(){
                            $(this).remove(); 
                            });
                            }, 1000);
                            hideAlert();
                        }
                    }
                });
            }
        });

        
    $(document).on('click', '#delete_image', function(){
      if(confirm('Are You Sure Delete Record?')) {
        var i_id=$(this).data('id');
          $.ajax({
            type: "POST",
            url: "manage_product.php",
            data: {
              delete_image: i_id,
            },
            success: function(data){
              if(data){
                $("#old_product_image").val('');
                $("#product_image").val('');
                $("#product_photo").text('');
                $("#delete_alert").css("display", "block");
                $("#delete_alert").text("Deleted Successfully");
                window.setTimeout(function() {
                $("#image_display_view,#delete_image").fadeOut(1000, 0).slideUp(1000, function(){
                  $(this).hide(); 
                });
              }, 1000);
              hideAlert();
            }
          }
        });
      }
    });

    $(document).on('click', '#image_delete', function(){
            if (confirm('Are You Sure Delete Record?')){
                var i_id=$(this).data('id');
                $.ajax({
                    type: "POST",
                    url: "manage_product.php",
                    data: {
                        image_delete: i_id,
                    },
                    success: function(data){
                        if(data){
                            $("#delete_alert").css("display", "block");
                            $("#delete_alert").html("Deleted Successfully");
                            window.setTimeout(function(){
                                $("#image_delete").fadeOut(1000, 0).slideUp(1000, function(){
                                $(this).hide(); 
                                setTimeout('location.reload(),100');
                                });
                            }, 500);
                            hideAlert();
                        }
                    }
                });
            }
        });
    </script>
</html>