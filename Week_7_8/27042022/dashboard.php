<!DOCTYPE html>
<html lang="en">
        <title>Demo</title>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    </head>
    <style>
        <style>
        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }

        td, th {
        border: 1px solid #dddddd;
        text-align: center;
        padding: 8px;
        }

        tr:nth-child(even) {
        background-color: #dddddd;
        }
        tr:hover {
            background-color: coral;
        }
        thead, th{
            background: #666633;
            color: white;
        }
    </style>
    </style>
    <body>
        <div class="container mt-5">
            <table class="table table-stripe table-hover">
                <thead>
                    <th>Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Action</th>
                    <th>Logout</th>
                </thead>
                <tbody>
                    <?php
                        include 'database.php';
                        $sql = "SELECT * FROM user";
                        $result = $conn->query($sql);
                        if($result && !empty($result) && $result->num_rows > 0)
                        {
                            while($data = $result->fetch_assoc())
                            {
                            ?>
                            <tr>
                                <td><?php echo $data['id']?></td>
                                <td><?php echo $data['v_first_name']?></td>
                                <td><?php echo $data['v_last_name']?></td>
                                <td><?php echo $data['v_email']?></td>
                                <td>
                                    <a class="btn btn-warning" href="edit.php?id=<?php echo $data['id'] ?>">Edit</a>
                                    <a class="btn btn-danger" href="delete.php?id=<?php echo $data['id'] ?>">Delete</a>
                                </td>
                                <td><a class="btn btn-primary" href="logout.php">Logout</a></td>
                            </tr>
                            <?php } }
                    ?>
                </tbody>
            </table>
        </div>
	</body>
</html>