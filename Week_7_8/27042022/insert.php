<?php
    include 'database.php';
    if(isset($_POST['Save']))
	{
		$first_name = isset($_POST['v_first_name']) ? $_POST['v_first_name']: '';
		$last_name = isset($_POST['v_last_name']) ? $_POST['v_last_name'] : '';
        $email = isset($_POST['v_email']) ? $_POST['v_email']:'';
		$password = isset($_POST['password']) ? $_POST['password'] :'';
		
		$sql="INSERT INTO user(v_first_name,v_last_name,v_email,password) VALUES('$first_name','$last_name','$email','$password')";

		if($conn->query($sql)===TRUE)
		{
			echo '<script>alert("Successfully Inserted")</script>';
			echo '<script>document.location="dashboard.php"</script>';
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
    <title>Demo</title>
    <head>
	<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container mt-5">
			<form method="POST">
				<div class="card">
		  			<h5 class="card-header">Insert Record</h5>
		  			<div class="card-body">
		    			<div>
							First Name : <input class="col-md-12 form-control mt-2" type="text" name="v_first_name" id="v_first_name" placeholder="Enter Your First Name" required>
						</div><br>
                        <div>
                            Last Name : <input class="col-md-12 form-control" type="text" name="v_last_name" id="v_last_name" placeholder="Enter Your Last Name" required><br>
                        </div>
						<div>
							Email : <input class="col-md-12 form-control mt-2" type="email" name="v_email" id="v_email" placeholder="Enter Your Email Id" required>
						</div><br>
						<div>
							password : <input class="col-md-12 form-control mt-2" type="password" name="password" id="password" placeholder="Enter Your password" required>
						</div><br>	
						
						<input type="submit" class="btn btn-success" name="Save" id="Save" value="Save">
						<a class="btn btn-success" href="index.php">Back</a>
					  </div>
					</div>
				</div>
			</form>
		</div>
    </body>
</html>