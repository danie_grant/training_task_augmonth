<?php 
	include 'database.php';
	session_start();
	$id=$_GET['id'];
	$sql_data="SELECT * FROM user WHERE id='$id'";
	$row = $conn->query($sql_data);

	$data = $row->fetch_assoc();

	if(isset($_POST['Update']))
	{
		$first_name = isset($_POST['v_first_name']) ? $_POST['v_first_name']: '';
		$last_name = isset($_POST['v_last_name']) ? $_POST['v_last_name'] : '';
        $email = isset($_POST['v_email']) ? $_POST['v_email']:'';
		$password = isset($_POST['password']) ? $_POST['password'] :'';
		
		$sql="UPDATE `user` SET v_first_name='$first_name',v_last_name='$last_name',v_email='$email',password='$password' WHERE id='$id'";

		if($conn->query($sql)===TRUE)
		{
			echo '<script>alert("Successfully Updated")</script>';
			echo '<script>document.location="dashboard.php"</script>';
		}
	}
?>

<!DOCTYPE html>
<html>
	<title>Edit Form</title>
	<head>
	<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	</head>
	<body>
		<div class="container mt-5">
			<form method="POST">
				<div class="card">
		  			<h5 class="card-header">Edit Record</h5>
					  <div class="card-body">
                      <div class="card-body">
		    			<div>
							First Name : <input class="col-md-12 form-control mt-2" type="text" name="v_first_name" id="v_first_name" placeholder="Enter Your First Name" value="<?php echo $data['v_first_name']?>" required>
						</div><br>
                        <div>
                            Last Name : <input class="col-md-12 form-control" type="text" name="v_last_name" id="v_last_name" placeholder="Enter Your Last Name" value="<?php echo $data['v_last_name']?>" required><br>
                        </div>
						<div>
							Email : <input class="col-md-12 form-control mt-2" type="email" name="v_email" id="v_email" placeholder="Enter Your Email Id" value="<?php echo $data['v_email']?>" required>
						</div><br>
						<div>
							password : <input class="col-md-12 form-control mt-2" type="password" name="password" id="password" placeholder="Enter Your password" value="<?php
                    echo $data['password']?>" required>
						</div><br>	
						<input class="btn btn-success" type="submit" name="Update" id="Update" value="Update">
						<a class="btn btn-success" href="index.php">Back</a>
		  			</div>
				</div>
			</form>
		</div>
	</body>
</html>