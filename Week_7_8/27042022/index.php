<?php 
	include 'database.php';
	session_start();

	if(isset($_POST['Login']))
	{
		$email = $_POST['v_email'];
		$password = $_POST['password'];

		$sql = "SELECT v_email,password FROM user where v_email='$email' And password='$password'";
		$data = $conn->query($sql);
		$row = $data->fetch_assoc();

		if($row > 0)
		{
			echo '<script>alert("Successfully Login")</script>';
			echo '<script>document.location="dashboard.php"</script>';
		}
	}
?>

<!DOCTYPE html>
<html>
	<title>Login Form</title>
	<head>
	<meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	</head>
	<body>
		<div class="container mt-5">
			<form method="POST">
				<div class="card">
	  				<h5 class="card-header">Login</h5>
				  	<div class="card-body">
						<div>
							Email : <input class="col-md-12 form-control mt-2" type="email" name="v_email" id="v_email" placeholder="Enter Your Email Id" required>
						</div><br><br>
						<div>
							password : <input class="col-md-12 form-control mt-2" type="password" name="password" id="password" placeholder="Enter Your password" required>
						</div><br><br>
						<input class="btn btn-primary" type="submit" name="Login" id="Login" value="Login">
						<a class="btn btn-success" href="insert.php">Register</a>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>