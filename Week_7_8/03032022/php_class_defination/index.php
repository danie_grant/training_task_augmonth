<?php 
  session_start();

  include 'database.php';
  
  $login = new database();

  if (isset($_POST['sign_in'])) 
  {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $select = "SELECT * from users where v_email='$email' And v_password='$password'";
    $results=$login->select_all($select);
    $row = $results->fetch_assoc();
    if(!empty($row))
    {
      $pass=$row['v_password'];
      if($v_password=$pass)
      {
        $_SESSION['user']=$row['v_email'];
        $_SESSION['success']="Login Successfully";
        header("location:dashboard.php");
      }
    } else {
      header('location:index.php?message=failed');
    }
  }

  if (isset($_POST['register'])) 
  {
    $reg_data['v_fname'] = $_POST['v_fname'];
    $reg_data['v_lname'] = $_POST['v_lname'];
    $reg_data['v_email'] = $_POST['v_email'];
    $reg_data['v_password'] = $_POST['v_password'];
    if($_POST["v_password"] === $_POST['conform_password'])
    {
      $where=array(            
        "v_email"=>$_POST['v_email']
      );
      $login->select("users",$where);
      $result = $login->sql;
      $row = mysqli_num_rows($result);
      if($row>0)
      {
        header('location:index.php?action=register&message=user_login');
      } else {
         $login->insert('users',$reg_data);
        header('location:index.php?action=login&message=insert');
      } 
    }
  }
  
  if(isset($_POST['logout'])){
    unset($_SESSION['user']);
    header('location:index.php?action=login&message=logout');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include 'inc/head.php'; ?>
  </head>
  <style>
    body{
      font-family: Arial, Sans-serif;
    }
    .error {
      color:red;
      font-family:verdana, Helvetica;
    }
  </style>
  <body>
    <div class="container mt-5">
    <?php if(isset($_GET['message']))
    {
      if($_GET['message'] == 'insert')
      { ?>
      <div class="alert alert-success alert-dismissable col-md-12 pull-right mt-5">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b> Record Inserted successfully</b>
      </div>
      <?php } elseif($_GET['message'] == 'reg_login')
      { ?>
      <div class="alert alert-success alert-dismissable col-md-12 pull-right mt-5">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b> Record Inserted successfully Please Login Now</b>
      </div>
      <?php } elseif($_GET['message'] == 'logout'){ ?>
        <div class="alert alert-success alert-dismissable col-md-12 pull-right">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Logout successfully</b>
        </div>
        <?php } elseif($_GET['message'] == 'failed'){ ?>
        <div class="alert alert-danger alert-dismissable col-md-12 pull-right">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Something Wrong!</b>
        </div>
        <?php } elseif($_GET['message'] == 'user_login'){ ?>
        <div class="alert alert-danger alert-dismissable col-md-12 pull-right">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Email Already Exist!</b>
        </div>
        <?php } } ?>
        <?php if(isset($_SESSION['success'])){ ?>
      <div class="alert alert-success alert-dismissible fade show" role="alert">
      <strong><?php echo $_SESSION['success']; ?></strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php } ?>
      <?php if(isset($_GET['action']) && $_GET['action']!=null && $_GET['action'] == "register") {?>
      <div class="card bg-light mb-3">
        <div class="card-header">Registration</div>
          <div class="card-body">
            <form method="POST" enctype="multipart/form-data" action="" id="registration_form" name="registration_form">
              <div class="form-group">
                <label for="exampleInputEmail1">First Name</label>
                <input type="text" class="form-control" id="v_fname" name="v_fname" placeholder="Please Enter First Name">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Last Name</label>
                <input type="text" class="form-control" id="v_lname" name="v_lname" placeholder="Please Enter Last Name">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" class="form-control" id="v_email" name="v_email" placeholder="Please Enter Your Email Id">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="v_password" name="v_password" placeholder="Please Enter Your Password">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Confirm Password</label>
                    <input type="password" class="form-control" id="conform_password" name="conform_password" placeholder="Enter Your Conform Password">
              </div>
              <button type="submit" class="btn btn-primary" id="register" name="register">Registration</button>
              <a href="index.php?action=login" class="btn btn-primary" id="sign_in" name="sign_in">Already Have Account Login Now</a>
            </form>
          </div>
        </div>
        <?php } else { ?>
        <div class="card bg-light mb-3">
          <div class="card-header">Login</div>
            <div class="card-body">
              <form method="POST" enctype="multipart/form-data" action="" id="login_form" name="login_form">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input type="email" class="form-control" id="email" name="email">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" class="form-control" id="password" name="password">
                </div>
                <button type="submit" class="btn btn-primary" id="sign_in" name="sign_in">Login</button>
                <a href="index.php?action=register" class="btn btn-primary">Not Have Account Register Now</a>
              </form>
            </div>
          </div>
          <?php } ?>
      </div>
    </div>
    <footer>
      <?php include 'inc/footer.php'; ?>
    </footer>
    <script>
      $(function()
      {          
        $("form#login_form").validate(
        {
          rules: 
          {
            email: 
            {
              required: true,
              email:true,
            },
            password: 
            {
              required: true,
            },
            message: 
            {
              maxlength: 1024
            }
          },
          messages: 
          {
            email: 
            {
              required: "Please Enter Your Email Id."
            },
            password: 
            {
              required: "Please Your Password."
            },
            message: 
            {
              maxlength: jQuery.format("Please limit the message to {0} letters!")
            }
          }
        });	
      });
    </script>
    <script>  
      $(function(){          
        $.validator.addMethod("pwcheck",
          function(value, element) {
            return /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,10}$/.test(value);
          });
        $("form#registration_form").validate({
          rules: 
          {
            v_fname: 
            {
                required: true,
            },
            v_lname: 
            {
              required: true,
            },
            v_email: 
            {
              required: true,
              email:true,
            },
            v_password: 
            {
              required: true,
              minlength: 6,
              maxlength:10, 
              pwcheck: true
            },
            conform_password: 
            {
              required: true,
              equalTo: "#v_password"
            },
            message: 
            {
              maxlength: 1024
            }
          },
          messages: 
          {
            v_fname: 
            {
              required: "Please Enter Your First Name."
            },
            v_lname: 
            {
              required: "Please Enter Your Last Name."
            },
            v_email: 
            {
              required: "Please Enter Your Email Id."
            },
            v_password: 
            {
              required: "Please Enter Your Password.",
              minlength:"Password must have min 6 length",
              maxlength:"Password should not have more then 10 length",
              pwcheck: "Password should contain 1 capital letter, 1 lower case letter, 1 special character & 1 digit"
            },
            conform_password: 
            {
              required: "Please Enter Your Confirm Password.",
              equalTo: "Both password should same"
            },
            message: 
            {
              maxlength: jQuery.format("Please limit the message to {0} letters!")
            }
          }
        });	
      }); 
    </script>
  </body>
</html>