<?php
	$servername = 'localhost';
	$username = 'root';
	$password = '';
	$dbname = "constant_test";
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
    define("status", [
        "Active",
        "InActive",
        "Delete",
        "Inprogress"
    ]);
   
    $gender = array(
        "Male", 
        "Female", 
        "Other"
    ); 
   
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </head>
    <style>
        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }

        td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        }

        tr:nth-child(even) {
        background-color: #dddddd;
        }
    </style>
    <body>
        <div class="container">
            <center><h2 class="mt-2 mb-2">Constant Demo</h2></center>
            <table class="table" border="1px" align="center" id="example">
                <thead class="thead-dark" align="center">
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>Status</th>
                </thead>
                <tbody>
                <?php
                    $select = "SELECT * FROM constant_demo";
                    $result = $conn->query($select);
                    if ($result->num_rows > 0) 
                    {
                        while($row = $result->fetch_assoc()) {
                        if($row["status"] == 1){
                            $status=status[0];
                        } elseif($row["status"] == 2){
                            $status=status[1];
                        }elseif($row["status"] == 3){
                            $status=status[2];
                        }else{
                            $status=status[3];
                        }

                        if($row["gender"] == 1){
                            $gender[0];
                            
                        } elseif($row["gender"] == 2){
                            $gender[1];
                        }else{
                           $gender[2];
                        }
                        
                    ?>
                    <tr align="center">
                        <td><?php echo $row['id']; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $status; ?></td>
                        <td><?php echo $gender[0]; ?></td>
                    </tr>
                    <?php }} else {
                        echo "<No Record Found";
                    }?>
                </tbody>
            </table>
        </div>
    </body>
</html>

