
<?php 
  $link= str_replace("/php/","",$_SERVER['REQUEST_URI']);
?>
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="dashboard.php">Store Managment</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class='nav-item <?php echo $link=="dashboard.php" ? 'active':'' ?>'>
        <a class="nav-link" href="dashboard.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class='nav-item <?php echo $link=="manage_category.php" ? 'active':'' ?>'>
        <a class="nav-link" href="manage_category.php">Category</a>
      </li>
      <li class='nav-item <?php echo $link=="manage_product.php" ? 'active':'' ?>'>
        <a class="nav-link" href="manage_product.php">Product</a>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
      <label class="form-control mr-sm-2 bg-dark"><b style="color:white;"><?php echo $_SESSION['user'];?></b></label>
      <form action="index.php" method="POST"> 
        <button class="btn btn-outline-danger my-2 my-sm-0" type="submit" name="logout">Logout</button>
      </form>
    </div>
  </div>
</nav>
<br><br>