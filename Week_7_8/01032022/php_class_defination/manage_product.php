<?php
  session_start();
  include 'database.php';

  $product = new database();

  if(empty($_SESSION))
  {
    header('location:index.php');
  }

  if(isset($_POST['product_success']))
  {
      $product_data['v_product_name'] = $_POST['v_product_name'];
      $product_data['i_price'] = $_POST['i_price'];
      $product_data['i_sale_price'] = $_POST['i_sale_price'];
      $product_data['i_qty'] = $_POST['i_qty'];
      $product_data['v_product_status'] = $_POST['v_product_status'];
      $product_data['v_product_code'] =substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 7);

      $arr = array('jpeg','png');
      $photo = explode(".", $_FILES['product_image']['name']);
      $filename = round(microtime(true));
      $pic2 = $filename . '.' . end($photo);
      $pic_tmp = $_FILES['product_image']['tmp_name'];
      $path = "assets/image/";
      $path = $path.$pic2;
      $sourceProperties = getimagesize($pic_tmp);
      $ext = pathinfo($_FILES['product_image']['name'], PATHINFO_EXTENSION);
      if(!in_array($ext, $arr))
      {
        header('location:manage_product.php?action=list&message=image_failed');exit;
      }
      
      $fileNewName = time();
      $folderPath = "assets/image/thumbnail/";
      if (!file_exists('assets/image/thumbnail/')) {
          mkdir('assets/image/thumbnail/', 0777, true);
      }
      $imageType = $sourceProperties[2];
      switch ($imageType) {
      case IMAGETYPE_PNG:
          $imageResourceId = imagecreatefrompng($pic_tmp); 
          $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'100','100');
          imagepng($targetLayer,$folderPath. $filename. "_100_100.". $ext);
          break;
      case IMAGETYPE_JPEG:
          $imageResourceId = imagecreatefromjpeg($pic_tmp); 
          $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'100','100');
          imagejpeg($targetLayer,$folderPath. $filename. "_100_100.". $ext);
          break;
      default:
          echo "Invalid Image type.";
          exit;
          break;
      }
      if($sourceProperties[0] > 1024 && $sourceProperties[1] > 720)
      {
          $imageType = $sourceProperties[2];
          switch ($imageType) {
          case IMAGETYPE_PNG:
              $imageResourceId = imagecreatefrompng($pic_tmp); 
              $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1], '1024','720');
              imagepng($targetLayer,$path);
              break;
          case IMAGETYPE_JPEG:
              $imageResourceId = imagecreatefromjpeg($pic_tmp); 
              $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'1024','720');
              imagejpeg($targetLayer,$path);
              break;
          default:
              echo "Invalid Image type.";
              exit;
              break;
          }
      } else {
          $product_img[0]['img_temp_name']=$_FILES['product_image']['tmp_name'];
          $product_img[0]['img_name']="assets/image/".$pic2;
          $product->File_upload($product_img);
      } 
      $product_data['product_image'] = $pic2;
      $last_id=$product->insert('product',$product_data);
      foreach($_FILES['v_product_image']['name'] as $key=>$val)
      {
          $type = strtolower(pathinfo($val,PATHINFO_EXTENSION));
          $photo = explode(".",$val);
          $pic = uniqid().'.'.$type;
          $arr = array("jpeg","png");
          if(in_array($type,$arr))
          {
              $cat_img[$key]['img_temp_name']=$_FILES['v_product_image']['tmp_name'][$key];
              $cat_img[$key]['img_name']="assets/image/".$pic;
              $product->File_upload($cat_img);
              $product->insert('images',['v_product_image'=>$pic,'v_status'=>0,'i_product_id'=>$last_id]);
              header('location:manage_product.php?action=list&message=insert');
          } else {
              header('location:manage_product.php?action=list&message=failed');
          }
      }
      foreach($_POST['i_cat_id'] as $key => $val)
      {
          $product->insert('tbl_relation_category_product',['i_cat_id'=>$val,'i_product_id'=>$last_id]);
      }
  }
  
  function imageResize($imageResourceId,$width,$height,$targetWidth,$targetHeight) {
    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);
    return $targetLayer;
  }

    $priceOrder = "asc";
    $salepriceOrder = "asc";
    $i_qty="asc";
    $orderBy = !empty($_GET["orderby"]) ? $_GET["orderby"] : "created_at";
    $order = !empty($_GET["order"]) ? $_GET["order"] : "desc";
    $orders ="ORDER BY ". $orderBy . " " . $order;
    if($orderBy == "i_price" && $order == "asc") {
        $priceOrder = "desc";
    }
    if($orderBy == "i_sale_price" && $order == "asc") {
        $salepriceOrder = "desc";
    }
    if($orderBy == "i_qty" && $order == "asc") {
        $i_qty = "desc";
    }

    if(isset($_POST['delete_id']))
    {
      $delete = new database();  
      $i_id=$_POST['delete_id'];
      $where=array("i_id" =>$_POST['delete_id']);
      $delete->select("product",$where);
      $result = $delete->sql;
      $row = mysqli_fetch_assoc($result);
      unlink("assets/image/".$row['product_image']);
      $delete->delete('product',"i_id='$i_id'",$where);
      if($delete){
        echo 1;
        exit;
      }else{
        echo 0;
        exit;
      }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <title>Product</title>
    <head>
        <?php include 'inc/head.php';?>
    </head>
    <body>
        <?php include 'inc/navbar.php';?>
        <div class="container mt-5">
      <div class="alert alert-danger alert-dismissable col-md-12 pull-right mt-5" id="delete_alert" style="display:none">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b> Record Deleted successfully</b>
      </div>
      <?php if(isset($_GET['message'])){
      if($_GET['message'] == 'insert'){ ?>
      <div class="alert alert-success alert-dismissable col-md-12 pull-right mt-5">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b> Record Inserted successfully</b>
      </div>
      <?php } elseif($_GET['message'] == 'update'){ ?>
        <div class="alert alert-success alert-dismissable col-md-12 pull-right">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Record Updated successfully</b>
        </div>
        <?php } else if($_GET['message'] == 'delete'){
          ?>
          <div class="alert alert-success alert-dismissable col-md-12 pull-right mt-5" id="delete">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> Record Deleted successfully</b>
        </div>
      <?php } elseif($_GET['message'] == 'image_failed'){ ?>
        <div class="alert alert-danger alert-dismissable col-md-12 pull-right">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Something Wrong Image!</b>
        </div> 
        <?php } }?>
        <div class="card bg-light mb-3">
            <div class="card-header"><?php 
                if(isset($_GET['action']) && $_GET['action']=="insert"){
                echo "Add Product";
                }elseif(isset($_GET['action']) && $_GET['action']=="edit"){
                echo "Edit Product";
                }else{
                echo "Product";
                } ?>
            </div>
            <div class="card-body">
                <?php if(isset($_GET['action']) && $_GET['action']=="insert"){ ?>
                <form method="POST" action="" enctype="multipart/form-data" id="product_form" name="product_form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-floating">
                                <label>Category Name</label>
                                <select class="category form-control" name="i_cat_id[]" id="i_cat_id" multiple>
                                    <option label="Please Select Category"></option>
                                    <?php 
                                        $category = new database();
                                        $where=array(
                                            "v_status"=>1,
                                        );
                                        $category->select("category",$where);
                                        $result = $category->sql;
                                        while($row = mysqli_fetch_assoc($result)){
                                        ?>                                                
                                        <option value="<?php echo $row['i_id']?>"><?php echo $row['v_category_name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating mb-4">
                                <label for="v_product_photo">Product Image Multiple</label>
                                <div class="custom-file">
                                    <input type="file" name="v_product_image[]" class="custom-file-input" id="v_product_image" multiple>
                                    <label class="custom-file-label" for="v_product_photo" id="v_product_photo">Please Select Product Image</label>
                                </div>
                            </div>
                            <div class="user-image mb-3 text-center">
                                <div class="imgGallery"> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating mb-3">
                                <label for="v_product_name ">Product Name</label>
                                <input class="form-control" id="v_product_name" type="text" name="v_product_name" placeholder="Enter your Product Name"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <label for="i_price">Price</label>
                                <input class="form-control" id="i_price" type="number" name="i_price" placeholder="Enter your Price"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <label for="i_sale_price">Sale Price</label>
                                <input class="form-control" id="i_sale_price" type="number" name="i_sale_price" placeholder="Enter your Sale Price"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating mb-3">
                                <label for="i_qty">Order Qty</label>
                                <input class="form-control" id="i_qty" type="number" name="i_qty" placeholder="Enter Your Order Quantity"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <label for="v_product_status">Status</label>
                                <select class="form-control" name="v_product_status" id="v_product_status" onchange="changeMessage(this)">
                                    <option value="">Select Status</option>
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating mb-3">
                                <label for="v_product_photo">Product Main Image</label>
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" id="product_image" name="product_image" onchange="preview()">
                                <center><img id="image_view" src="" width="100px;"/></center>
                                <label class="custom-file-label" for="v_product_photo" id="v_product_photo">Please Select Product Main Image</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4 ml-2">
                        <button class="btn btn-success" type="submit" name="product_success" id="product_success">Save</button>
                        <button class="btn btn-secondary"><a href="manage_product.php" style="color:inherit"> Back </a></button>
                    </div>
                </form>
            </div>
        
        <?php } else { ?>
            <a class="btn btn-success" href="manage_product.php?action=insert">Add Product</a><br><br>
        <form method="POST" autocomplete="off" enctype="multipart/form-data" action="" id="reset_form">
            <div class="row">
                <div class="col-sm">
                    <input class="form-control" id="search_product" type="text" name="search_product" placeholder="Search Product Name" value="<?php echo isset($_POST['search_product']) ? $_POST['search_product'] : ''?>"/>
                </div>
                <div class="col-sm">
                    <input class="form-control" id="search_price" type="number" name="search_price" placeholder="Search Product Price" value="<?php echo isset($_POST['search_price']) ? $_POST['search_price'] : ''?>"/>
                </div>
                <div class="col-sm">
                    <input class="form-control" id="search_qty" type="number" name="search_qty" placeholder="Search Product Qty" value="<?php echo isset($_POST['search_qty']) ? $_POST['search_qty'] : ''?>"/>
                </div>
                <div class="col-sm">
                    <select class="form-control" name="search" id="search">
                        <option value="">All Record Status</option>
                        <option value='active' <?php echo (isset($_POST['search']) && $_POST['search']=='active') ? 'selected' : ''?>>Active</option>
                        <option value='inactive' <?php echo (isset($_POST['search']) && $_POST['search']=='inactive') ? 'selected' : ''?>>Inactive</option>
                    </select>
                </div>
                <div class="col-sm">
                    <button class="btn btn-success" type="submit" name="product_search" id="product_search">Search</button>
                    <button class="btn btn-info" type="reset" name="product_reset" id="product_reset">Reset</button>
                </div>
            </div>
        </form><br>
        <table class="table table stripe my-table table-hover table-responsive">
            <thead style="text-align: center" >
                <tr style="background-color: #708090;">
                <th>Id</th>
                <th>Image</th>
                <th>Category Name</th>
                <th>Product Name</th>
                <th>Product Code</th>
                <th><a href="?orderby=i_price&order=<?php echo $priceOrder; ?>">Price</th>
                <th><a href="?orderby=i_sale_price&order=<?php echo $salepriceOrder; ?>">Sale Price</th>
                <th><a href="?orderby=i_qty&order=<?php echo $i_qty; ?>">Qty</th>
                <th>Order</th>
                <th>Status</th>
                <th>Created at</th>
                <th>Updated at</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $per_page_record = 5;       
                if (isset($_GET["page"])) {    
                    $page = $_GET["page"];    
                } else {    
                    $page=1;    
                }    
                $where= array();
                if(isset($_POST['product_search'])) 
                {
                   if(!empty($_POST['search_product']))
                   {
                        $where[]= " v_product_name like '%".$_POST['search_product']."%'";
                   }
                   if(!empty($_POST['search']))
                   {
                        $status = '';
                        if(isset($_POST['search']) && $_POST['search'] == 'active'){
                          $status = 1;
                        }
                        else if(isset($_POST['search']) && $_POST['search'] == 'inactive'){
                          $status = 0;
                        }
                        $where[]= " v_product_status='".$status."'";
                   }
                   if(!empty($_POST['search_price']))
                   {
                       $where[]= " i_price='".$_POST['search_price']."'";
                   }
                   if(!empty($_POST['search_qty']))
                   {
                       $where[]= " i_qty='".$_POST['search_qty']."'";
                   }
                }
                if(!empty($where))
                {
                    $where = " WHERE " . implode(' AND ',$where);
                }else{
                    $where = '';
                }
                $page = isset($_GET['page']) ? $_GET['page'] : 1;
                $offset = ($page - 1)  * $per_page_record;
                $select="SELECT distinct product.*,GROUP_CONCAT(category.v_category_name) as category_name from product LEFT JOIN tbl_relation_category_product ON product.i_id=tbl_relation_category_product.i_product_id LEFT JOIN category ON tbl_relation_category_product.i_cat_id=category.i_id";
                $select=$select.$where;
                $select=$select." GROUP BY product.i_id ";
                $select=$select.$orders." LIMIT ".$offset.",". $per_page_record;
                $results=$product->select_all($select);
                $count_record = $results->num_rows;
                $counter = 0;
                if($results->num_rows < 1)
                {
                    $select="SELECT distinct product.*,GROUP_CONCAT(category.v_category_name) as category_name from product LEFT JOIN tbl_relation_category_product ON product.i_id=tbl_relation_category_product.i_product_id LEFT JOIN category ON tbl_relation_category_product.i_cat_id=category.i_id";
                    $select=$select.$where;
                    $select=$select." GROUP BY product.i_id ";
                    $select=$select.$orders." LIMIT ". $per_page_record;
                    $results=$product->select_all($select);
                }
    
                if (!empty($results) && $results->num_rows > 0) {
                while ($row = $results->fetch_assoc()) {
                ?>
                <tr align="center" id="<?php echo $row['i_id'];?>">
                    <td><?php echo $row['i_id']; ?></td>
                    <?php
                        $explodedImageName = array_filter(explode('.',$row['product_image']));
                        $imageName = '';
                        if(!empty($explodedImageName))
                        {
                            $imageName = $explodedImageName[0].'_100_100.'.$explodedImageName[1];
                        }
                    ?>
                    <td><img src="assets/image/thumbnail/<?php echo $imageName;?>"></td>
                    <td><?php echo $row['category_name'];?></td>
                    <td><?php echo $row['v_product_name']; ?></td>
                    <td><?php echo $row['v_product_code'];?></td>
                    <td><?php echo $row['i_price']; ?></td>
                    <td><?php echo $row['i_sale_price']; ?></td>
                    <td><?php echo $row['i_qty']; ?></td>
                    <td><?php echo ++$counter;?></td>
                    <td><?php echo $row['v_product_status'] == 1 ? '<span class="badge badge-success">Active</span>': '<span class="badge badge-danger">Inactive</span>';?></td>
                    <td><?php echo $row['created_at']; ?></td>
                    <td><?php echo $row['updated_at']; ?></td>
                    <td><a href="manage_product.php?action=edit&i_id=<?php echo $row["i_id"]; ?>" class="btn btn-warning">Edit</a>
                        <button class="btn btn-danger" data-id="<?php echo $row['i_id']  ?>" id="delete_id">Delete </button>
                    </td>
                </tr>
                <?php } } else {
                    echo "No Record Found";
                } ?>
            </tbody>
            <tfoot style="text-align: center">
                <tr style="background-color: #708090;">
                    <th>Id</th>
                    <th>Image</th>
                    <th>Category Name</th>
                    <th>Product Name</th>
                    <th>Product Code</th>
                    <th>Price</th>
                    <th>Sale Price</th>
                    <th>Qty</th>
                    <th>Order</th>
                    <th>Status</th>
                    <th>Created at</th>
                    <th>Updated at</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
        <?php } ?>
        <footer class="bg-dark fixed-bottom" style="color:white;">
            <?php include 'inc/footer.php'; ?>
        </footer>
        <script>
            $("#i_cat_id").select2({
                placeholder: "Select Category Name",
                allowClear: true
            });

            $(function(){
                var multiImgPreview = function (input, imgPreviewPlaceholder){
                    if (input.files) {
                        var filesAmount = input.files.length;
                        for (i = 0; i < filesAmount; i++) 
                        {
                            var reader = new FileReader();
                            reader.onload = function (event) 
                            {
                                $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                            }
                            reader.readAsDataURL(input.files[i]);
                        }
                    }
                };
                $('#v_product_image').on('change',function() 
                {
                    multiImgPreview(this, 'div.imgGallery');
                });
            });
            
            $("#v_product_image").change(function() {
                $("#v_product_photo").text(this.files[0].name);
            });
            
            function preview() {
                image_view.src = URL.createObjectURL(event.target.files[0]);
            }

            $(document).on('click','#product_reset',function(){
            $("#search_product,#search_price,#search_qty,#search").val('');
            $("#reset_form").submit();
        });


        $(document).on('click', '#delete_id', function()
        {
            if (confirm('Are You Sure Delete Record?')) 
            {
                var i_id=$(this).data('id');
                $.ajax({
                    type: "POST",
                    url: "manage_product.php",
                    data: {
                        delete_id: i_id,
                    },
                    success: function(data){
                    if(data){
                        $("#delete_alert").css("display", "block");
                        $("#delete_alert").text("Product Deleted Successfully");
                        window.setTimeout(function() {
                        $("#"+i_id).fadeOut(1000, 0).slideUp(1000, function(){
                        $(this).remove(); 
                        });
                    }, 5000);
                    hideAlert();
                    }
                }
                });
            }
        });



        $(function()
        {          
            $("form#product_form").validate(
            {
                rules: 
                {
                    "i_cat_id[]" : 
                    {
                        required: true,
                    },
                    "v_product_image[]": 
                    {
                        required : function(element) {
                        if($("#v_product_image").val() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    },
                        required: true,
                        extension: "jpeg|png"
                    },
                    "product_image": 
                    {
                        required : function(element) {
                        if($("#product_image").val() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    },
                        required: true,
                        extension: "jpeg|png"
                    },
                    "v_product_name": 
                    {
                        required: true,
                    },
                    "i_price": 
                    {
                        required: true,
                    },
                    "i_sale_price": 
                    {
                        required: true,
                        min : function(element) {
                            return parseFloat($("#i_price").val())+1;
                        },
                    },
                    "i_qty": 
                    {
                        required: true,
                    },
                    "v_product_status": 
                    {
                        required: true,
                    },
                    message: 
                    {
                        maxlength: 1024
                    }
                },
                messages: 
                {
                    "i_cat_id[]" : 
                    {
                        required: "Please Select Category."
                    },
                    "v_product_image[]" : 
                    {
                        required: "Please Select Your Product Multiple Image.",
                        extension: "Please Select jpeg or png image.",
                    },
                    "product_image" : 
                    {
                        required: "Please Select Your Product Main Image.",
                        extension: "Please Select jpeg or png image.",
                    },
                    "v_product_name": 
                    {
                        required: "Please Enter Your Product Name."
                    },
                    "i_price": 
                    {
                        required: "Please Enter Your Price."
                    },
                    "i_sale_price": 
                    {
                        required: "Please Enter Your Sale Price."
                    },
                    "i_qty": 
                    {
                        required: "Please Enter Your Order Qty."
                    },
                    "v_product_status": 
                    {
                        required: "Please Select Your Product Status."
                    },
                    message: 
                    {
                        maxlength: jQuery.format("Please limit the message to {0} letters!")
                    }
                }
            });	
        }); 
        </script>
    </body>
</html>