<?php 
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "demo";
    $conn = new mysqli($servername,$username,$password,"$dbname");

    if($conn->connect_error)
    {
        die("Failed".$conn->connect_error);
    }
?>
<style>
    .center {
        margin-left: auto;
        margin-right: auto;
    }
</style>
<table border='2px' cellpadding='5px' class='center'>
    <thead>
        <th>Id</th>
        <th>Name</th>
        <th>Start Date</th>
        <th>Message</th>
    </thead>
    <?php
    
    $current_date = date('Y-m-20');

    $sql = "SELECT * from test_subscribers";
    $result = $conn->query($sql);
    
    while($data = $result->fetch_assoc())
    {
        $date1 = new DateTime(date('Y-m-d'));
        $date2 = new DateTime($data['date']);
        $Months = $date1->diff($date2);
        $sub_month = $Months->y + $Months->m + 2 .(" Email Send"); 
        ?>
        <tr>
            <td><?php echo $data['id']?></td>
            <td><?php echo $data['v_name']?></td>
            <td><?php echo $data['date']?></td>
            <td><?php echo $current_date.' '.$sub_month; ?></td>
        </tr> 
    <?php } ?>
</table>