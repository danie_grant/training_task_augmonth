<?php

use Illuminate\Support\Facades\Route;
use App\Http\controllers\Auth\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('login', [App\Http\controllers\Auth\AuthController::class, 'index'])->name('login');
Route::post('post-login', [App\Http\controllers\Auth\AuthController::class, 'postLogin'])->name('login.post'); 
Route::get('registration', [App\Http\controllers\Auth\AuthController::class, 'registration'])->name('register');
Route::post('post-registration', [App\Http\controllers\Auth\AuthController::class, 'postRegistration'])->name('register.post'); 
Route::get('dashboard', [AuthController::class, 'dashboard']); 
Route::get('logout', [AuthController::class, 'logout'])->name('logout');