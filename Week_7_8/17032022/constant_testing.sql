-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2022 at 05:57 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `constant_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `constant_demo`
--

CREATE TABLE `constant_demo` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `gender` enum('1','2','3') NOT NULL COMMENT '''male'',''female'',''other''',
  `status` enum('1','2','3','4') NOT NULL COMMENT '''Active'',''InActive'',''Delete'',''''Inprogress'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `constant_demo`
--

INSERT INTO `constant_demo` (`id`, `name`, `email`, `gender`, `status`) VALUES
(1, 'dev', 'daniegrant.cis@gmail.com', '1', '1'),
(2, 'sita', 'sita@gmail.com', '2', '1'),
(3, 'poonam', 'poonam@gmail.com', '2', '2'),
(4, 'gita', 'gita@gmail.com', '2', '2'),
(5, 'xyz', 'xyz@gmail.com', '3', '3'),
(6, 'abc', 'abc@gmail.com', '3', '4'),
(7, 'mahesh', 'mahesh1@gmail.com', '1', '2'),
(8, 'ram', 'ram@gmail.com', '1', '3'),
(9, 'rajesh', 'rajesh@gmail.com', '1', '3'),
(10, 'paresh', 'paresh@gmail.com', '1', '4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `constant_demo`
--
ALTER TABLE `constant_demo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `constant_demo`
--
ALTER TABLE `constant_demo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
