<?php
	//Database Connection
    $servername = 'localhost';
	$username = 'root';
	$password = '';
	$dbname = "constant_testing";
	$conn = new mysqli($servername, $username, $password, $dbname);
	
    if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
    
    //Constant Status
    define("status", [
        "Active",
        "InActive",
        "Delete",
        "Inprogress"
    ]);

    //Show Gender Details
    $gender = array(
        "Male", 
        "Female", 
        "Other"
    ); 
?>
<!DOCTYPE html>
<html>
    <title>Constant Demo</title>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    </head>

    <style>
        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }

        td, th {
        border: 1px solid #dddddd;
        text-align: center;
        padding: 8px;
        }

        tr:nth-child(even) {
        background-color: #dddddd;
        }
        tr:hover {
            background-color: coral;
        }
        thead, th{
            background: #666633;
            color: white;
        }
    </style>
    
    <body>
        <div class="container">
            <center><h2 class="mt-2 mb-2">Constant Demo</h2></center>
            <table class="table .table-hover">
                <thead>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    <?php
                        $select = "SELECT * FROM constant_demo";
                        $result = $conn->query($select);
                        if($result && !empty($result) && $result->num_rows > 0)
                        {
                            while($row = $result->fetch_assoc()) {

                            //Constant Status active inactive Delete and Inprogress
                            if(isset($row["status"]) && $row["status"] == 1){
                                $status=status[0];
                            } elseif(isset($row["status"]) && $row["status"] == 2){
                                $status=status[1];
                            }elseif(isset($row["status"]) && $row["status"] == 3){
                                $status=status[2];
                            }else{
                                $status=status[3];
                            }

                            //Gender Male Female and Other
                            if(isset($row["gender"]) && $row["gender"] == 1)
                            {
                            $genders = $gender[0];
                            } elseif(isset($row["gender"]) && $row["gender"] == 2){
                                $genders = $gender[1];
                            }else{
                                $genders = $gender[2];
                            }
                        ?>
                        <tr>
                            <!-- Table Data  Show -->
                            <td><?php echo $row['id']; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['email']; ?></td>
                            <td><?php echo $genders; ?></td>
                            <td><?php echo $status; ?></td>
                        </tr>
                    <?php }} ?>
                </tbody>
            </table>
        </div>
    </body>
</html>

