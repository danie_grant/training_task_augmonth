<?php
    $servername = 'localhost';
	$username = 'root';
	$password = '';
	$dbname = "constant_testing";
	$conn = new mysqli($servername, $username, $password, $dbname);
	
    if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
    
    define("status", [
        "Active",
        "InActive",
    ]);

    $Designation = array(
        "Developing", 
        "Designing", 
        "QA",
        "Other"
    ); 
?>
<!DOCTYPE html>
<html>
    <title>Constant</title>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    </head>

    <style>
        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }

        td, th {
        border: 1px solid #dddddd;
        text-align: center;
        padding: 8px;
        }
    </style>
    
    <body>
        <div class="container">
            <center><h2 class="mt-2 mb-2">Constant </h2></center>
            <table class="table .table-hover">
                <thead>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Designation</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    <?php
                        $select = "SELECT * FROM constant";
                        $result = $conn->query($select);
                        if($result->num_rows > 0)
                        {
                            while($row = $result->fetch_assoc()) {
                            if($row["designation"]== 1)
                            {
                                $ans = $Designation[0];
                            } elseif($row["designation"] == 2){
                                $ans = $Designation[1];
                            }elseif($row["designation"] == 3){
                                $ans = $Designation[2];
                            }else{
                                $ans = $Designation[3];
                            }
                            
                            if($row['status']==1)
                            {
                                $stat=status[0];
                            }else {
                                $stat=status[1];
                            }

                        ?>
                        <tr>
                            <td><?php echo $row['id']; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['email']; ?></td>
                            <td><?php echo $ans; ?></td>
                            <td><?php echo $stat; ?></td>
                        </tr>
                    <?php } } ?>
                </tbody>
            </table>
        </div>
    </body>
</html>

