<style>
    .center {
        margin-left: auto;
        margin-right: auto;
        }
</style>
<?php
    $student_data = [
        "Dev" => [
            "Email" => "Admin@gmail.com",
            "Maths" => 69,
            "English"=> 55,
            "Gujarati" => 88, 
        ],
        "Meet" => [
            "Email" => "Test@gmail.com",
            "Maths" => 39,
            "English"=> 80,
            "Gujarati" => 75, 
        ],
        "Shiv" => [
            "Email" => "Demo@gmail.com",
            "Maths" => 80,
            "English"=> 89,
            "Gujarati" => 78, 
        ],
    ];

    echo "<br>";
    echo "<table border='2px' cellpadding='5px' class='center'> <tr><th>Name</th><th>Email</th><th>Maths</th><th>English</th><th>Gujarati</th></tr>";
    foreach($student_data as $k => $v)
    {
        echo "<tr>
            <td>$k</td>";
            foreach($v as $list)
            {
                echo "<td>$list</td>";
            }
            echo "</td>";
    }  
    echo "</table>";
?>