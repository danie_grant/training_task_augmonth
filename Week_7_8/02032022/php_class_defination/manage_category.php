<?php
  session_start();
  include 'database.php';

  $category = new database();

  if(empty($_SESSION))
  {
    header('location:index.php');
  }
  
  if(isset($_POST['category_success']))
  {
    $cat_data['v_category_name'] = $_POST['v_category_name'];
    $cat_data['i_cat_qty'] = $_POST['i_cat_qty'];
    $cat_data['v_status'] = $_POST['v_status'];
    $where=array(
        "v_category_name"=>$_POST['v_category_name']
    );
    $arr = array('jpeg','png');
    $photo = explode(".", $_FILES['v_category_image']['name']);
    $filename = round(microtime(true));
    $pic2 = $filename . '.' . end($photo);
    $pic_tmp = $_FILES['v_category_image']['tmp_name'];
    $path = "assets/image/";
    $path = $path.$pic2;
    $sourceProperties = getimagesize($pic_tmp);
    $ext = pathinfo($_FILES['v_category_image']['name'], PATHINFO_EXTENSION);
    if(!in_array($ext, $arr))
    {
      header('location:manage_category.php?action=list&message=failed_image');
    }
    $fileNewName = time();
    $folderPath = "assets/image/thumbnail/";
    if (!file_exists('assets/image/thumbnail/')) {
      mkdir('assets/image/thumbnail/', 0777, true);
    }
    $imageType = $sourceProperties[2];
    switch ($imageType) {
    case IMAGETYPE_PNG:
      $imageResourceId = imagecreatefrompng($pic_tmp); 
      $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'100','100');
      imagepng($targetLayer,$folderPath. $filename. "_100_100.". $ext);
      break;
    case IMAGETYPE_JPEG:
      $imageResourceId = imagecreatefromjpeg($pic_tmp); 
      $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'100','100');
      imagejpeg($targetLayer,$folderPath. $filename. "_100_100.". $ext);
      break;
    default:
      echo "Invalid Image type.";
      exit;
      break;
    }
  
    if($sourceProperties[0] > 1024 && $sourceProperties[1] > 720)
    {
      $imageType = $sourceProperties[2];
      switch ($imageType) {
      case IMAGETYPE_PNG:
        $imageResourceId = imagecreatefrompng($pic_tmp); 
        $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1], '1024','720');
        imagepng($targetLayer,$path);
        break;
      case IMAGETYPE_JPEG:
        $imageResourceId = imagecreatefromjpeg($pic_tmp); 
        $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'1024','720');
        imagejpeg($targetLayer,$path);
        break;
      default:
        echo "Invalid Image type.";
        exit;
        break;
      }
    } else {
      $cat_img[0]['img_temp_name']=$_FILES['v_category_image']['tmp_name'];
      $cat_img[0]['img_name']="assets/image/".$pic2;
      $category->File_upload($cat_img); 
    } 
    $cat_data['v_category_image'] = $pic2;

    $category->select("category",$where);
    $result = $category->sql;
    $row = mysqli_fetch_assoc($result);
    if($row>0)
    {
      $_SESSION['danger']="Category Already  Exist";
      header('location:manage_category.php?action=list&message=failed');
    } 
    $category->insert('category',$cat_data);
    header('location:manage_category.php?action=list&message=insert');
  }

  if (isset($_POST['category_edit'])) 
  {
    $edit = new database();
    $i_id=$_POST['i_id'];
    $where=array(            
       "i_id"=>$_POST['i_id']
    );
    $edit->select("category",$where);
    $result = $edit->sql;
    $row = mysqli_fetch_assoc($result);
    $edit_data['v_category_name'] = $_POST['v_category_name'];
    $edit_data['i_cat_qty'] = $_POST['i_cat_qty'];
    $edit_data['v_status'] = $_POST['v_status'];
    $arr = array('jpeg','png');
    $ext = pathinfo($_POST['old_category_image'], PATHINFO_EXTENSION);
    $pic2 = isset($_POST['old_category_image']) ? $_POST['old_category_image'] : '';
    if ($_FILES['v_category_image']['size'] != 0 && $_FILES['v_category_image']['error'] == 0)
    {
      $photo = explode(".", $_FILES['v_category_image']['name']);
      $filename = round(microtime(true));
      $pic2 = $filename . '.' . end($photo);
      $pic_tmp = $_FILES['v_category_image']['tmp_name'];
      $path = "assets/image/";
      $path = $path.$pic2;
      $sourceProperties = getimagesize($pic_tmp);
      $ext = pathinfo($_FILES['v_category_image']['name'], PATHINFO_EXTENSION);
      if(!in_array($ext, $arr))
      {
        header('location:manage_category.php?action=list&message=failed_image');
      }
      $fileNewName = time();
      $folderPath = "assets/image/thumbnail/";
      if(!file_exists('assets/image/thumbnail/')) 
      {
        mkdir('assets/image/thumbnail/', 0777, true);
      }
      $imageType = $sourceProperties[2];
      switch ($imageType) {
      case IMAGETYPE_PNG:
        $imageResourceId = imagecreatefrompng($pic_tmp); 
        $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'100','100');
        imagepng($targetLayer,$folderPath. $filename. "_100_100.". $ext);
        break;
      case IMAGETYPE_JPEG:
        $imageResourceId = imagecreatefromjpeg($pic_tmp); 
        $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'100','100');
        imagejpeg($targetLayer,$folderPath. $filename. "_100_100.". $ext);
        break;
      default:
        echo "Invalid Image type.";
        exit;
        break;
      }
    }
    if($sourceProperties[0] > 1024 && $sourceProperties[1] > 720)
    {
      $imageType = $sourceProperties[2];
      switch ($imageType) {
      case IMAGETYPE_PNG:
        $imageResourceId = imagecreatefrompng($pic_tmp); 
        $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1], '1024','720');
        imagepng($targetLayer,$path);
        break;
      case IMAGETYPE_JPEG:
        $imageResourceId = imagecreatefromjpeg($pic_tmp); 
        $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1],'1024','720');
        imagejpeg($targetLayer,$path);
        break;
      default:
        echo "Invalid Image type.";
        exit;
        break;
      }
    }
    if(in_array($ext, $arr)){
      $edit_data['v_category_image'] = $pic2;
      $cat_img[0]['img_temp_name']=$_FILES['v_category_image']['tmp_name'];
      $cat_img[0]['img_name']="assets/image/".$pic2;
      $edit->File_upload($cat_img);
      header('location:manage_category.php?action=list&message=update');
    }
    $edit->update("category",$edit_data,"i_id='$i_id'");
    header('location:manage_category.php?action=list&message=update');
  }
  function imageResize($imageResourceId,$width,$height,$targetWidth,$targetHeight) {
    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);
    return $targetLayer;
  }

  //category delete
  if(isset($_POST['delete']))
  {
    $delete = new database();  
    $i_id=$_POST['delete'];
    $where=array("i_id" =>$_POST['delete']);
    $delete->select("category",$where);
    $result = $delete->sql;
    $row = mysqli_fetch_assoc($result);
    unlink("assets/image/".$row['v_category_image']);
    $delete->delete('category',"i_id='$i_id'",$where);
    if($delete){
      echo 1;
      exit;
    }else{
      echo 0;
      exit;
    }
  }
  
  //category delete image
  if(isset($_POST['delete_cat']))
  {
    $image = new database();
    $i_id=$_POST['delete_cat'];
    $where=array(            
       "i_id"=>$_POST['delete_cat']
    );
    $image->select("category", $where);
    $result = $image->sql;
    $row = mysqli_fetch_assoc($result);
    if(file_exists("assets/image/".$row['v_category_image']))
    {
    unlink("assets/image/".$row['v_category_image']);  
    }
    $delete_data['v_category_image']=NULL;
    $image->update('category',$delete_data,"i_id='".$_POST['delete_cat']."'");
    if($image){
      echo 1;
      exit;
    }else{
      echo 0;
      exit;
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
  <title>Store Management</title>
  <head>
    <?php include 'inc/head.php'; ?>
  </head>
  <body>
    <?php include 'inc/navbar.php'; ?>
    <div class="container mt-5">
      <div class="alert alert-danger alert-dismissable col-md-12 pull-right mt-5" id="delete_alert" style="display:none">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b> Record Deleted successfully</b>
      </div>
      <?php if(isset($_GET['message'])){
      if($_GET['message'] == 'insert'){ ?>
      <div class="alert alert-success alert-dismissable col-md-12 pull-right mt-5">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b> Record Inserted successfully</b>
      </div>
      <?php } elseif($_GET['message'] == 'update'){ ?>
        <div class="alert alert-success alert-dismissable col-md-12 pull-right">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Record Updated successfully</b>
        </div>
        <?php } else if($_GET['message'] == 'delete'){
          ?>
          <div class="alert alert-success alert-dismissable col-md-12 pull-right mt-5" id="delete">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> Record Deleted successfully</b>
        </div>
      <?php } elseif($_GET['message'] == 'failed'){ ?>
        <div class="alert alert-danger alert-dismissable col-md-12 pull-right">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b> Category Already Exist!</b>
        </div>
        <?php } elseif($_GET['message'] == 'image_failed'){ ?>
        <div class="alert alert-danger alert-dismissable col-md-12 pull-right">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Something Wrong Image!</b>
        </div>
      <?php } } ?>                       
      <div class="card bg-light mb-3">
        <div class="card-header">
          <?php 
            if(isset($_GET['action']) && $_GET['action']=="insert"){
              echo "Add Category";
            }elseif(isset($_GET['action']) && $_GET['action']=="edit"){
              echo "Edit Category";
            } else {
              echo "Category";
            } 
          ?>
        </div>
        <div class="card-body">
          <?php if(isset($_GET['action']) && $_GET['action']=="insert"){ ?>
          <form method="POST" action="" enctype="multipart/form-data" id="category_form" name="category_form">
            <div class="row">
              <div class="col-md-6">
                <div class="form-floating mb-3">
                  <label for="v_category_photo">Category Image</label>
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="v_category_image" name="v_category_image" onchange="preview()">
                    <center><img id="image_view" src="" width="100px;"/></center>
                    <label class="custom-file-label" for="v_category_photo" id="v_category_photo">Please Select Category Image</label>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-floating">
                  <label for="v_category_name">Category Name</label>
                  <input class="form-control" id="v_category_name" type="text" name="v_category_name" placeholder="Enter Your Category Name"/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-floating">
                  <label for="i_cat_qty">Order</label>
                  <input class="form-control" id="i_cat_qty" type="number" name="i_cat_qty" placeholder="Enter Your Order Quantity"/>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-floating">
                  <label for="v_status">Status</label>
                  <select class="form-control" name="v_status" id="v_status" onchange="changeMessage(this)">
                    <option value="">Select Status</option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                  </select>
                </div>
              </div><br><br>
            </div>
            <div class="mt-4 ml-2">
              <button class="btn btn-success" type="submit" name="category_success" id="category_success">Save</button>
              <button class="btn btn-secondary"><a href="manage_category.php" style="color:inherit"> Back </a></button>
            </div>
          </div>
        </form>
      </div>
      <?php } elseif(isset($_GET['action']) && $_GET['action']=="edit"){
        if(isset($_GET['i_id']) && !empty($_GET['i_id'])){
          $i_id=$_GET['i_id'];
            $edit = new database();
            $where=array(            
              "i_id"=>$_GET['i_id']
            );
          $edit->select("category",$where);
          $result = $edit->sql;
          $row = mysqli_fetch_assoc($result);
        }
      ?>
      <form method="POST" action="" enctype="multipart/form-data" id="category_edit_form" name="category_edit_form">
        <input type="hidden" name="i_id" value="<?php echo $_GET['i_id'] ?>">
          <div class="row">
            <div class="col-md-6">
              <div class="form-floating">
                <label for="v_category_name">Category Name</label>
                <input class="form-control" id="v_category_name" type="text" name="v_category_name" value="<?php echo $row['v_category_name'];?>" placeholder="Enter Your Category Name"/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-floating">
                <label for="i_cat_qty">Order</label>
                <input class="form-control" id="i_cat_qty" type="number" name="i_cat_qty" placeholder="Enter Your Order Quantity" value="<?php echo $row['i_cat_qty'];?>"/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-floating">
                <label for="v_status">Status</label>
                <select class="form-control" name="v_status" id="v_status" onchange="changeMessage(this)">
                  <option value="">Select Status</option>
                  <option value="1" <?php echo $row['v_status'] == 1 ? "selected": "";?>>Active</option>
                  <option value="0" <?php echo $row['v_status'] == 0 ? "selected": "";?>>Inactive</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-floating mb-3">
                <label for="v_category_photo">Image</label>
                <center><div class="custom-file">
                   <input type="file" class="custom-file-input mt-2" id="v_category_image" name="v_category_image" onchange="preview()">
                   <input type="hidden" name="old_category_image" id="old_category_image" value="<?php echo isset($row['v_category_image']) ? $row['v_category_image'] : ''?>">
                   <label class="custom-file-label" for="v_category_photo" id="v_category_photo">Please Select Category Image</label>
                    <?php 
                     if(isset($row['v_category_image']))
                     { 
                    ?>
                    <img id="image_view" src="assets/image/<?php echo $row['v_category_image'];?>" width="100px;" height="100px;">
                    <button type="button" class="btn btn-danger" data-id="<?php echo $row['i_id'] ?>" id="delete_cat">Delete </button>
                  <?php } ?>
                </div></center>
              </div>
            </div>
          </div>
          <div class="mt-4 ml-2">
            <button class="btn btn-success" type="submit" name="category_edit" id="category_edit">Save</button>
            <button class="btn btn-secondary"><a href="manage_category.php" style="color:inherit"> Back </a></button>
          </div>
        </div>
      </form>
      <?php } else {?>
      <a class="btn btn-success" href="manage_category.php?action=insert">Add Category</a><br><br>
      <form method="GET" autocomplete="off" enctype="multipart/form-data" action="" id="reset_form">
        <div class="row">
          <div class="col-sm">
            <input class="form-control" id="search_category" type="text" name="search_category" placeholder="Search Category Name" value="<?php echo isset($_GET['search_category']) ? $_GET['search_category'] : ''?>"/>
          </div>&nbsp;
          <div class="col-sm">
            <select class="form-control" name="search" id="search">
              <option value="">All Record Status</option>
              <option value='Active' <?php echo (isset($_GET['search']) && $_GET['search']=='Active') ? 'selected' : ''?>>Active</option>
              <option value='Inactive' <?php echo (isset($_GET['search']) && $_GET['search']=='Inactive') ? 'selected' : ''?>>Inactive</option>
            </select>
          </div>&nbsp;
          <div class="col-sm">
            <button class="btn btn-success" type="submit" name="category_search" id="category_search">Search</button>
            <button class="btn btn-info" type="reset" name="category_reset" id="category_reset">Reset</button>
          </div>
        </div>
      </form><br>
      <table class="table stripe my-table table-hover" style="width:100%" id="category_table_data">
        <thead style="text-align: center">
          <tr style="background-color: #708090;">
            <th>Id</th>
            <th>Image</th>
            <th>Category Name</th>
            <th>Order</th>
            <th>No of Product</th>
            <th>Status</th>
            <th>Created Date</th>
            <th>Modified Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $per_page_record=2;       
            if(isset($_GET["page"])) {    
              $page = $_GET["page"];    
            } else {    
              $page = 1;    
            }    
            $page = isset($_GET['page']) ? $_GET['page'] : 1; 
            $where="";
            if(isset($_GET['category_search'])) 
            {
              if(!empty($_GET['search_category']) && !empty($_GET['search']))
              {
                $status = '';
                if(isset($_GET['search']) && $_GET['search'] == 'Active'){
                  $status = 1;
                }else if(isset($_GET['search']) && $_GET['search'] == 'Inactive'){
                  $status = 0;
                }
                $where=" where v_status='".$status."' AND v_category_name like '%".$_GET['search_category']."%'";
              }else if(empty($_GET['search_category']) && !empty($_GET['search'])){
                $status = '';
                if(isset($_GET['search']) && $_GET['search'] == 'Active'){
                  $status = 1;
                }else if(isset($_GET['search']) && $_GET['search'] == 'Inactive'){
                  $status = 0;
                }
                $where=" where v_status='".$status."'";    
              } else if(!empty($_GET['search_category']) && empty($_GET['search'])){
                $where= ' where v_category_name like "%'.$_GET['search_category'].'%"';
              }
            }

            $offset = ($page - 1)  * $per_page_record;
            $select ="SELECT c.*, COUNT(i_cat_id) As i_no_of_product FROM category c LEFT JOIN tbl_relation_category_product ON c.i_id=tbl_relation_category_product.i_cat_id ";
            $select=$select.$where;
            $select=$select." GROUP BY c.i_id";
            $select=$select." ORDER BY c.i_id DESC LIMIT ".$offset.",". $per_page_record;
            $results=$category->select_all($select);
            if (!empty($results) && $results->num_rows > 0) {
            while($row = mysqli_fetch_array($results)){
          ?>
          <tr align="center" id="<?php echo $row['i_id'];?>">
            <td><?php echo $row['i_id'];?></td>
          <?php
              $explodedImageName = array_filter(explode('.',$row['v_category_image']));
              $imageName = '';
              if(!empty($explodedImageName))
              {
                $imageName = $explodedImageName[0].'_100_100.'.$explodedImageName[1];
              }
            ?>
            <td><img src="assets/image/thumbnail/<?php echo $imageName;?>"></td>
              <td><?php echo $row['v_category_name']; ?></td>
            <td><?php echo $row['i_cat_qty']; ?></td>
            <td><?php echo $row['i_no_of_product'];?></td>
            <td><?php echo $row['v_status'] == 1 ? '<span class="badge badge-success">Active</span>': '<span class="badge badge-danger">Inactive</span>';?></td>
            <td><?php echo $row['created_date'];?></td>
            <td><?php echo $row['modified_date'];?></td>
            <td><a href="manage_category.php?action=edit&i_id=<?php echo $row["i_id"]; ?>" class="btn btn-warning">Edit</a>
              <button class="btn btn-danger" data-id="<?php echo $row['i_id']  ?>" id="delete">Delete </button>
            </td>
          </tr>
          <?php } } else { echo "No Record Found"; } ?>
        </tbody>
        <tfoot style="text-align: center">
          <tr style="background-color: #708090;">
            <th>Id</th>
            <th>Image</th>
            <th>Category Name</th>
            <th>Order</th>
            <th>No of Product</th>
            <th>Status</th>
            <th>Created Date</th>
            <th>Modified Date</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
      <div class="pagination">    
        <?php  
          $select="SELECT c.*, COUNT(i_cat_id) As i_no_of_product FROM category c LEFT JOIN tbl_relation_category_product ON c.i_id=tbl_relation_category_product.i_cat_id ";
          $select=$select.$where;
          $select=$select." GROUP BY c.i_id";
          $query=$category->select_all($select); 
          $count=$query->num_rows;
          $total_records = $count;  
          echo "</br>";     
          $total_pages=ceil($total_records/$per_page_record);     
          $pagLink="";   
          $queryParams = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
          parse_str($queryParams, $vars);
          unset($vars['page']);
          $queryParams = http_build_query($vars);    
          if($page>=2){   
            echo "<a href='manage_category.php?page=".($page-1)."'>Prev</a>";   
          }       
          for($i=1; $i<=$total_pages; $i++) {   
            if ($i == $page) {   
              $pagLink.="<a class='active' href='manage_category.php?page=".$i.'&'.$queryParams."'>".$i." </a>";   
            } else {   
              $pagLink.="<a href='manage_category.php?page=".$i.'&'.$queryParams."'>".$i."</a>";     
            }   
          };     
            echo $pagLink;   
          if($page<=$total_pages){   
            echo "<a href='manage_category.php?page=".($page+1).'&'.$queryParams."'> Next </a>";   
          }   
        ?>    
      </div>
      <div class="inline">   
        <input id="page" type="number" min="1" max="<?php echo $total_pages?>" placeholder="<?php echo $page."/".$total_pages; ?>" value="<?php echo isset($_GET['page']) ? $_GET['page'] : '1'?>">   
        <button onClick="go2Page();">Go</button>   
      </div>
      <?php }?>
      <footer class="bg-dark fixed-bottom" style="color:white;">
        <?php include 'inc/footer.php'; ?>
      </footer>
    </div>
  </body>
  <script type="text/javascript">
    $("#v_category_image").change(function() {
      $("#v_category_photo").text(this.files[0].name);
    });
    function preview() {
    image_view.src = URL.createObjectURL(event.target.files[0]);
      $("#image_view").show();
      $("#delete_cat").show();
    }
    function changeMessage(v_status) {
      var selectedText = v_status.options[v_status.selectedIndex].innerHTML;
      var selectedValue = v_status.value;
    }
    function go2Page()   
    {   
      var page=document.getElementById("page").value; 
      window.location.href='manage_category.php?page='+page;   
    } 
    
    $(document).on('click','#category_reset',function(){
        $("#search_category,#search").val('');
        $("#reset_form").submit();
      })
    
    function CategoryTableData()
    {
      $("#category_table_data").load(location.href + " #category_table_data"); 
    }

    $(document).on('click', '#delete', function(){
      if (confirm('Are You Sure Delete Record?')) {
        var i_id=$(this).data('id');
          $.ajax({
            type: "POST",
            url: "manage_category.php",
            data: {
              delete: i_id,
            },
            success: function(data){
              if(data){
                $("#delete_alert").css("display", "block");
                $("#delete_alert").text("Category Deleted Successfully");
                window.setTimeout(function() {
                $("#"+i_id).fadeOut(1000, 0).slideUp(1000, CategoryTableData(), function(){
                  $(this).remove(); 
                });
              }, 5000);
              hideAlert();
            }
          }
        });
      }
    });

    $(document).on('click', '#delete_cat', function(){
      if (confirm('Are You Sure Delete Record?')) {
        var i_id=$(this).data('id');
        $.ajax({
          type: "POST",
          url: "manage_category.php",
          data: {
            delete_cat: i_id,
          },
          success: function(data)
          {
            if(data)
            {
              $("#delete_alert").css("display", "block");
              $("#delete_alert").html("Deleted Successfully");
              window.setTimeout(function() {
              $("#image_view,#delete_cat").fadeOut(1000, 0).slideUp(1000, function(){
                $(this).hide(); 
              });
              }, 500);
              hideAlert();
            }
          }
        });
      }
    });
    
    $(function()
    {          
      $("form#category_form").validate(
      {
        rules: 
        {
          v_category_image : {
            required : function(element) {
              if($("#old_category_image").val() == '') {
                  return true;
              } else {
                  return false;
              }
          },
            required : true,
            extension: "jpeg|png"
          },
          v_category_name: 
          {
            required: true,
          },
          i_cat_qty: 
          {
            required: true,
          },
          v_status: 
          {
            required: true,
          },
          message: 
          {
            maxlength: 1024
          }
        },
        messages: 
        {
          v_category_image: 
          {
            required: "Please Select Your Category Image.",
            extension: "Please Select jpeg or png image.",
          },
          v_category_name: 
          {
            required: "Please Enter Your Category Name."
          },
          i_cat_qty: 
          {
            required: "Please Enter Your Category Order Qty."
          },
          v_status: 
          {
            required: "Please Select Your Category Status."
          },
          message: 
          {
            maxlength: jQuery.format("Please limit the message to {0} letters!")
          }
        }
      });	
    }); 

    $(function()
    {          
      $("form#category_edit_form").validate(
      {
        rules: 
        {
          v_category_name: 
          {
            required: true,
          },
          i_cat_qty: 
          {
            required: true,
          },
          v_status: 
          {
            required: true,
          },
          v_category_image: {
              required : function(element) {
                if($("#old_category_image").val() == '') {
                  return true;
                } else {
                  return false;
                }
              },
              extension: "jpeg|png"
            },
          message: 
          {
            maxlength: 1024
          }
        },
        messages: 
        {
          v_category_name: 
          {
            required: "Please Enter Your Category Name."
          },
          i_cat_qty: 
          {
            required: "Please Enter Your Category Order Qty."
          },
          v_status: 
          {
            required: "Please Select Your Category Status."
          },
          v_category_image : 
            {
              required: "Please Select Your Image.",
              extension: "Please Select jpeg or png image.",
            },
          message: 
          {
            maxlength: jQuery.format("Please limit the message to {0} letters!")
          }
        }
      });	
    });
  </script>
</html>