-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2022 at 01:56 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store_managment_class`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `i_id` int(10) NOT NULL,
  `v_category_image` varchar(200) DEFAULT NULL,
  `v_category_name` varchar(200) NOT NULL,
  `i_cat_qty` int(5) NOT NULL,
  `v_status` int(2) NOT NULL DEFAULT 0 COMMENT '1=Active, 0=Inactive',
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified_date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`i_id`, `v_category_image`, `v_category_name`, `i_cat_qty`, `v_status`, `created_date`, `modified_date`) VALUES
(22, '1646392512.jpeg', 'Electronic', 1, 1, '2022-03-04 11:15:14', '2022-03-04 11:15:14'),
(23, '1646393757.jpeg', 'Laptop', 1, 1, '2022-03-04 11:35:57', '2022-03-04 11:35:57'),
(24, '1646393809.png', 'Mobile', 1, 0, '2022-03-04 11:36:49', '2022-03-04 11:36:49'),
(27, '1646395839.jpeg', 'Tablet', 3, 1, '2022-03-04 12:02:56', '2022-03-04 12:10:38'),
(28, '1646395945.jpeg', 'Car', 1, 1, '2022-03-04 12:12:25', '2022-03-04 12:12:25');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `i_id` int(11) NOT NULL,
  `v_product_image` varchar(255) DEFAULT NULL,
  `v_status` int(1) NOT NULL DEFAULT 0 COMMENT '0=Inactive,1=Active',
  `i_product_id` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`i_id`, `v_product_image`, `v_status`, `i_product_id`) VALUES
(66, '1254775704.jpeg', 0, 7),
(67, '1479491397.jpeg', 0, 7),
(68, '1702641775.jpeg', 0, 8),
(69, '2119929228.jpeg', 0, 8),
(70, '1437282099.jpeg', 0, 9),
(71, '1655572672.jpeg', 0, 9),
(72, '126357481.jpeg', 0, 10),
(73, '1423960767.jpeg', 0, 10),
(74, '29287922.jpeg', 0, 10),
(75, '237849204.jpeg', 0, 11),
(76, '380322608.jpeg', 0, 11);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `i_id` int(10) UNSIGNED NOT NULL,
  `v_product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `v_product_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `i_price` int(11) NOT NULL,
  `i_sale_price` int(11) NOT NULL,
  `i_qty` int(11) NOT NULL,
  `v_product_status` int(1) NOT NULL DEFAULT 0 COMMENT '0 = Inactive , 1= Active',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`i_id`, `v_product_name`, `product_image`, `v_product_code`, `i_price`, `i_sale_price`, `i_qty`, `v_product_status`, `created_at`, `updated_at`) VALUES
(7, 'Lenovo G580', '1646393959.jpeg', 'P946O52', 30000, 31000, 3, 1, '2022-03-04 11:39:19', '2022-03-04 12:22:06'),
(8, 'swift', '1646394497.jpeg', 'HRL083S', 300000, 310000, 1, 1, '2022-03-04 11:48:17', '2022-03-04 11:48:17'),
(10, 'HP Pavilion', '1646395172.jpeg', 'M7NEIAX', 40000, 41000, 1, 1, '2022-03-04 11:59:33', '2022-03-04 11:59:33'),
(11, 'Redmi note pro 8', '1646396362.png', 'ZEL49H1', 20000, 21000, 1, 0, '2022-03-04 12:19:22', '2022-03-04 12:20:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_relation_category_product`
--

CREATE TABLE `tbl_relation_category_product` (
  `i_id` int(10) NOT NULL,
  `i_cat_id` int(11) NOT NULL,
  `i_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_relation_category_product`
--

INSERT INTO `tbl_relation_category_product` (`i_id`, `i_cat_id`, `i_product_id`) VALUES
(8, 26, 9),
(9, 22, 10),
(10, 23, 10),
(11, 28, 8),
(14, 22, 11),
(15, 22, 7);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `i_id` int(10) NOT NULL,
  `v_fname` varchar(200) NOT NULL,
  `v_lname` varchar(200) NOT NULL,
  `v_email` varchar(200) NOT NULL,
  `v_password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`i_id`, `v_fname`, `v_lname`, `v_email`, `v_password`) VALUES
(5, 'test', 'demo', 'Testing@gmail.com', 'Admin@123'),
(6, 'admin', 'demo', 'admin@gmail.com', 'Admin@1234'),
(7, 'dani', 'grant', 'daniegrant.cis@gmail.com', 'Crest@123'),
(8, 'gosiya', 'devashi', 'devashi.gosiya@gmail.com', 'Devshi@123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `tbl_relation_category_product`
--
ALTER TABLE `tbl_relation_category_product`
  ADD PRIMARY KEY (`i_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`i_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `i_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `i_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `i_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_relation_category_product`
--
ALTER TABLE `tbl_relation_category_product`
  MODIFY `i_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `i_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
