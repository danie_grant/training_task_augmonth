<?php 
	abstract class ImageResize 
	{
		public $imageResourceId;
		public $width;
		public $height;
		public $targetWidth;
		public $targetHeight;
		
		public function __construct($imageResourceId,$width,$height,$targetWidth,$targetHeight)
		{
			$this->imageResourceId;
			$this->width;
			$this->height;
			$this->targetWidth;
			$this->targetHeight;
		}
		
		abstract public function imageResize($imageResourceId,$width,$height,$targetWidth,$targetHeight);
	}
?>