<?php
    session_start();
    include 'database.php';
?>
<html>
    <title>Order Registration </title>
    <head>
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" />
    </head>
    <body>
    <div class="container mt-5">
            <div class="card-header" style="background-color:rgb(101, 100, 200); color: honeydew; ">
                <h1 align="center">Order Registration </h1>
            </div>
            <div class="card-body" style="border: 3px solid rgb(101, 100, 200); ">
                <form method="POST" autocomplete="off" enctype="multipart/form-data" action="order_registration_process.php">
                    <div class="form-group row">                    
                        <div class="form-group col-md-6" >
                            <label for="d_order_date">Order Date</label>
                            <input class="date form-control" type="date" name="d_order_date" id="d_order_date">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="d_shipped_date">shipped Date</label>
                            <input class="date form-control" type="date" name="d_shipped_date" id="d_shipped_date">
                        </div>
                    </div>
                    <input class="btn btn-success" type="submit" name="save" value="Save" id="save">&nbsp;&nbsp;
                    <a class="btn btn-secondary" href="index.php"><i class="fa fa-secondary"></i>Back</a>
                </div>    
            </form>
        </div> 
    </body>
    <script>
        setTimeout(function () {
            // Closing the alert
            $('.alert').alert('close');
            }, 4000);
    </script>
</html>