<table class="table table-hover" id="tables_data">
    <thead style="background: #3616;">
        <tr align="center">
            <th>ID</th>
            <th> First Name </th>
            <th> Last Name </th>
            <th> City Name </th>
            <th> Email Id </th>
            <th> Action </th>
        </tr>
    </thead>
    <tbody align="center">
    <?php
        include 'database.php';
        $sql=mysqli_query($conn,"select * from `js_master`");
        while($row=mysqli_fetch_array($sql)){
            ?>
                <tr align="center">
                    <td><?php echo $row['i_id']; ?></td>
                    <td><?php echo $row['v_first_name']; ?></td>
                    <td><?php echo $row['v_last_name']; ?></td>
                    <td><?php echo $row['v_city_name']; ?></td>
                    <td><?php echo $row['v_email_id']; ?></td>
                    <td>
                        <button class="btn btn-danger" id="btn_delete" value="<?php echo $row['i_id']; ?>">Delete</button>
                        <button class="btn btn-warning" data-toggle="modal" id="btn_edit" data-i_id="<?php echo $row['i_id']; ?>" data-target="#editmodel">Edit</button>
                    </td>
                </tr>
            <?php
        }
    ?>
    </tbody>
</table>