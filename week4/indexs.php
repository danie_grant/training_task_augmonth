<?php
    session_start();
    include 'database.php';
?>
<!DOCTYPE html>
<html>
    <title>Customers</title>
    <head>
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    </head>
    <body>
        <h1 align="center">Customer Data Show</h1>
        <a class="btn btn-success" href="customer_registration.php"><i class="fa fa-success"></i>Customer Add</a>
        <a class="btn btn-secondary" href="order_registration.php"><i class="fa fa-secondary"></i>Order Add</a>

        <div class="container-fluid">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Customer First Name</th>
                        <th>Customer Last Name</th>
                        <th>Mobile No</th>
                        <th>Address</th>
                        <th>Order Date</th>
                        <th>Shipped Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        //left join data
                        $select="SELECT * FROM customers LEFT JOIN orders ON customers.i_customer_id=orders.i_order_id";
                        
                        $result = $conn->query($select);
                        if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                    ?>
                    <tr>
                        <td><?php echo $row['i_order_id']; ?></td>
                        <td><?php echo $row['v_customer_first_name']; ?></td>
                        <td><?php echo $row['v_customer_last_name']; ?></td>
                        <td><?php echo $row['b_mobile_no']; ?></td>
                        <td><?php echo $row['v_address']; ?></td>
                         <td><?php echo $row['d_order_date']; ?></td>
                        <td><?php echo $row['d_shipped_date']; ?></td>
                        <td><a class="btn btn-danger" href="delete.php?i_customer_id=<?php echo $row['i_customer_id']; ?>"><i class="fa fa-trash"></i></a></td>
                    </tr>
                    <?php }} else {
                    echo "No Record Found";
                    }?>
                </tbody>
            </table>
        </div>
    </body>
</html>