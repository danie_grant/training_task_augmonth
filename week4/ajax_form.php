<?php
    session_start();
    include 'database.php';
?>
<html>
    <title>demo</title>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer"/>
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
    </head>
    <body>
        <div class="col-md-4 alert alert-success mt-5" id="alert">
        </div>
        <div class="col-md-12">
            <h3>Add New Record </h3>
            <div>
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal">Add Record</button>
            </div><br>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Add New Record</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <form method="POST" autocomplete="off" enctype="multipart/form-data">
                            
                            <div class="modal-body">
                                <label for="v_first_name"> First Name <span> </span> </label>
                                <input class="form-control" id="v_first_name" name="v_first_name" type="text" required>
                            </div>
                            <div class="modal-body">
                                <label for="v_last_name"> Last Name <span> </span> </label>
                                <input class="form-control" id="v_last_name" name="v_last_name" type="text" required>
                            </div>
                            <div class="modal-body">
                                <label for="v_city_name"> city Name <span> </span> </label>
                                <input class="form-control" id="v_city_name" name="v_city_name" type="text" required>
                            </div>
                            <div class="modal-body">
                                <label for="v_email_id"> Email id <span> </span> </label>
                                <input class="form-control" id="v_email_id" name="v_email_id" type="email" required>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success" name="save" value="save" id="save">Save</button>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
        </div>
       

        <div class="col-md-12">
            <div class="modal fade" id="editmodel" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Edit Record</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <form method="POST" autocomplete="off" enctype="multipart/form-data">
                            <div class="modal-body">
                                <input class="form-control" id="edit_id" name="edit_id" type="hidden" hidden>
                                <label for="first_name"> First Name <span> </span> </label>
                                <input class="form-control" id="first_name" name="first_name" type="text" value="">
                            </div>
                            <div class="modal-body">
                                <label for="last_name"> Last Name <span> </span> </label>
                                <input class="form-control" id="last_name" name="last_name" type="text" value="">
                            </div>
                            <div class="modal-body">
                                <label for="city_name"> City Name <span> </span> </label>
                                <input class="form-control" id="city_name" name="city_name" type="text" value="">
                            </div>
                            <div class="modal-body">
                                <label for="email_id"> Email ID <span> </span> </label>
                                <input class="form-control" id="email_id" name="email_id" type="email" value="">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success" name="edit_btn" id="edit_btn">Save</button>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
        </div> 
        <div id="tables_data"><?php include_once('ajax_tables_data.php'); ?></div>

        <script type="text/javascript">
            $("#alert").hide();
            $(document).ready(function() {
                    $('#save').on('click', function(e) {
                    e.preventDefault();
                    var v_first_name=$('#v_first_name').val();
                    var v_last_name=$('#v_last_name').val();
                    var v_city_name=$('#v_city_name').val();
                    var v_email_id=$('#v_email_id').val();
                    $.ajax({
                        type: "POST",
                        url: "ajax_insert.php",
                        data: {
                                v_first_name:v_first_name,
                                v_last_name:v_last_name,
                                v_city_name:v_city_name,
                                v_email_id:v_email_id,
                            },
                            success: function(data) {
                            $('#myModal').modal('hide');
                            $('#alert').html(data).fadeIn('slow');
                            $('#alert').delay(1500).fadeOut('slow');
                            test();
                        }
                    });
                });
            });
            
            function test()
            {
                $('#tables_data').load('ajax_tables_data.php');
            }

            $(document).on('click','#btn_delete', function(){
                if (confirm('Are You Sure Delete Record?')) {
                 $i_id=$(this).val();
                //console.log(i_id);
                    $.ajax({
                        type: "POST",
                        url: "ajax_delete.php",
                        data: { i_id:$i_id,},
                        success: function (data) {
                            //alert(data);
                            $('#alert').html(data).fadeIn('slow');
                            $('#alert').delay(1500).fadeOut('slow');
                            test();
                        }
                    });
                }        
            });

            $(document).on("click", "#btn_edit", function(e){
                e.preventDefault();
                i_id=$(this).data('i_id');
               // console.log(i_id);
                    $.ajax({
                    type: "POST",
                    url: "ajax_edit.php",
                    data: {
                        i_id:i_id,
                    },
                    dataTyepe:"json",
                    success: function(data){
                      //alert(data);
                        data = JSON.parse(data);
                        $("#edit_id").val(i_id);
                        $("input#first_name").val(data.v_first_name);
                        $("input#last_name").val(data.v_last_name);
                        $("input#city_name").val(data.v_city_name);
                        $("input#email_id").val(data.v_email_id);
                    }
                });
            });

            $(document).on('click','#edit_btn', function(e){
                e.preventDefault();
                var i_id=$('#edit_id').val();
                var v_first_name=$('#first_name').val();
                var v_last_name=$('#last_name').val();
                var v_city_name=$('#city_name').val();
                var v_email_id=$('#email_id').val();

                $.ajax({
                    type: "POST",
                    url: "ajax_update.php",
                    data: {
                        i_id:i_id,
                        v_first_name:v_first_name,
                        v_last_name:v_last_name,
                        v_city_name:v_city_name,
                        v_email_id:v_email_id,
                    },
                    success: function(data){
                        alert(data);
                        $('#editmodel').hide();
                        $('.modal-backdrop').remove();
                        $("input#edit_id").val(i_id);
                        $("input#first_name").val(data);
                        $("input#last_name").val(data);
                        $("input#city_name").val(data);
                        $("input#email_id").val(data);
                        $('#alert').html(data).fadeIn('slow');
                        $('#alert').delay(1500).fadeOut('slow');
                        test();
                    },
                });
            });
        </script> 
    </body>
</html>