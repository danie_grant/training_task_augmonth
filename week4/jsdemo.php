<html>
    <title>demo</title>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
        <!-- font font-awesome cdn -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer"/>
    </head>
    <body>
        <form action="" autocomplete="off" onsubmit="event.preventDefault(); formsubmit();">
            <div class="container mt-5">
                <div class="card">
                    <center><div class="card-header">
                        Demo Example
                    </div></center>
                    <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label>First name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter Your First Name" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Last name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Your Last Name" required>
                        </div>
                    </div>
                    <div class="form-row">
                    <div class="col-md-6 mb-3">
                            <label>City</label>
                            <input type="text" class="form-control" id="city_name" name="city_name" placeholder="Enter Your City" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Email Id</label>
                            <input type="email" class="form-control" id="email_id" name="email_id" placeholder="Enter Your Email Id" required>
                        </div>
                        <button class="btn btn-primary" type="submit" name="submit" id="submit">Submit</button>
                    </div>                   
                </div>
            </div>
        </form>
        <br>
        <table class="table" id="js_master">
            <thead>
                <tr align="center">
                    <th> First Name </th>
                    <th> Last Name </th>
                    <th> City Name </th>
                    <th> Email Id </th>
                    <th> Action </th>
                </tr>
            </thead>
            <tbody align="center">
                <tr>
                    <td>raj</td>
                    <td>shah</td>
                    <td>ahemdabad</td>
                    <td>abc@gmail.com</td>
                    <td><a class="btn btn-warning" onClick="onEdit(this)">Edit</a>&nbsp;<a class="btn btn-danger" onClick="onDelete(this)">Delete</a></td>
                </tr>
            </tbody>
        </table>
        <script>
            var selectedRow = null;
            
            function formsubmit()
            {
                var formData = readformdata();
                if (selectedRow == null)
                    insertNewRecord(formData);
                else
                    updateRecord(formData);
                    resetform();
            }

            function readformdata()
            {
                var formData = {};
                formData["first_name"] = document.getElementById("first_name").value;
                formData["last_name"] = document.getElementById("last_name").value;
                formData["city_name"] = document.getElementById("city_name").value;
                formData["email_id"] = document.getElementById("email_id").value;
                return formData;
            }
            
            function insertNewRecord(data) {
                var table = document.getElementById("js_master").getElementsByTagName('tbody')[0];
                var newRow = table.insertRow(table.length);
                cell1 = newRow.insertCell(0);
                cell1.innerHTML = data.first_name;
                cell2 = newRow.insertCell(1);
                cell2.innerHTML = data.last_name;
                cell3 = newRow.insertCell(2);
                cell3.innerHTML = data.city_name;
                cell4 = newRow.insertCell(3);
                cell4.innerHTML = data.email_id;
                cell5 = newRow.insertCell(4);
                cell5.innerHTML = '<a class="btn btn-warning" onClick="onEdit(this)">Edit</a>&nbsp;<a class="btn btn-danger" onClick="onDelete(this)">Delete</a>';            
            }
            
            function resetform()
            {
                document.getElementById("first_name").value='';
                document.getElementById("last_name").value='';
                document.getElementById("city_name").value='';
                document.getElementById("email_id").value='';
                selectedRow=null;
            }

            function onEdit(td)
            {
                selectedRow=td.parentElement.parentElement;
                document.getElementById("first_name").value= selectedRow.cells[0].innerHTML;
                document.getElementById("last_name").value=selectedRow.cells[1].innerHTML;
                document.getElementById("city_name").value=selectedRow.cells[2].innerHTML;
                document.getElementById("email_id").value=selectedRow.cells[3].innerHTML;
            }   

             function updateRecord(formData)
             {
                 selectedRow.cells[0].innerHTML=formData.first_name;
                 selectedRow.cells[1].innerHTML=formData.last_name;
                 selectedRow.cells[2].innerHTML=formData.city_name;
                 selectedRow.cells[3].innerHTML=formData.email_id;
             }

            function onDelete(td)
            {
                var data = confirm("Are You Sure Want To delete!");
                if(data)
                {
                    row= td.parentElement.parentElement;
                    document.getElementById("js_master").deleteRow(row.rowIndex);
                    alert("Deleted Success"); 
                    resetform();           
                }
            }
        </script> 
    </body>
</html>