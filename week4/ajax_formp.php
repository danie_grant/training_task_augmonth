<?php
    session_start();
    include 'database.php';
?>
<html>
    <title>demo</title>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer"/>
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
    </head>
    <body>
        <form method="POST" autocomplete="off" enctype="multipart/form-data" id="form_submit">
            <div class="container mt-5">
                <div class="card">
                    <center><div class="card-header">
                        Demo Example
                    </div></center>
                    <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label>First name</label>
                            <input type="text" class="form-control" id="v_first_name" name="v_first_name" placeholder="Enter Your First Name" required/>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Last name</label>
                            <input type="text" class="form-control" id="v_last_name" name="v_last_name" placeholder="Enter Your Last Name" required/>
                        </div>
                    </div>
                    <div class="form-row">
                    <div class="col-md-6 mb-3">
                            <label>City</label>
                            <input type="text" class="form-control" id="v_city_name" name="v_city_name" placeholder="Enter Your City" required/>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Email Id</label>
                            <input type="email" class="form-control" id="v_email_id" name="v_email_id" placeholder="Enter Your Email Id" required/>
                        </div>
                        <button class="btn btn-primary" type="submit" name="submit" id="submit" value="submit">Submit</button>
                    </div>                   
                </div>
            </div>
        </form><br>
        <div id="tables_data"><?php include_once('ajax_tables_data.php'); ?></div>

        <form method="POST" autocomplete="off" enctype="multipart/form-data" id="edit_form" style="display:none">
            <div class="container mt-5" id="edit_form">
                <div class="card">
                    <center><div class="card-header">
                        Demo Example
                    </div></center>
                    <div class="card-body">
                    <div class="form-row">
                        <input class="form-control" id="edit_id" name="edit_id" type="hidden" hidden> 
                        <div class="col-md-6 mb-3">
                            <label>First name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="" placeholder="Enter Your First Name" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Last name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="" placeholder="Enter Your Last Name" required>
                        </div>
                    </div>
                    <div class="form-row">
                    <div class="col-md-6 mb-3">
                            <label>City</label>
                            <input type="text" class="form-control" id="city_name" name="city_name" value="" placeholder="Enter Your City" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Email Id</label>
                            <input type="email" class="form-control" id="email_id" name="email_id" value="" placeholder="Enter Your Email Id" required>
                        </div>
                        <button class="btn btn-success" type="submit" name="edit_btn" id="edit_btn" value="edit_btn">Save</button>
                    </div>                   
                </div>
            </div>
        </form><br> 

        <script type="text/javascript">
            $(document).ready(function() {
                $("#form_submit").submit(function(e) {
                    e.preventDefault();
                    var v_first_name=$('#v_first_name').val();
                    var v_last_name=$('#v_last_name').val();
                    var v_city_name=$('#v_city_name').val();
                    var v_email_id=$('#v_email_id').val();
                    $.ajax({
                        type: "POST",
                        url: "ajax_insert.php",
                        data: {
                                v_first_name:v_first_name,
                                v_last_name:v_last_name,
                                v_city_name:v_city_name,
                                v_email_id:v_email_id,
                            },
                            success: function(data) {
                            test();
                            $("#form_submit")[0].reset();
                        }
                    });
                });
            });
            
            function test()
            {
                $('#tables_data').load('ajax_tables_data.php');
            }

            $(document).on('click','#btn_delete', function(){
                if (confirm('Are You Sure Delete Record?')) {
                 $i_id=$(this).val();
                //console.log(i_id);
                    $.ajax({
                        type: "POST",
                        url: "ajax_delete.php",
                        data: { i_id:$i_id,},
                        success: function (data) {
                        //alert(data);
                        $('#form_submit').hide();
                        test();
                        }
                    });
                }        
            });

            $(document).on("click", "#btn_edit", function(e){
                $("#edit_form").show();
                e.preventDefault();
                i_id=$(this).data('i_id');
                console.log(i_id);
                    $.ajax({
                    type: "POST",
                    url: "ajax_edit.php",
                    data: {
                        i_id:i_id,
                    },
                    success: function(data){
                     // alert(data);
                        data = JSON.parse(data);
                        $("#edit_id").val(i_id);
                        $("input#first_name").val(data.v_first_name);
                        $("input#last_name").val(data.v_last_name);
                        $("input#city_name").val(data.v_city_name);
                        $("input#email_id").val(data.v_email_id);
                    }
                });
            });

            $(document).on('click','#edit_btn', function(e){
                e.preventDefault();
                var i_id=$('#edit_id').val();
                var v_first_name=$('#first_name').val();
                var v_last_name=$('#last_name').val();
                var v_city_name=$('#city_name').val();
                var v_email_id=$('#email_id').val();
                         
                $.ajax({
                    type: "POST",
                    url: "ajax_update.php",
                    data: {
                        i_id:i_id,
                        v_first_name:v_first_name,
                        v_last_name:v_last_name,
                        v_city_name:v_city_name,
                        v_email_id:v_email_id,
                    },
                    success: function(data){
                        alert(data);
                        $("input#edit_id").val(i_id);
                        $("input#first_name").val(data);
                        $("input#last_name").val(data);
                        $("input#city_name").val(data);
                        $("input#email_id").val(data);
                        test();
                    },
                });
            });
        </script> 
    </body>
</html>