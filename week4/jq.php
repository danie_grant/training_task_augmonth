<?php
    session_start();
    include 'database.php';
?>
<html>
    <title>demo</title>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous"/>
        <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>  
    </head>
    <body>
        <form autocomplete="off" enctype="multipart/form-data" id="form_submit">
            <div class="container mt-5">
                <div class="card">
                    <center><div class="card-header">
                        Demo Example
                    </div></center>
                    <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label>First name</label>
                            <input type="text" class="form-control" id="v_first_name" name="v_first_name" placeholder="Enter Your First Name" required/>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Last name</label>
                            <input type="text" class="form-control" id="v_last_name" name="v_last_name" placeholder="Enter Your Last Name" required/>
                        </div>
                    </div>
                    <div class="form-row">
                    <div class="col-md-6 mb-3">
                            <label>City</label>
                            <input type="text" class="form-control" id="v_city_name" name="v_city_name" placeholder="Enter Your City" required/>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Email Id</label>
                            <input type="email" class="form-control" id="v_email_id" name="v_email_id" placeholder="Enter Your Email Id" required/>
                        </div>
                        <button class="btn btn-primary" type="submit" name="submit" id="submit" value="submit">Submit</button>
                    </div>                   
                </div>
            </div>
            <div class="col-md-12 mt-5">
                <table class="table text-center" id="empTable">
                    <tbody>
                        <tr class="text-center">
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">City Name</th>
                            <th scope="col">Email id</th>
                            <th colspan="2">Action</th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Edit User</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
        </form>
    </body>
    <script>
        var users = [{
            id: 1,
            v_first_name: "abc",
            v_last_name: "xyz",
            v_city_name: "junagadh",
            v_email_id: "abc@gmail.com",
            
        }];
        console.log(users);
        $.each(users, function(i, emp) {
            appendToUserTable(emp);
        });

        $("form").submit(function(e) {
            e.preventDefault();
        });

        $("form#form_submit").submit(function() {
            var msg = "User data successfully!";
            var emp = {};
            var v_first_nameInput = $('input[name="v_first_name"]').val().trim();
            var v_last_nameInput = $('input[name="v_last_name"]').val().trim();
            var v_city_nameInput = $('input[name="v_city_name"]').val().trim();
            var v_email_idInput = $('input[name="v_email_id"]').val().trim();
            if (v_first_nameInput && v_last_nameInput && v_city_nameInput && v_email_idInput) {
                $(this).serializeArray().map(function(data) {
                    emp[data.v_first_name] = data.value;
                    emp[data.v_last_name] = data.value;
                    emp[data.v_city_name] = data.value;
                    emp[data.v_email_id] = data.value;
                });
                emp["v_first_name"] = v_first_nameInput;
                emp["v_last_name"] = v_last_nameInput;
                emp["v_city_name"] = v_city_nameInput;
                emp["v_email_id"] = v_email_idInput;

                var lastUser = users[Object.keys(users).sort().pop()];
                emp.id = lastUser.id + 1;

                users.push(emp);
                appendToUserTable(emp);
                flashMessage(msg);
                $('input[name="v_first_name"]').val('');
                $('input[name="v_last_name"]').val('');
                $('input[name="v_city_name"]').val('');
                $('input[name="v_email_id"]').val('');
            }
        });
        
        function appendToUserTable(emp) 
        {
            $("#empTable > tbody:last-child").append(`
                <tr id="emp-${emp.id}">
                    '<td class="userData" name="v_first_name" id="v_first_name">${emp.v_first_name}</td>
                    '<td class="userData" name="v_last_name" id="v_last_name">${emp.v_last_name}</td>
                    '<td class="userData" name="v_city_name" id="v_city_name">${emp.v_city_name}</td>
                    '<td class="userData" name="v_email_id" id="v_email_id">${emp.v_email_id}</td>
                    '<td align="center">
                        <button class="btn btn-warning form-control" onClick="editUser(${emp.id})" data-toggle="modal" data-target="#myModal")">EDIT</button>
                    </td>
                    <td align="center">
                        <button class="btn btn-danger form-control" onClick="deleteUser(${emp.id})">DELETE</button>
                    </td>
                </tr>
            `);
        }
        
        function deleteUser(id) {
            var action = confirm("Are you sure you want to delete this user?");
            var msg = "User deleted successfully!";
            users.forEach(function(emp, i) {
                if (emp.id == id && action != false) {
                    users.splice(i, 1);
                    $("#empTable #emp-" + emp.id).remove();
                    $(document).ready(function() {
                        $("#flashMsg").addClass("alert-danger");
                    });
                }
                flashMessage(msg);
            });
        }

        function flashMessage(msg) {
            $(".flashMsg").remove();
            $(".row").prepend(`<div class="col-sm-12">
            <div class="flashMsg alert alert-success fade in" role="alert" id="flashMsg"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true" class="close1"> </span></button> <strong> ${msg}</strong></div> 
            </div>
            `);
            setTimeout(function() {
                // Closing the alert
                $('#flashMsg').alert('close');
            }, 3000);
        }

        function editUser(id) {
            users.forEach(function(emp, i) {
                if (emp.id == id) {
                    $(".modal-body").empty().append(`
                            <form id="updateUser" action="" autocomplete="off">
                            <label for="v_first_name">First Name</label>
                            <input id="v_first_name" class="form-control" type="text" name="v_first_name" value="${emp.v_first_name}"/>
                            <label for="v_last_name">Last Name</label>
                            <input class="form-control" type="text" name="v_last_name" value="${emp.v_last_name}"/>
                            <label for="v_city_name">City Name</label>
                            <input class="form-control" type="text" name="v_city_name" value="${emp.v_city_name}"/>
                            <label for="v_email_id">Email Id</label>
                            <input class="form-control" type="text" name="v_email_id" value="${emp.v_email_id}"/>
                            <input class="form-control" type="hidden" name="date" value="${emp.date}"/>
                    `);
                    $(".modal-footer").empty().append(`
                        <button type="button" class="btn btn-success" onClick="updateUser(${id})">Save</button>
                        <button type ="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </form>`);
                }
            });
            
        }
        function updateUser(id) {
            var msg = "User updated successfully!";
            var emp = {};
            emp.id = id;
            users.forEach(function(emp, i) {
                if (emp.id == id) {
                    $("#updateUser").children("input").each(function() {
                        var value = $(this).val();
                        var attr = $(this).attr("name");
                        //  console.log(attr);
                        if (attr == "v_first_name") {
                            emp.v_first_name = value;
                        } else if (attr == "v_last_name") {
                            emp.v_last_name = value;
                        } else if (attr == "v_city_name") {
                            emp.v_city_name = value;
                        } else if (attr == "v_email_id") {
                            emp.v_email_id = value;
                        } 
                    });

                    users.splice(i);
                    users.splice(emp.id, 0, emp);
                    //console.log(users);
                    $("#empTable #emp-" + emp.id).children(".userData").each(function() {
                        var attr = $(this).attr("name");
                        if (attr == "v_first_name") {
                            $(this).text(emp.v_first_name);
                        } else if (attr == "v_last_name") {
                            $(this).text(emp.v_last_name);
                        } else if (attr == "v_city_name") {
                            $(this).text(emp.v_city_name);
                        } else if (attr == "v_email_id") {
                            $(this).text(emp.v_email_id);
                        } 
                    });
       //             $(".modal").modal("toggle");
                    $(document).ready(function() {
                        $("#flashMsg").removeClass("alert-delete");
                        $("#flashMsg").addClass("alert-success");
                    });
                    flashMessage(msg);
                }
            });
        }
        $(document).click('.btn-success',function(){
            $('#myModal').hide();
        });
    </script>
</html>